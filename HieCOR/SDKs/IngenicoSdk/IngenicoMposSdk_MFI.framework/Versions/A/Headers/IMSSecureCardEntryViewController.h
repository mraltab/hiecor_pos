/*
 * //////////////////////////////////////////////////////////////////////////////
 * //
 * // Copyright (c) 2016 ROAM, Inc. All rights reserved.
 * //
 * //////////////////////////////////////////////////////////////////////////////
 */

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "Ingenico.h"

/*!
 *  Invoked when there has been a progress in the secure card entry transaction.
 *
 *  @param progressMessage progress message
 *  @see RoamProgressMessage
 *  @param extraMessage    extra message if exists
 */

typedef void (^UpdateProgress)(IMSProgressMessage progressMessage, NSString * _Nullable extraMessage);

/*!
 *  Invoked when the secure card entry transaction process is complete.
 *
 *  @param response Response for the secure card entry transaction
 *  @see IMSTransactionResponse
 *  @param error  nil if succeed, otherwise the code indicates the reason
 */

typedef void (^TransactionOnDone)(IMSTransactionResponse * _Nullable response, NSError * _Nullable error);

@interface IMSSecureCardEntryViewController : UIViewController

- (id _Nullable)initWithTransactionRequest:(IMSSCETransactionRequest * _Nonnull)transactionrequest
                         andUpdateProgress:(UpdateProgress _Nonnull)progressHandler
                                 andOnDone:(TransactionOnDone _Nonnull)handler __attribute__((deprecated));

- (id _Nullable)initWithCreditSaleTransactionRequest:(IMSSCECreditSaleTransactionRequest * _Nonnull)creditSaleTransactionrequest
                         andUpdateProgress:(UpdateProgress _Nonnull)progressHandler
                                 andOnDone:(TransactionOnDone _Nonnull)handler;

- (id _Nullable)initWithCreditAuthTransactionRequest:(IMSSCECreditAuthTransactionRequest * _Nonnull)creditAuthTransactionrequest
                                   andUpdateProgress:(UpdateProgress _Nonnull)progressHandler
                                           andOnDone:(TransactionOnDone _Nonnull)handler;

- (id _Nullable)initWithCreditRefundTransactionRequest:(IMSSCECreditRefundTransactionRequest * _Nonnull)creditRefundtransactionrequest
                         andUpdateProgress:(UpdateProgress _Nonnull)progressHandler
                                 andOnDone:(TransactionOnDone _Nonnull)handler;

@end
