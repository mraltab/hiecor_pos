/*
 * //////////////////////////////////////////////////////////////////////////////
 * //
 * // Copyright (c) 2016 ROAM, Inc. All rights reserved.
 * //
 * //////////////////////////////////////////////////////////////////////////////
 */

#import "IMSBaseTransactionRequest.h"

/*!
 * This object contains basic information required for a reversal.
 */

@interface IMSRevesalTransactionRequest : IMSBaseTransactionRequest

/*!
 * Transaction ID of the original sale transaction generated by Ingenico Payment Server.
 */

@property (nonatomic, strong) NSString *originalSaleTransactionID;

/*!
 * Constructs a reversal request with mandatory fields that are common for all reversal transaction requests.
 * @param type Transaction type.
 * @param originalSaleTransactionID id of the transaction to reverse
 * @param gpsLong longitude of the device location
 * @param gpsLal latitude of the device location
 */

- (id) initWithType:(IMSTransactionType )type andOriginalSaleTransactionID:(NSString *)originalSaleTransactionID andLongitude:(NSString *)gpsLong andLatitude:(NSString *)gpsLal;

/*!
 * Constructs a reversal request with mandatory fields that are common for all reversal transaction requests.
 * @param type Transaction type.
 * @param originalSaleTransactionID id of the transaction to reverse
 * @param clerkID ID reference used to associate the transaction with a waiter / clerk / sales associate.
 *                The field can be alphanumeric and length cannot exceed 4 characters.
 * @param gpsLong longitude of the device location
 * @param gpsLal latitude of the device location
 */

- (id) initWithType:(IMSTransactionType )type andOriginalSaleTransactionID:(NSString *)originalSaleTransactionID andClerkID:(NSString *)clerkID andLongitude:(NSString *)gpsLong andLatitude:(NSString *)gpsLal;

/*!
 * Constructs a reversal request with mandatory fields that are common for all reversal transaction requests.
 * @param type Transaction type.
 * @param originalSaleTransactionID id of the transaction to reverse
 * @param clerkID ID reference used to associate the transaction with a waiter / clerk / sales associate.
 *                The field can be alphanumeric and length cannot exceed 4 characters.
 * @param gpsLong longitude of the device location
 * @param gpsLal latitude of the device location
 * @param customReference 20 digit merchant order number (optional). Alphanumeric characters and hyphen only. Max length: 20.
 */

- (id) initWithType:(IMSTransactionType )type andOriginalSaleTransactionID:(NSString *)originalSaleTransactionID andClerkID:(NSString *)clerkID andLongitude:(NSString *)gpsLong andLatitude:(NSString *)gpsLal andCustomReference:(NSString *)customReference;



@end
