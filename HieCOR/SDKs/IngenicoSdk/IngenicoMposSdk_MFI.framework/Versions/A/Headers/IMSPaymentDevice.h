/*
 * //////////////////////////////////////////////////////////////////////////////
 * //
 * // Copyright (c) 2016 ROAM, Inc. All rights reserved.
 * //
 * //////////////////////////////////////////////////////////////////////////////
 */

#import "Ingenico.h"

/*!
 *  Handler to be invoked after the device setup request is processed.
 *  @param current current step of the set up process
 *  @param total   total number of steps in the setup process
 */

typedef void (^SetupProgressHandler)(NSInteger current, NSInteger total);

/*!
 *  Handler to be invoked after the device setup request is processed.
 *
 *  @param error    nil if succeed, otherwise the code indicates the reason
 */

typedef void (^SetupHandler)(NSError * _Nullable error);

/*!
 *  Handler to be invoked after receiving the card reader's serial number.
 *  @param serialNumber    serial number of connected device
 *  @param error    nil if succeed, otherwise the code indicates the reason
 */

typedef void (^GetDeviceSerialNumberHandler)(NSString * _Nullable serialNumber,NSError * _Nullable error);

/*!
 *  Hanlder to be invoked after receiving the card reader's battery level.
 *  @param batteryLevel    battery level of connected device
 *  @param error    nil if succeed, otherwise the code indicates the reason
 */

typedef void (^BatteryLevelHanlder)(NSInteger batteryLevel, NSError * _Nullable error);

/*!
 *  Hanlder to be invoked after the card is removed from card reader or the timeout expires.
 *  @param error    nil if succeed, otherwise the code indicates the reason
 */

typedef void (^WaitForCardRemovalHanlder)(NSError * _Nullable error);

/*!
 *  Hanlder to be invoked after checking if there is firmware update is available.
 *  @param error    nil if succeed, otherwise the code indicates the reason
 *  @param action    enum indicating if there is a firmware update available or not
 *  @param firmwareInfo    contains information about the firmware
 */

typedef void (^CheckFirmwareUpdateHanlder)(NSError * _Nullable error, IMSFirmwareUpdateAction action,IMSFirmwareInfo * _Nullable firmwareInfo);

/*!
 *  Invoked when there has been a progress in the firmware download.
 *
 *  @param downloadedSize  bytes downloaded
 *  @param totalFileSize    total file size
 */

typedef void (^FirmwareDownloadProgress)(long downloadedSize,long totalFileSize);

/*!
 *  Invoked when there has been a progress in the firmware update.
 *
 *  @param current current step of the firmware update process
 *  @param total   total number of steps in the firmware update process
 */

typedef void (^FirmwareUpdateProgress)(NSInteger current, NSInteger total);

/*!
 *  Hanlder to be invoked when the firmware update process is finished.
 *  @param error    nil if succeed, otherwise the code indicates the reason
 */

typedef void (^FirmwareUpdateHandler)(NSError * _Nullable error);

/*!
 *  Hanlder to be invoked after checking if device setup is available.
 *  @param error    nil if succeed, otherwise the code indicates the reason
 *  @param isSetupRequired    true if device setup is required, false otherwise
 */

typedef void (^CheckDeviceSetupHandler)(NSError * _Nullable error,bool isSetupRequired);

/*!
 *  Hanlder to be invoked when the configure beep is finished.
 *  @param error    nil if succeed, otherwise the code indicates the reason
 */

typedef void (^ConfigureBeepHandler)(NSError * _Nullable error);

/*!
 *  Handler to be invoked after decrypting the magnetica card data.
 *
 *  @param error    nil if succeed, otherwise the code indicates the reason
 *  @param decryptedTrackData    decryptedTrackData if exists
 */
typedef void (^ReadMagneticCardDataHanlder) (NSString * _Nullable decryptedTrackData, NSError * _Nullable error);

/*!
 *  Hanlder to be invoked when the configure idle shutdown timeout is finished.
 *  @param error    nil if succeed, otherwise the code indicates the reason
 */

typedef void (^ConfigureIdleShutdownTimeoutHandler)(NSError * _Nullable error);

/*!
 *  Hanlder to be invoked after display text is processed by the payment device
 *  @param error    nil if succeed, otherwise the code indicates the reason
 */

typedef void (^DisplayTextHandler)(NSError * _Nullable error);

/*!
 *  Hanlder to be invoked after the home screen is displayed by the payment device
 *  @param error    nil if succeed, otherwise the code indicates the reason
 */

typedef void (^ShowHomeScreenHandler)(NSError * _Nullable error);

/*!
 *  Hanlder to be invoked after tip amount is collected on the reader
 *  @param error    nil if success, otherwise the code indicates the reason <br>
 *                  Returns TipEntryAborted(6014), if tip entry is aborted on the reader. <br>
 *                  Returns InvalidAmount(4990), if tip entered is outside the valid range.
 *  @param tipAmount    Tip amount in cents. (0 - 999999999) <br>
 *                      Returns 0 for failure.
 */

typedef void (^RetrieveTipAmountHandler)(NSError * _Nullable error, NSInteger tipAmount);

/*!
 *  Hanlder to be invoked after configure application selection is finished
 *  @param error    nil if succeed, otherwise the code indicates the reason
 */

typedef void (^ConfigureApplicationSelectionHandler)(NSError * _Nullable error);

/*!
 *  This class contains methods to handle device setup, device communication
 *  and device status.
 */

@interface IMSPaymentDevice : NSObject

/*!
 *  Sets the supported device type.
 *  @param deviceType device type
 */

- (void) setDeviceType:(RUADeviceType) deviceType;

/*!
 *  Sets the list of supported audio jack device types.
 *
 *  @param deviceList list of audio jack device types
 */

- (void) setDeviceTypes:(NSArray * _Nonnull)deviceList;

/*!
 *  Searches for available bluetooth devices.
 *
 *  @param searchListener search listener
 */

- (void)search:(id <RUADeviceSearchListener>  _Nonnull)searchListener;

/*!
 *  Searches for available bluetooth devices for duration.
 *
 *  @param duration duration of the bluetooth discovery in milliseconds
 *  @param searchListener search listener
 */

- (void)searchForDuration:(long)duration andListener:(id <RUADeviceSearchListener>  _Nonnull)searchListener;

/*!
 *  Stops searching for bluetooth devices.
 */

- (void)stopSearch;

/*!
 *  Sets up the connection with the selected bluetooth device.
 *
 *  @param device Device object
 */

- (void)select:(RUADevice * _Nonnull)device;

/*!
 *  Initializes the card reader to work with the device types specified.
 *  Also sets up a DeviceStatusHandler to handle the device connection status.
 *
 *  @param deivceStatusHanlder device status handler
 */

- (void)initialize:(id<RUADeviceStatusHandler> _Nonnull) deivceStatusHanlder;

/*!
 *  Gets the connected payment device's serial number
 *
 *  @param hanlder The callback used to indicate that the
 *                 get serial number request has been processed.
 */

- (void)getDeviceSerialNumber:(GetDeviceSerialNumberHandler _Nonnull)hanlder;

/*!
 *  Returns the type of the connected device.
 */

- (RUADeviceType)getType;

/*!
 *  Get battery level of the connected device
 *
 *  @param handler The callback used to indicate that the
 *                 get battery level request has been processed.
 */

- (void)getDeviceBatteryLevel:(BatteryLevelHanlder _Nonnull)handler;

/*!
 *  Sets up the card reader with public keys and Application identifiers based 
 *  on the response of checkDeviceSetup.
 *
 *  @param setupHandler The callback used to indicate that the
 *                      the device setup request has been processed.
 */

- (void)setup:(SetupHandler _Nonnull)setupHandler;

/*!
 *  Sets up the card reader with public keys and Application identifiers based
 *  on the response of checkDeviceSetup.
 *
 *  @param progressHandler    progress handler
 *  @param setupHandler The callback used to indicate that the
 *                      the device setup request has been processed.
 */

- (void)setupWithProgressHandler:(SetupProgressHandler _Nonnull)progressHandler
                       andOnDone:(SetupHandler _Nonnull)setupHandler;

/*!
 * Asynchronous method that will start the process of pairing with
 * a Bluetooth card reader via AudioJack that doesn't have a keypad or display.
 * Currently only supported by the RP450c device.
 *
 * @param pairListener device pairing listener
 * @see RUAAudioJackPairingListener
 */
- (void)requestPairing:(id <RUAAudioJackPairingListener>  _Nonnull) pairListener;

/*!
 * Asynchronous method that will complete the process of pairing with
 * a Bluetooth card reader via AudioJack that doesn't have a keypad or display.
 * Currently only supported by the RP450c device.
 * <br>@deprecated
 */
- (void)confirmPairing:(BOOL)isMatching __deprecated;

/*!
 *  Get allowed pos entry modes of the connected device
 *
 *  @return list of allowed pos entry modes of the connected device.
 */
- (NSArray * _Nullable)allowedPOSEntryModes;

/*!
 * Releases all the card reader resources.
 * Calls RUA RUADeviceStatusHandler onDisconnected() when complete.
 */
- (void)releaseDevice;

/*!
 * Releases all the card reader resources.
 * Calls RUA RUAReleaseHandler done() when complete.
 */
- (void)releaseDevice:(id <RUAReleaseHandler> _Nonnull)releaseHandler;

/*!
 * Asynchronous method that will make the card reader wait until the card is fully 
 * removed or if the timeout expires before returning a response
 *
 * @param cardRemovalTimeout timeout period for card removal in seconds.<br> Range 1 - 65 <br> 0 - indefinite wait
 * @param handler The callback used to indicate that the card is removed from card reader or the timeout expires.
 */
- (void)waitForCardRemoval:(NSInteger)cardRemovalTimeout andOnDone:(WaitForCardRemovalHanlder _Nonnull)handler;

/*!
 *  Indicates the connection status of the device
 *
 *  @return True - if it is connected, 
 *          False - otherwise
 */
-(bool)isConnected;

/*!
 * Asynchronous method that will remotely turn on the device connected via audio jack.
 * Ensure that an audio jack device is plugged in before invoking this method.
 *
 * @param turnOnDeviceCallback The callback used to return the result if the device was successfully turned on or not.
 * @see RUATurnOnDeviceCallback
 */
- (void)turnOnDeviceViaAudioJack:(id <RUATurnOnDeviceCallback>  _Nonnull) turnOnDeviceCallback;

/*!
 * Asynchronous method that will check if the card reader requires a firmware update or not.
 *
 * @param checkFirmwareUpdateHandler The callback used to provide the result if firmware update is required
 */
- (void)checkFirmwareUpdate:(CheckFirmwareUpdateHanlder _Nonnull)checkFirmwareUpdateHandler;

/*!
 * Asynchronous method that will update the firmware of the connected reader to the provided version
 *
 * @param firmwareDownloadProgress Indicates the download progress
 * @param firmwareUpdateProgress Indicates the update progress
 * @param handler The callback used to provide the result of the firmware update
 */
- (void)updateFirmwareWithDownloadProgress:(FirmwareDownloadProgress _Nonnull)firmwareDownloadProgress
                           andFirmwareUpdateProgress:(FirmwareUpdateProgress _Nonnull)firmwareUpdateProgress
                                           andOnDone:(FirmwareUpdateHandler _Nonnull)handler;

/*!
 * Asynchronous method that will check if connected device is required to be setup before processing transactions.
 *
 * @param checkDeviceSetupHandler The callback used to provide the result if setup is required
 */
- (void)checkDeviceSetup:(CheckDeviceSetupHandler _Nonnull)checkDeviceSetupHandler;

/**
 * Returns the active communication type of connected device.
 * E.g RP450c that is plugged in via audio jack and can have active bluetooth connection.
 * @return {@see RUACommunicationInterface}
 */
- (RUACommunicationInterface)getActiveCommunicationType;

/*!
 * Asynchronous method that will configure the card reader beeps
 *
 * @param disableCardRemovalBeep                 indicates whether the reader should disable beep when card is ready to be removed
 * @param disableCardPresentmentBeep             indicates whether the reader should disable beep when reader is ready for card to be presented (Insert / Swipe / Tap)
 * @param handler                                callback used to provide the result of the configure beep
 */
- (void)configureCardRemovalBeep:(bool)disableCardRemovalBeep
          andCardPresentmentBeep:(bool)disableCardPresentmentBeep
                       andOnDone:(ConfigureBeepHandler _Nonnull)handler;

/*!
 * Reads magnetic card data from the connected card reader and returns the decrypted track data.
 *
 * @param handler            The callback used to indicate that the
 *                           readMagneticCardData request has been processed
 */
- (void)readMagneticCardData:(ReadMagneticCardDataHanlder _Nonnull)handler;

/*!
 * Asynchronous method that will configure the duration that the device remains active before shutting down.
 * <b>Note:</b> This API is not supported for audiojack only devices i.e. G4x, RP350x
 *
 * @param idleShutdownTimeoutInSeconds timeout period in seconds.<br> Range 180 - 1800 <br>
 * @param handler The callback used to indicate that configuration was completed.
 */
- (void)configureIdleShutdownTimeout:(NSInteger)idleShutdownTimeoutInSeconds andOnDone:(ConfigureIdleShutdownTimeoutHandler _Nonnull)handler;

/*!
 * Asynchronous method that displays the text on the payment device.
 * Note: The currently supported devices with LCD display are RP750 and MOBY/8500.
 * Both of these devices support displaying characters upto 4 lines and 21 characters in each line.
 *
 * @param row integer representing the row where the text needs to be displayed. 1 - 4 are permissible values for RP750 and MOBY/8500
 * @param column integer representing the column where the text needs to be displayed. 1 - 21 are permissible values for RP750 and MOBY/8500
 * @param text text that needs to be displayed. For RP750 and MOBY/8500, if text is more than 21 characters then it will wrap to the second row.
 * @param displayTextHandler The callback used to indicate that text is displayed.
 */
- (void)displayText:(NSString *_Nonnull)text atRow:(NSInteger)row andColumn:(NSInteger)column andOnDone:(DisplayTextHandler _Nonnull)displayTextHandler;

/*!
 * Asynchronous method that shows the home screen on the payment device.
 *
 * @param showHomeScreenHandler The callback used to indicate that display is cleared.
 */
- (void)showHomeScreen:(ShowHomeScreenHandler _Nonnull)showHomeScreenHandler;

/*!
 * Asynchronous method that configures application selection on the card reader.
 * Note: The currently supported devices with pin pad are RP750 and MOBY/8500.
 *
 * @param option The enum value IMSApplicationSelectionOption.
 * @param handler The callback used to indicate that configuration was completed.
 *
 * @see IMSApplicationSelectionOption
 * @see ConfigureApplicationSelectionHandler
 */
- (void)configureApplicationSelection:(IMSApplicationSelectionOption) option
                            andOnDone:(ConfigureApplicationSelectionHandler _Nonnull) handler;

/*!
 * Asynchronous method that lets tip entry on the reader.
 * Note: The currently supported devices with pin pad are RP750 and MOBY/8500.
 * @param retrieveTipAmountHandler The callback used to provide the result of tip entry.
 *
 * @see RetrieveTipAmountHandler
 */
- (void)retrieveTipAmount:(RetrieveTipAmountHandler _Nonnull)retrieveTipAmountHandler;

/*!
 * Synchronous method that will cancel the firmware update process and force the reader to exit the firmware update mode
 */
- (void)cancelFirmwareUpdate;

@end
