/*
 * //////////////////////////////////////////////////////////////////////////////
 * //
 * // Copyright (c) 2016 ROAM, Inc. All rights reserved.
 * //
 * //////////////////////////////////////////////////////////////////////////////
 */

#import "IMSBaseRefundTransactionRequest.h"

/*!
 * This object contains information required for requesting refund for a card sale.
 */

@interface IMSCreditRefundTransactionRequest : IMSBaseRefundTransactionRequest

/*!
 * Constructs a card refund request.
 * @param originalSaleTransactionID id of the credit sale transaction to refund
 * @param amount refund amount
 * @param gpsLong longitude of the device location
 * @param gpsLal latitude of the device location
 */

- (id) initWithOriginalSaleTransactionID:(NSString *)originalSaleTransactionID andAmount:(IMSAmount *)amount andLongitude:(NSString *)gpsLong andLatitude:(NSString *)gpsLal;

/*!
 * Constructs a card refund request.
 * @param originalSaleTransactionID id of the credit sale transaction to refund
 * @param amount refund amount
 * @param clerkID ID reference used to associate the transaction with a waiter / clerk / sales associate.
 *                The field can be alphanumeric and length cannot exceed 4 characters.
 * @param gpsLong longitude of the device location
 * @param gpsLal latitude of the device location
 */

- (id) initWithOriginalSaleTransactionID:(NSString *)originalSaleTransactionID andAmount:(IMSAmount *)amount andClerkID:(NSString *)clerkID andLongitude:(NSString *)gpsLong andLatitude:(NSString *)gpsLal;

/*!
 * Constructs a card refund request.
 * @param originalSaleTransactionID id of the credit sale transaction to refund
 * @param amount refund amount
 * @param clerkID ID reference used to associate the transaction with a waiter / clerk / sales associate.
 *                The field can be alphanumeric and length cannot exceed 4 characters.
 * @param gpsLong longitude of the device location
 * @param gpsLal latitude of the device location
 * @param customReference 20 digit merchant order number (optional). Alphanumeric characters and hyphen only. Max length: 20.
 */

- (id) initWithOriginalSaleTransactionID:(NSString *)originalSaleTransactionID andAmount:(IMSAmount *)amount andClerkID:(NSString *)clerkID andLongitude:(NSString *)gpsLong andLatitude:(NSString *)gpsLal andCustomReference:(NSString *)customReference;


@end
