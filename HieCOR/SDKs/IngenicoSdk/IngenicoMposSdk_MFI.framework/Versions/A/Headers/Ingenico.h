/*
 * //////////////////////////////////////////////////////////////////////////////
 * //
 * // Copyright (c) 2016 ROAM, Inc. All rights reserved.
 * //
 * //////////////////////////////////////////////////////////////////////////////
 */
#import "IMSDebitSaleTransactionRequest.h"
#import "IMSCreditSaleTransactionRequest.h"
#import "IMSCashSaleTransactionRequest.h"
#import "IMSKeyedCardSaleTransactionRequest.h"
#import "IMSVoidTransactionRequest.h"
#import "IMSCashRefundTransactionRequest.h"
#import "IMSCreditRefundTransactionRequest.h"
#import "IMSCreditAuthTransactionRequest.h"
#import "IMSCreditCardRefundTransactionRequest.h"
#import "IMSCreditAuthCompleteTransactionRequest.h"
#import "IMSKeyedCreditAuthTransactionRequest.h"
#import "IMSDebitCardRefundTransactionRequest.h"
#import "IMSKeyedCardSaleTransactionWithCardReaderRequest.h"
#import "IMSKeyedCreditForceSaleTransactionRequest.h"
#import "IMSCreditForceSaleTransactionRequest.h"
#import "IMSCreditBalanceInquiryTransactionRequest.h"
#import "IMSDebitBalanceInquiryTransactionRequest.h"
#import "IMSTokenEnrollmentTransactionRequest.h"
#import "IMSKeyedTokenEnrollmentTransactionRequest.h"
#import "IMSStoreAndForwardCreditSaleTransactionRequest.h"
#import "IMSStoreAndForwardKeyedCardSaleTransactionRequest.h"
#import "IMSStoreAndForwardCreditAuthTransactionRequest.h"
#import "IMSStoreAndForwardCashSaleTransactionRequest.h"
#import "IMSStoreAndForwardVoidTransactionRequest.h"
#import "IMSCreditResaleTransactionRequest.h"
#import "IMSCreditReauthTransactionRequest.h"
#import "IMSCreditAuthAdjustTransactionrequest.h"
#import "IMSPartialVoidTransactionRequest.h"
#import "IMSTransactionResponse.h"
#import "IMSUserProfile.h"
#import "IMSProduct.h"
#import "IMSEmailReceiptInfo.h"
#import "IMSTransactionHistoryDetail.h"
#import "IMSTransactionHistorySummary.h"
#import "IMSInvoiceHistorySummary.h"
#import "IMSSecurityQuestion.h"
#import "IMSFirmwareInfo.h"
#import "IMSEnum.h"
#import "IMSResponseCode.h"
#import "IMSPendingTransaction.h"
#import "IMSTokenRequestParametersBuilder.h"
#import "IMSTokenRequestParameters.h"
#import "IMSTokenResponseParameters.h"
#import "IMSStoredTransactionSummary.h"
#import "IMSSCETransactionRequest.h"
#import "IMSSCECreditSaleTransactionRequest.h"
#import "IMSSCECreditAuthTransactionRequest.h"
#import "IMSSCECreditRefundTransactionRequest.h"
#import "IMSTokenSaleTransactionRequest.h"
#import "IMSCreditSaleAdjustTransactionRequest.h"
#import "IMSEmvOfflineData.h"
#import "IMSEmvData.h"
#import "IMSProcessorInfo.h"
#import "IMSTransactionsSummary.h"
#import "IMSAVSOnlyTransactionRequest.h"
#import "IMSSaleTransactionRequest.h"
#import "IMSRefundTransactionRequest.h"
#import "IMSBalanceInquiryTransactionRequest.h"
#ifdef RUA_MFI
#import <RUA_MFI/RUA.h>
#import <RUA_MFI/RUADeviceStatusHandler.h>
#import <RUA_MFI/RUADeviceSearchListener.h>
#else
#import <RUA_BLE/RUA.h>
#import <RUA_BLE/RUADeviceStatusHandler.h>
#import <RUA_BLE/RUADeviceSearchListener.h>
#endif
#import <Foundation/Foundation.h>
#import "IMSPayment.h"
#import "IMSUser.h"
#import "IMSPaymentDevice.h"
#import "IMSStoreAndForward.h"

static NSString * _Nonnull mPOS_Version = @"2.0.0.21";
/*!
 * This is a singleton class that acts as the entry point to access the Ingenico mPOS SDK.
 */

/*!
 * Callback to be invoked after pinging the Ingenico Payment Services
 *  @param error  nil if succeed, otherwise the code indicates the reason
 */

typedef void (^PingHanlder)(NSError * _Nullable error);


@interface Ingenico : NSObject

/*!
 *  Returns a handler to process transactions.
 *  @see IMSPayment
 */

@property (readonly) IMSPayment * _Nonnull Payment;

/*!
 *  Returns a handler to perform user related actions.
 *  @see IMSUser
 */

@property (readonly) IMSUser * _Nonnull User;

/*!
 *  Returns a handler to manage devices.
 *  @see IMSPaymentDevice
 */

@property (readonly) IMSPaymentDevice * _Nonnull PaymentDevice;

/*!
 *  Returns a handler to perform store and forward transactions.
 *  @see IMSStoreAndForward
 */

@property (readonly) IMSStoreAndForward * _Nonnull StoreAndForward;

/*!
 *  Returns an instance of this class.
 */
+ (Ingenico * _Nullable)sharedInstance;


/*!
 *  Initializes the APIs and sets URL, API Key and client version (for tracking purposes).
 *
 *  @param baseURL hostname of the Ingenico Payment Server
 *         Note: Use https URL in production.
 *  @param apiKey  API key of the client application for the Ingenico Payment Server
 *                 API Keys with the below prefixes are supported :
 *                   1. RPX
 *                   2. SDK
 *                   3. CAT
 *  @param clientVersion  version of the client application
 */
- (void)initializeWithBaseURL:(NSString * _Nonnull)baseURL apiKey:(NSString * _Nonnull)apiKey clientVersion:(NSString * _Nonnull)clientVersion;


/*!
 *  Initializes the APIs and sets URL, API Key and client version (for tracking purposes).
 *
 *  @param baseURL hostname of the Ingenico Payment Server
 *         Note: Use https URL in production.
 *  @param apiKey  API key of the client application for the Ingenico Payment Server
 *                 API Keys with the below prefixes are supported :
 *                   1. RPX
 *                   2. SDK
 *                   3. CAT
 *  @param clientVersion  version of the client application
 *  @param timeout timeout period that would be used for network requests. Accepted values 10-60 seconds
 */
- (void)initializeWithBaseURL:(NSString * _Nonnull)baseURL apiKey:(NSString * _Nonnull)apiKey clientVersion:(NSString * _Nonnull)clientVersion timeout:(NSInteger)timeout;


/*!
 * Releases all the resources held by the APIs. Also releases the device.
 * <br>
 * <b>Note:</b> Call this method after {@link User#logOff(LogoffCallback)}.
 */
- (void)reset;


/*!
 * Enable/Disable logging for Ingenico mPOS SDK. Logging is disabled by default.
 * For security reasons all of card holder, card and other sensitive information are masked in logs.
 * <br>
 * <b>Important:</b> Avoid logging in production
 *
 *  @param logging true - to enable, false - to disable
 */
- (void)setLogging:(BOOL)logging;

/*!
 *
 * Set a custom logger to override the default OS logging system
 * @param customLogger an object that conforms to the RUADebugLogListener protocol
 * @see RUADebugLogListener
 * <br>
 * <b>Important:</b> Avoid logging in production
 *
 */
- (void)setCustomLogger:(id<RUADebugLogListener> _Nonnull)customLogger;

/*!
 * Asynchronous method that checks if the Ingenico Payment Services are reachable
 *  @param handler            the callback used to indicate that the
 *                            ping action has been executed
 */
- (void)ping:(PingHanlder _Nonnull)handler;

/*!
 *  Returns the version of Ingenico mPOS SDK
 */
- (NSString *_Nonnull)getVersion;

@end
