//
//  IngenicoViewControllerVC.swift
//  HieCOR
//
//  Created by Hiecor on 17/06/20.
//  Copyright © 2020 HyperMacMini. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class IngenicoViewControllerVC: BaseViewController {

    var delegate: PaymentTypeContainerViewControllerDelegate?
    var totalAmount = Double()
    @IBOutlet weak var txfAmount: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        txfAmount.delegate = self
        txfAmount.setDollar(font: txfAmount.font!)
        if HomeVM.shared.DueShared > 0 {
            txfAmount.text = HomeVM.shared.DueShared.currencyFormatA
        } else {
            txfAmount.text = totalAmount.currencyFormatA
        }
        appDelegate.CardReaderAmount = txfAmount.text?.toDouble() ?? 0.0
    }
    
    @objc func handleCardNumberTextField(sender: UITextField) {
        
        callValidateToChangeColor()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        txfAmount.updateCustomBorder()
    }
    
    func callValidateToChangeColor() {
        let obEx = (txfAmount.text! as NSString).replacingOccurrences(of: "$", with: "")
        if DataManager.isSplitPayment {
            if UI_USER_INTERFACE_IDIOM() == .pad {
                if txfAmount.text != "" {
                    delegate?.checkPayButtonColorChange?(isCheck: true, text: "CARD READER")
                }else{
                    delegate?.checkPayButtonColorChange?(isCheck: false, text: "CARD READER")
                }
            } else {
                if txfAmount.text != "" {
                    delegate?.checkIphonePayButtonColorChange?(isCheck: true, text: "CARD READER")
                }else{
                    delegate?.checkIphonePayButtonColorChange?(isCheck: false, text: "CARD READER")
                }
            }
            
        } else {
            if UI_USER_INTERFACE_IDIOM() == .pad {
                if txfAmount.text != "" && ((obEx as NSString).doubleValue == totalAmount ||  totalAmount < (obEx as NSString).doubleValue)  {
                    delegate?.checkPayButtonColorChange?(isCheck: true, text: "CARD READER")
                }else{
                    delegate?.checkPayButtonColorChange?(isCheck: false, text: "CARD READER")
                }
            }else{
                if txfAmount.text != "" && ((obEx as NSString).doubleValue == totalAmount ||  totalAmount < (obEx as NSString).doubleValue) {
                    delegate?.checkIphonePayButtonColorChange?(isCheck: true, text: "CARD READER")
                }else{
                    delegate?.checkIphonePayButtonColorChange?(isCheck: false, text: "CARD READER")
                }
            }
        }
        
    }
    
    func disableValidateToChangeColor() {
           if UI_USER_INTERFACE_IDIOM() == .pad {
               if txfAmount.text == "" || (txfAmount.text != String(totalAmount) ||  txfAmount.text! < String(totalAmount))  {
                   delegate?.checkPayButtonColorChange?(isCheck: false, text: "CARD READER")
               }
           }else{
               if txfAmount.text == "" || (txfAmount.text != String(totalAmount) ||  txfAmount.text! < String(totalAmount))  {
                   delegate?.checkIphonePayButtonColorChange?(isCheck: false, text: "CARD READER")
               }
           }
       }
}

//MARK: PaymentTypeDelegate
extension IngenicoViewControllerVC: PaymentTypeDelegate {
    
    func didUpdateTotal(amount: Double , subToal : Double) {
        print("amount",amount)
        totalAmount = amount
        txfAmount.text = amount.currencyFormatA
        appDelegate.CardReaderAmount = txfAmount.text?.toDouble() ?? 0.0
        callValidateToChangeColor()
    }
    
    func sendIngenicoCardData(with key: String, isIPad: Bool, data: Any) {
        
        print(data)
        let ingenicoData = data as! IMSTransactionResponse
        //txfAmount.text = "\(ingenicoData.submittedAmount.total)"
        let numnber = ingenicoData.redactedCardNumber
        var expYear = ""
        var expmounth = ""
        let hh = ingenicoData.transactionType
        
        if ingenicoData.cardExpirationDate == "" || ingenicoData.cardExpirationDate == nil {
            expYear = ""
            expmounth = ""
        } else {
            expYear = "\(ingenicoData.cardExpirationDate.prefix(2))"
            expmounth = "\(ingenicoData.cardExpirationDate.suffix(2))"
        }
        
        let amt = Double(ingenicoData.submittedAmount.total - ingenicoData.submittedAmount.tip)/100
        var transaction : [String:Any]
        var transactionCode = ""
        if ingenicoData.transactionResponseCode.rawValue == 2  {
            transactionCode = "declined"
        } else if ingenicoData.transactionResponseCode.rawValue == 1 {
            transactionCode = "approved"
        }
        
        transaction = [ "authorization_code": ingenicoData.authCode ?? "",
                        "authorized_amount": Double(ingenicoData.authorizedAmount)/100 ,
                        "avs_response": "Unknown",
        "batch_number": ingenicoData.batchNumber ?? "",
        "card_type": "Unknown",
        "clerk_display": ingenicoData.clerkDisplay ?? "",
        "client_transaction_id": ingenicoData.clientTransactionID ?? "",
        "currency_code": ingenicoData.submittedAmount.currency ?? "",
        "customer_display": ingenicoData.customerDisplay ?? "",
        "cvd_response": "Unknown",
        "expiration_date": ingenicoData.cardExpirationDate ?? "",
        "mcm_transaction_id": ingenicoData.transactionID ?? "",
        "original_amount": Double(ingenicoData.submittedAmount.total)/100,
        "primary_account_number_masked": ingenicoData.redactedCardNumber ?? "" ,
        "request_transaction_code": "CreditSale",
        "sequence_no": ingenicoData.sequenceNumber ?? "",
        "transaction_amount": Double(ingenicoData.submittedAmount.total)/100,
        "transaction_code":transactionCode,
        "transaction_group_id": ingenicoData.transactionGroupID ?? "",
        "transaction_id": ingenicoData.transactionGUID ?? ""]

        let transactiondata = ["transaction":transaction]

        
        let Obj = ["cardnumber":numnber,"mm":expmounth, "yyyy":expYear, "amount": amt, "cvv":"", "auth":"AUTH_CAPTURE", "orig_txn_response":ingenicoData.description, "merchant": "ingenico", "txn_response": transactiondata] as [String : Any]
        delegate?.getPaymentData?(with: Obj)

        if UI_USER_INTERFACE_IDIOM() == .pad {
            self.delegate?.placeOrderForIpad?(with: 1 as AnyObject) //1 for pass dummy value// not for use
        }
    }
    
    private func sendIngenicoCardData(with key: String, isIPad: Bool) {
//        tf_CreditCard.resetCustomError(isAddAgain: false)
//q
//        if key == "auth" {
//            let Obj = ["cardnumber":tf_CreditCard.text!,"mm":tf_MM.text!, "yyyy":tf_YYYY.text!, "amount": tf_Amount.text!, "cvv":tf_CVV.text!, "auth":"AUTH"]
//            delegate?.getPaymentData?(with: Obj)
//        }
//        else {
//            let Obj = ["cardnumber":tf_CreditCard.text!,"mm":tf_MM.text!, "yyyy":tf_YYYY.text!, "amount": tf_Amount.text!, "cvv":tf_CVV.text!, "auth":"AUTH_CAPTURE", "tip":tipAmount] as [String : Any]
//            delegate?.getPaymentData?(with: Obj)
//        }
        
        //let Obj = ["cardnumber":tf_CreditCard.text!,"mm":tf_MM.text!, "yyyy":tf_YYYY.text!, "amount": tf_Amount.text!, "cvv":tf_CVV.text!, "auth":"AUTH_CAPTURE", "tip":tipAmount] as [String : Any]
        //delegate?.getPaymentData?(with: Obj)
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            self.delegate?.placeOrderForIpad?(with: 1 as AnyObject) //1 for pass dummy value// not for use
        }
    }
    
}

//MARK: UITextFieldDelegate
extension IngenicoViewControllerVC: UITextFieldDelegate {
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resetCustomError(isAddAgain: false)
        textField.hideAssistantBar()
        if textField == txfAmount {
            txfAmount.selectAll(nil)
        }
        if Keyboard._isExternalKeyboardAttached() {
            IQKeyboardManager.shared.enableAutoToolbar = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txfAmount {
            let currentText = textField.text ?? ""
            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
            callValidateToChangeColor()
            let amount = Double(replacementText) ?? 0.0
            appDelegate.CardReaderAmount = amount
            if HomeVM.shared.DueShared > 0 {
                return replacementText.isValidDecimal(maximumFractionDigits: 2) && amount <= HomeVM.shared.DueShared
            } else {
                return replacementText.isValidDecimal(maximumFractionDigits: 2) && amount <= totalAmount
            }
            //return replacementText.isValidDecimal(maximumFractionDigits: 2)
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //Check For External Accessory
        if DataManager.isSplitPayment {
            if textField == txfAmount {
                delegate?.balanceDueRemaining?(with: txfAmount.text?.toDouble() ?? 0.0)
                appDelegate.CardReaderAmount = txfAmount.text?.toDouble() ?? 0.0
            }
        }
        if Keyboard._isExternalKeyboardAttached() {
            textField.resignFirstResponder()
            SwipeAndSearchVC.shared.enableTextField()
            return
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        if DataManager.isSplitPayment {
            if textField == txfAmount {
                delegate?.balanceDueRemaining?(with: txfAmount.text?.toDouble() ?? 0.0)
            }
        }
        disableValidateToChangeColor()
        textField.resignFirstResponder()
        return false
    }
}
