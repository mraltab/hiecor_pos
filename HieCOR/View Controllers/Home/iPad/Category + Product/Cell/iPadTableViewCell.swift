//
//  iPadTableViewCell.swift
//  HieCOR
//
//  Created by Deftsoft on 26/07/18.
//  Copyright © 2018 HyperMacMini. All rights reserved.
//

import UIKit

class iPadTableViewCell: UITableViewCell {
    
    //MARK: IBOutlet
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var collectionViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var textFieldVIew: UIView!
    @IBOutlet weak var textTypeTextView: UITextView!
    
    //MARK: Variables
    var arrayAttributes = [AttributesModel]()
    var arrayAttrData = [AttributeValues]()
    var arrayNewAttributes: AttributesModel?
    var delegate: iPadTableViewCellDelegate?
    var arrayAttributeValue: JSONArray?
    var arraySurchage = [ProductSurchageVariationDetail]()
    var cartProductsArray = Array<Any>()
    var arraySurchageVariation = JSONArray()
    var prizeValue : Double = 0.0
    var strName = ""
    
    //MARK: Class Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
   
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
    }
    
    func getdataSurchargevariation(surchargevalue: [String], arraySurchage: JSONArray) {
        arraySurchageVariation = arraySurchage
        
        print(arraySurchageVariation)
    }
}

//MARK: UICollectionViewDataSource & UICollectionViewDelegate
extension iPadTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayAttrData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "iPadCollectionViewCell", for: indexPath) as! iPadCollectionViewCell
        cell.clickButton.tag = indexPath.row
        cell.clickButton.addTarget(self, action: #selector(self.selectProductItem), for: .touchUpInside)
        
        let dictAttributevalue = arrayAttrData[indexPath.row]
        
        print(dictAttributevalue.attribute_value)
        
        cell.namelabel.text = dictAttributevalue.attribute_value
    
        
        let arrayData  = arrayAttributes as NSArray
        let attributeModel = arrayData[0] as! AttributesModel

        cell.backView.borderColor = UIColor.HieCORColor.blue.colorWith(alpha: 1.0)
        cell.backView.backgroundColor = dictAttributevalue.isSelect ? UIColor.HieCORColor.blue.colorWith(alpha: 1.0) : UIColor.white
        cell.namelabel.textColor = dictAttributevalue.isSelect ? UIColor.white : #colorLiteral(red: 0.3136949539, green: 0.3137450218, blue: 0.3136840463, alpha: 1)

        if attributeModel.attribute_type == "radio" {
            cell.checkButton.setImage(dictAttributevalue.isSelect ? UIImage(named: "checkCircle") : UIImage(named: "unCheckCircle"), for: .normal)
        } else {
            
            for valOne in arraySurchageVariation {
                
                let valId = valOne["attributevalueId"] as! String
                
                if valId == dictAttributevalue.attribute_value_id {
                    cell.namelabel.text = "\(valOne["attributeName"] as! String) (+$\(valOne["surchargePrize"] as! String))"
                }
            }
            
            cell.checkButton.setImage(dictAttributevalue.isSelect ? UIImage(named: "checkSquare") : UIImage(named: "unCheckSquare"), for: .normal)
        }
        
        //collectionView.layoutIfNeeded()
        //collectionView.setNeedsLayout()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        //collectionView.layoutIfNeeded()
        //collectionView.setNeedsLayout()
        
        let dictAttributevalue = arrayAttrData[indexPath.row]
        
        print(dictAttributevalue)
        
        var name = dictAttributevalue.attribute_value ?? ""
        
        for valOne in arraySurchageVariation {
            
            let valId = valOne["attributevalueId"] as! String
            
            if valId == dictAttributevalue.attribute_value_id {
                name = "\(valOne["attributeName"] as! String) (+$\(valOne["surchargePrize"] as! String))"
            }
        }
        
        var newSizer = name.size(withAttributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17.0)])
        newSizer.height = (UI_USER_INTERFACE_IDIOM() == .pad ? 60 : 50)
        newSizer.width += 80
        return newSizer

    }
    
    @objc func selectProductItem(_ sender: UIButton) {
        ////
        
        let dict = arrayAttrData[sender.tag]
        
        print(dict)
        
        var name = dict.attribute_value ?? ""
        print(name)
        
        var prizeVal = 0.0
        
        for valOne in arraySurchageVariation {
            
            let valId = valOne["attributevalueId"] as! String
            
            if valId == dict.attribute_value_id {
                name = "\(valOne["attributeName"] as! String) (+$\(valOne["surchargePrize"] as! String))"
                
                prizeVal = Double(valOne["surchargePrize"] as! String)!
            }
        }
        
        print(name)
        print(prizeValue)
        
        prizeValue =  prizeValue + prizeVal
        print(prizeValue)
        
        /////
        let arrayData  = arrayAttributes as NSArray
        let attributeModel = arrayData[0] as! AttributesModel
        var dictAttributevalue = arrayAttrData[sender.tag]
 
        if dictAttributevalue.isSelect == true, attributeModel.attribute_type != "radio" {
            dictAttributevalue.isSelect = false
        } else {
            dictAttributevalue.isSelect = true
        }
        arrayAttrData.removeAll()
        arrayAttrData = AttributeSubCategory.shared.getUpdateAttribute(with: arrayAttributeValue!, isSelected: dictAttributevalue.isSelect, index: sender.tag, type: attributeModel.attribute_type, attributeId: attributeModel.attribute_id)
        
        let jso = AttributeSubCategory.shared.attributevalueConvertJSon(with: arrayAttrData, attributeId: attributeModel.attribute_id)
        
        print(jso)
        
        arrayAttributes[0].jsonArray = jso

        self.delegate?.didUpdateAttribute(indexPath: IndexPath(row: sender.tag, section: self.collectionView.tag), attributes: arrayAttributes, isRadio: attributeModel.attribute_type, isSelect: dictAttributevalue.isSelect, price: prizeVal)
    }
}

