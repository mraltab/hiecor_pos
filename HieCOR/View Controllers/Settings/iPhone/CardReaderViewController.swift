//
//  CardReaderViewController.swift
//  HieCOR
//
//  Created by Hiecor on 26/03/21.
//  Copyright © 2021 HyperMacMini. All rights reserved.
//


import UIKit
import CoreData
import CoreBluetooth
import CoreBluetooth.CBService
import MobileCoreServices
import IQKeyboardManagerSwift
import ExternalAccessory

class CardReaderViewController: BaseViewController, SWRevealViewControllerDelegate, UITableViewDataSource, UITableViewDelegate {
    
    
    fileprivate var ingenico:Ingenico!
    fileprivate var deviceList:[RUADevice] = []
    let ClientVersion:String  = "4.2.3"
    
    @IBOutlet weak var tbl_CardReaderDevice: UITableView!
    @IBOutlet weak var cardReaderMainView: UIView!
    @IBOutlet weak var cardReaderDeviceViewWidth: NSLayoutConstraint!
    @IBOutlet weak var cardReaderDeviceViewHeight: NSLayoutConstraint!
    
    var cardReaderViewShowHideDelegate: SettingViewControllerDelegate?
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbl_CardReaderDevice.dataSource = self
        tbl_CardReaderDevice.delegate = self
        cardReaderMainView.isHidden = false
        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height
        if UI_USER_INTERFACE_IDIOM() == .pad {
            cardReaderDeviceViewWidth.constant = displayWidth * 0.5
        }else{
            cardReaderDeviceViewWidth.constant = displayWidth - 40
        }
        cardReaderDeviceViewHeight.constant = displayHeight/2
        
    }
    
    @IBAction func btn_cardReaderCancelBtnAction(_ sender: Any) {
        
        cardReaderViewShowHideDelegate?.cardReaderViewShowHide?(with: true)
        
    }
    
    func loginWithUserName( _ uname : String, andPW pw : String){
        
        self.view.endEditing(true)
        //SVProgressHUD.show(withStatus: "Logging")
        Ingenico.sharedInstance()?.user.loginwithUsername(uname, andPassword: pw) { (user, error) in
            //SVProgressHUD.dismiss()
            if (error == nil) {
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                self.setLoggedIn(true)
                
                Indicator.sharedInstance.showIndicator()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    self.checkFirmwareUpdate()
                    self.cardReaderMainView.isHidden = true
                    self.tbl_CardReaderDevice.reloadData()
                    //self.performSegue(withIdentifier:"loginsuccess" , sender: nil)
                }
                
            }else{
                Indicator.sharedInstance.hideIndicator()
                self.navigationItem.rightBarButtonItem?.isEnabled = false
                let nserror = error as NSError?
                let alertController:UIAlertController  = UIAlertController.init(title: "Failed", message: "Login failed, please try again later \(self.getResponseCodeString((nserror?.code)!))", preferredStyle: .alert)
                let okAction:UIAlertAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceList.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tbl_CardReaderDevice.dequeueReusableCell(withIdentifier: "CardReaderDeviceCell", for: indexPath) as! CardReaderDeviceCell
        //cell.deviceNameLbl.text = "device name\([indexPath.row])"
        
        let device:RUADevice = deviceList[(indexPath as NSIndexPath).row]
        if device.name != nil{
            cell.deviceNameLbl.text =  String.init(format: "Name: %@", device.name)
        }
        //cell.identifierLabel.text = String.init(format: "ID: %@", device.identifier)
        // cell.communicationLabel.text = self.getStringFromCommunication(device.communicationInterface)
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("enter")
                    if  deviceList.count > 0 {
                        Indicator.sharedInstance.showIndicator()
                        connectingDevice = deviceList[(indexPath as NSIndexPath).row]
                        ingenico.paymentDevice.select(connectingDevice!)
                        ingenico.paymentDevice.initialize(self)
                        //ingenico.paymentDevice.stopSearch()
        //                if (baseURLTextField.text != nil) {
        //                    UserDefaults.standard.setValue(baseURLTextField.text, forKey: "DefaultURL")
        //                    UserDefaults.standard.synchronize()
        //                }
                        ingenico.initialize(withBaseURL: HomeVM.shared.ingenicoData[0].str_url,
                                            apiKey: HomeVM.shared.ingenicoData[0].str_apikey,
                                            clientVersion: ClientVersion)
                        ingenico.setLogging(false)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                            self.loginWithUserName(HomeVM.shared.ingenicoData[0].str_username, andPW: HomeVM.shared.ingenicoData[0].str_password)
                        }
                        
                    }
    }
    
}

//MARK: IngenicoInfoViewControllerDelegate
extension CardReaderViewController: SettingViewControllerDelegate {
    func IngenicoDeviceDataDelegate() {
        print("call back")
        ingenico = Ingenico.sharedInstance()
        ingenico.setLogging(false)
    }
    
    func IngenicoDeviceDataDelegateData(with data: [RUADevice]) {
        print("call back")
        ingenico = Ingenico.sharedInstance()
        ingenico.setLogging(false)
        deviceList = data
        tbl_CardReaderDevice.reloadData()
    }
}
