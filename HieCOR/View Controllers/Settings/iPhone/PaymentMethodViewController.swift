//
//  PaymentMethodViewController.swift
//  HieCOR
//
//  Created by Hiecor on 19/02/21.
//  Copyright © 2021 HyperMacMini. All rights reserved.
//

import UIKit
import CoreData
import CoreBluetooth
import CoreBluetooth.CBService
import MobileCoreServices
import IQKeyboardManagerSwift

class PaymentMethodViewController: BaseViewController, SWRevealViewControllerDelegate, RUADeviceSearchListener, RUAAudioJackPairingListener, SeetingControllerDelegate, SettingViewControllerDelegate {
    func reloadTableViewForIngenicoCheckBox() {
        tbl_Settings.reloadData()
    }
    
    
    func isReaderSupported(_ reader: RUADevice) -> Bool{
        if (reader.name == nil){
            return false
        }
        print(reader.name)
        return reader.name.lowercased().hasPrefix("rp") || reader.name.lowercased().hasPrefix("mob")
    }
    
    func discoveredDevice(_ reader: RUADevice!) {
        var isIncluded:Bool = false
        for device:RUADevice in deviceList {
            if device.identifier == reader.identifier {
                isIncluded = true
                break
            }
        }
        if !isIncluded {
            if isReaderSupported(reader){
                deviceList.append(reader)
            }
            
        }
        
        print(deviceList)
    }
    
    func discoveryComplete() {
        print(deviceList)
        isDiscoveryComplete = true
    }
    
    
    //MARK: - RUARadioJackPairingListener
    
    func onPairConfirmation(_ readerPasskey: String!, mobileKey mobilePasskey: String!) {
        DispatchQueue.main.async {
            let passKey:String = readerPasskey
            let message : String = String.init(format: "Passkey : %@", passKey)
            let notification:UILocalNotification  = UILocalNotification()
            notification.alertBody = message
            notification.fireDate = Date.init(timeIntervalSinceNow: 1)
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.timeZone = TimeZone.current
            UIApplication.shared.scheduleLocalNotification(notification)
        }
    }
    
    func onPairSucceeded() {
        //   isPaired = true;
        DispatchQueue.main.async {
            //self.deviceStatusLabel.text = "Device Status: Pairing completed.\n Remove reader from  audio jack to test Bluetooth connection.\n Make sure reader is powered on."
        }
    }
    
    func onPairNotSupported() {
        //self.deviceStatusLabel.text = "Device Status: Pairing not supported"
    }
    
    func  onPairFailed() {
        //self.deviceStatusLabel.text = "Device Status: Pairing Failed"
    }
    
    func loginWithUserName( _ uname : String, andPW pw : String){
        
        self.view.endEditing(true)
        //SVProgressHUD.show(withStatus: "Logging")
        Ingenico.sharedInstance()?.user.loginwithUsername(uname, andPassword: pw) { (user, error) in
            //SVProgressHUD.dismiss()
            if (error == nil) {
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                self.setLoggedIn(true)
                
                Indicator.sharedInstance.showIndicator()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    self.checkFirmwareUpdate()
                    self.cardReaderViewShowHideDelegate?.cardReaderViewShowHide?(with: true)
                    // self.cardReaderMainView.isHidden = true // Need sudama for new setting
                    self.tbl_Settings.reloadData()
                    //self.performSegue(withIdentifier:"loginsuccess" , sender: nil)
                }
                
            }else{
                Indicator.sharedInstance.hideIndicator()
                self.navigationItem.rightBarButtonItem?.isEnabled = false
                let nserror = error as NSError?
                let alertController:UIAlertController  = UIAlertController.init(title: "Failed", message: "Login failed, please try again later \(self.getResponseCodeString((nserror?.code)!))", preferredStyle: .alert)
                let okAction:UIAlertAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    
    
    
    
    //MARK: IBOutlet
    @IBOutlet var tbl_Settings: UITableView!
    
    //MARK: Variables
    private var array_PaymentMethods = Array<Any>()
    private var array_iconsList = Array<Any>()
    private var payment_Switch: UISwitch?
    private var isPaxSelected = false
    private var arraySelectedPaymet = [String]()
    private var selectedPaymentMethodIndex = Int()
    fileprivate var ingenico:Ingenico!
    fileprivate var deviceList:[RUADevice] = []
    private var isDiscoveryComplete = false;
    private weak var timer: Timer?
    let ClientVersion:String  = "4.2.3"
    var versionOb = Int()
    
    
    //MARK: Delegate
    var paymentMethodSettingDelegate: SettingViewControllerDelegate?
    var cardReaderViewShowHideDelegate: SettingViewControllerDelegate?
    var paymentToMainiPadIngenicoDelegate: SettingViewControllerDelegate?
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        versionOb = Int(DataManager.appVersion)!
        tbl_Settings.rowHeight = 50
        //  updatePaymentArray()
        
        ingenico = Ingenico.sharedInstance()
        ingenico.setLogging(false)
        deviceList = [RUADevice]()
        delegateCall = self
        timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(SearchAgainIngenicoData), userInfo: nil, repeats: true)
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        DataManager.selectedPayment = arraySelectedPaymet
        timer?.invalidate()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.loadData()
        
    }
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "cardReader"
        {
            let vc = segue.destination as! CardReaderViewController
            paymentToMainiPadIngenicoDelegate = vc
            
        }
    }
    
    private func loadData() {
        arraySelectedPaymet = ["CREDIT"]
        if DataManager.selectedPayment != nil
        {
            arraySelectedPaymet = DataManager.selectedPayment!
        }
        self.updatePaymentArray()
        self.automaticallyAdjustsScrollViewInsets = false
        tbl_Settings.contentInset = UIEdgeInsets.zero
        tbl_Settings.tableFooterView = UIView()
        tbl_Settings.reloadData()
    }
    
    private func removePaymentData(method: String) {
        if let key = PaymentsViewController.paymentDetailDict["key"] as? String {
            if method == key || method == "MULTI CARD" {
                PaymentsViewController.paymentDetailDict.removeAll()
            }
        }
    }
    
    @objc func SearchAgainIngenicoData() {
        if HomeVM.shared.ingenicoData.count > 0 {
            ingenico.initialize(withBaseURL: HomeVM.shared.ingenicoData[0].str_url,
                                apiKey: HomeVM.shared.ingenicoData[0].str_apikey,
                                clientVersion: ClientVersion)
            
            //if communicationSegment.selectedSegmentIndex == 1 {
            // doSearching()
            
            self.ingenico.paymentDevice.setDeviceType(RUADeviceTypeRP45BT)
            
            self.ingenico.paymentDevice.search(self)
        }
    }

    @IBAction func paymentBackAction(_ sender: Any) {
        paymentMethodSettingDelegate?.didMoveToNextScreen?(with: "Checkout Options")
    }
    
    private func updatePaymentArray() {
        self.isPaxSelected = arraySelectedPaymet.contains("PAX PAY")
        
        if isPaxSelected {
            //array_PaymentMethods = ["CREDIT", "CASH", "INVOICE", "ACH CHECK", "GIFT CARD", "EXTERNAL GIFT CARD", "MULTI CARD", "CHECK", "EXTERNAL", "PAX PAY", "     CREDIT", "     DEBIT", "     GIFT" ,"INTERNAL GIFT CARD"]
            if versionOb < 4 {
                array_PaymentMethods = ["CREDIT", "CASH", "INVOICE", "ACH CHECK", "GIFT CARD", "EXTERNAL GIFT CARD", "CHECK", "EXTERNAL", "PAX PAY", "     CREDIT", "     DEBIT", "     GIFT"]
            }else{
                if DataManager.allowIngenicoPaymentMethod == "true" {
                    if DataManager.isCardReader {
                        array_PaymentMethods = ["CREDIT", "CASH", "INVOICE", "ACH CHECK", "GIFT CARD", "EXTERNAL GIFT CARD", "MULTI CARD", "CHECK", "EXTERNAL", "PAX PAY", "     CREDIT", "     DEBIT", "     GIFT" ,"INTERNAL GIFT CARD", "CARD READER", "RP45BT"]
                    }else{
                        array_PaymentMethods = ["CREDIT", "CASH", "INVOICE", "ACH CHECK", "GIFT CARD", "EXTERNAL GIFT CARD", "MULTI CARD", "CHECK", "EXTERNAL", "PAX PAY", "     CREDIT", "     DEBIT", "     GIFT" ,"INTERNAL GIFT CARD", "CARD READER"]
                    }
                } else {
                    array_PaymentMethods = ["CREDIT", "CASH", "INVOICE", "ACH CHECK", "GIFT CARD", "EXTERNAL GIFT CARD", "MULTI CARD", "CHECK", "EXTERNAL", "PAX PAY", "     CREDIT", "     DEBIT", "     GIFT" ,"INTERNAL GIFT CARD"]
                }
            }
        }else {
            // array_PaymentMethods = ["CREDIT", "CASH", "INVOICE", "ACH CHECK", "GIFT CARD", "EXTERNAL GIFT CARD", "MULTI CARD", "CHECK", "EXTERNAL", "PAX PAY","INTERNAL GIFT CARD"]
            if versionOb < 4 {
                array_PaymentMethods = ["CREDIT", "CASH", "INVOICE", "ACH CHECK", "GIFT CARD", "EXTERNAL GIFT CARD", "CHECK", "EXTERNAL", "PAX PAY"]
            }else{
                if DataManager.allowIngenicoPaymentMethod == "true" {
                    if DataManager.isCardReader {
                        array_PaymentMethods = ["CREDIT", "CASH", "INVOICE", "ACH CHECK", "GIFT CARD", "EXTERNAL GIFT CARD", "MULTI CARD", "CHECK", "EXTERNAL", "PAX PAY","INTERNAL GIFT CARD", "CARD READER", "RP45BT"]
                    } else {
                        array_PaymentMethods = ["CREDIT", "CASH", "INVOICE", "ACH CHECK", "GIFT CARD", "EXTERNAL GIFT CARD", "MULTI CARD", "CHECK", "EXTERNAL", "PAX PAY","INTERNAL GIFT CARD", "CARD READER"]
                    }
                } else {
                    array_PaymentMethods = ["CREDIT", "CASH", "INVOICE", "ACH CHECK", "GIFT CARD", "EXTERNAL GIFT CARD", "MULTI CARD", "CHECK", "EXTERNAL", "PAX PAY","INTERNAL GIFT CARD"]
                }
            }
        }
        DataManager.selectedPayment = arraySelectedPaymet
        tbl_Settings.reloadData()
    }
}
//MARK: UITableViewDataSource, UITableViewDelegate
extension PaymentMethodViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_PaymentMethods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_Settings.dequeueReusableCell(withIdentifier: "SettingsTableCell", for: indexPath) as! SettingsTableCell
        
        // let lbl_Name = cell.contentView.viewWithTag(1) as? UILabel
        let switchBtn = cell.contentView.viewWithTag(111) as? UISwitch
        let nameLb = array_PaymentMethods[indexPath.row] as? String
        cell.label?.text = nameLb?.capitalized
        
        cell.switchButton.removeTarget(self, action: #selector(btn_PaymentAction(sender:)), for: .allEvents)
        if array_PaymentMethods[indexPath.row] as? String == "MULTI CARD" {
            cell.label?.text = "SPLIT PAYMENT"
        }
        
        if array_PaymentMethods[indexPath.row] as? String == "GIFT CARD" {
            cell.label?.text = "HEARTLAND GIFT CARD"
        }
        
        cell.switchButton.tag = indexPath.row
        switchBtn?.setOn(arraySelectedPaymet.contains(array_PaymentMethods[indexPath.row] as? String ?? ""), animated: false)
        //payment_Switch = paySwitch
        
        cell.switchButton.isHidden = false
        
        if isPaxSelected && (indexPath.row == 10 || indexPath.row == 11 || indexPath.row == 12) {
            if indexPath.row == 12 {
                cell.switchButton.setOn(DataManager.isPaxPayGiftCard, animated: false)
            }
            let selectedPaymentName = (array_PaymentMethods[indexPath.row] as! String).replacingOccurrences(of: " ", with: "")
            cell.switchButton.setOn(DataManager.selectedPAX.contains(selectedPaymentName), animated: false)
        }
        
        if indexPath.row == 4 {
            cell.switchButton.setOn(DataManager.isGiftCard, animated: false)
        }
        
        
        if isPaxSelected && indexPath.row == 9 {
            // cell.viewLineLeadingConstrant.constant = 50
            cell.switchButton.setOn(true, animated: false)
            
        }else{
            cell.viewLineLeadingConstrant.constant = 20
        }
        
        if !isPaxSelected && indexPath.row == 10 {
            cell.switchButton.setOn(DataManager.isInternalGift, animated: false)
        }
        
        if isPaxSelected && indexPath.row == 13 {
            cell.switchButton.setOn(DataManager.isInternalGift, animated: false)
        }
        
        if isPaxSelected && indexPath.row == 14 {
            cell.switchButton.setOn(DataManager.isCardReader, animated: false)
        }
        
        
        if !isPaxSelected && indexPath.row == 11 {
            cell.switchButton.setOn(DataManager.isCardReader, animated: false)
        }
        
        
        if isPaxSelected && indexPath.row == 10 {
            cell.viewLineLeadingConstrant.constant = 50
            
        }
        if isPaxSelected && indexPath.row == 11 {
            cell.viewLineLeadingConstrant.constant = 50
            
        }
        if isPaxSelected && indexPath.row == 12 {
            cell.viewLineLeadingConstrant.constant = 50
            
        }
        
        
        if DataManager.isCardReader {
            if !isPaxSelected && indexPath.row == 12 {
                cell.viewLineLeadingConstrant.constant = 50
                
            }
            
            if isPaxSelected && indexPath.row == 15 {
                cell.viewLineLeadingConstrant.constant = 50
                
            }
        }
        
        if array_PaymentMethods[indexPath.row] as? String == "RP45BT" {
            cell.switchButton.isHidden = true
            var isSetUp = false
            cell.switchButton.setOn(DataManager.isCardReader, animated: false)
            if self.getIsDeviceConnected() {
                cell.accessoryType =  .checkmark
            } else {
                cell.accessoryType =  .none
            }
            cell.setEditing(false, animated: false)
        } else {
            cell.accessoryType =  .none
            cell.accessoryView = nil
            
        }
        
        //  payment_Switch?.tag = indexPath.row
        print("indexPath.row--\(indexPath.row)")
        
        cell.switchButton?.addTarget(self, action:#selector(btn_PaymentAction(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //                       Indicator.sharedInstance.showIndicator()
        //                        connectingDevice = deviceList[(indexPath as NSIndexPath).row]
        //                        ingenico.paymentDevice.select(connectingDevice!)
        //                        ingenico.paymentDevice.initialize(self)
        //                        //ingenico.paymentDevice.stopSearch()
        //        //                if (baseURLTextField.text != nil) {
        //        //                    UserDefaults.standard.setValue(baseURLTextField.text, forKey: "DefaultURL")
        //        //                    UserDefaults.standard.synchronize()
        //        //                }
        //                        ingenico.initialize(withBaseURL: HomeVM.shared.ingenicoData[0].str_url,
        //                                            apiKey: HomeVM.shared.ingenicoData[0].str_apikey,
        //                                            clientVersion: ClientVersion)
        //                        ingenico.setLogging(false)
        //
        //                        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
        //                            self.loginWithUserName(HomeVM.shared.ingenicoData[0].str_username, andPW: HomeVM.shared.ingenicoData[0].str_password)
        //                        }
        // self.cardReaderMainView.isHidden = true
        
        
        
        
        
        if !isPaxSelected && indexPath.row == 12 {
            if DataManager.isIngenicoConnected {
                //appDelegate.showToast(message: "Device Setup Inside")
                if getIsDeviceConnected() {
                    Indicator.sharedInstance.showIndicator()
                    Ingenico.sharedInstance()?.paymentDevice.checkSetup({ (error, isSetupRequired) in
                        if error != nil {
                            //SVProgressHUD.showError(withStatus: "Check device setup failed")
                            print("setup error")
                            //appDelegate.showToast(message: "Device Setup error")
                            self.loginWithUserName(HomeVM.shared.ingenicoData[0].str_username, andPW: HomeVM.shared.ingenicoData[0].str_password)
                            Indicator.sharedInstance.hideIndicator()
                        }
                        else if isSetupRequired {
                            print("setup required")
                            //appDelegate.showToast(message: "Device Setup required")
                            Indicator.sharedInstance.hideIndicator()
                            self.checkDeviceSetup()
                        } else {
                            print("setup Done")
                            appDelegate.showToast(message: "Device Setup Already Completed")
                            Indicator.sharedInstance.hideIndicator()
                        }
                    })
                    return
                }
            }
            // let IngenicoModelObj = IngenicoModel()
            ingenico.initialize(withBaseURL: HomeVM.shared.ingenicoData[0].str_url,
                                apiKey: HomeVM.shared.ingenicoData[0].str_apikey,
                                clientVersion: ClientVersion)
            
            //if communicationSegment.selectedSegmentIndex == 1 {
            // doSearching()
            
            self.ingenico.paymentDevice.setDeviceType(RUADeviceTypeRP45BT)
            
            self.ingenico.paymentDevice.search(self)
            // cardReaderMainView.isHidden = false
            paymentToMainiPadIngenicoDelegate?.IngenicoDeviceListDelegate?()
            paymentToMainiPadIngenicoDelegate?.IngenicoDeviceDataDelegateData?(with: deviceList)
            cardReaderViewShowHideDelegate?.cardReaderViewShowHide?(with: false)
            //  moveToSyncScreen()
        }
        
        if isPaxSelected && indexPath.row == 15 {
            
            if DataManager.isIngenicoConnected {
                if getIsDeviceConnected() {
                    //appDelegate.showToast(message: "Device Setup Inside")
                    Indicator.sharedInstance.showIndicator()
                    Ingenico.sharedInstance()?.paymentDevice.checkSetup({ (error, isSetupRequired) in
                        if error != nil {
                            //SVProgressHUD.showError(withStatus: "Check device setup failed")
                            print("setup error")
                            //appDelegate.showToast(message: "Device Setup error")
                            self.loginWithUserName(HomeVM.shared.ingenicoData[0].str_username, andPW: HomeVM.shared.ingenicoData[0].str_password)
                            Indicator.sharedInstance.hideIndicator()
                        }
                        else if isSetupRequired {
                            print("setup required")
                            appDelegate.showToast(message: "Device Setup required")
                            Indicator.sharedInstance.hideIndicator()
                            self.checkDeviceSetup()
                        } else {
                            print("setup Done")
                            
                            appDelegate.showToast(message: "Device Setup Already Completed")
                            Indicator.sharedInstance.hideIndicator()
                        }
                    })
                    return
                }
            }
            // let IngenicoModelObj = IngenicoModel()
            ingenico.initialize(withBaseURL: HomeVM.shared.ingenicoData[0].str_url,
                                apiKey: HomeVM.shared.ingenicoData[0].str_apikey,
                                clientVersion: ClientVersion)
            
            //if communicationSegment.selectedSegmentIndex == 1 {
            // doSearching()
            
            self.ingenico.paymentDevice.setDeviceType(RUADeviceTypeRP45BT)
            
            self.ingenico.paymentDevice.search(self)
            //                }else{
            //                    let devicesList: [NSNumber] = [ NSNumber.init(value: RUADeviceTypeRP450c.rawValue as UInt32),
            //                                                    NSNumber.init(value: RUADeviceTypeRP350x.rawValue as UInt32),
            //                                                    NSNumber.init(value: RUADeviceTypeG4x.rawValue as UInt32),
            //                                                    NSNumber.init(value: RUADeviceTypeRP750x.rawValue as UInt32)];
            //                    ingenico.paymentDevice.setDeviceTypes(devicesList)
            //                    let loginVC:LoginViewController = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") as! LoginViewController
            //                    self.navigationController?.pushViewController(loginVC, animated: false)
            //                    ingenico.paymentDevice.initialize(self)
            //                }
            paymentToMainiPadIngenicoDelegate?.IngenicoDeviceListDelegate?()
            paymentToMainiPadIngenicoDelegate?.IngenicoDeviceDataDelegateData?(with: deviceList)
            cardReaderViewShowHideDelegate?.cardReaderViewShowHide?(with: false)
            // moveToSyncScreen()
        }
        
        
        
    }
    
    @objc func btn_PaymentAction(sender: UISwitch)
    {
        if !sender.isOn {
            removePaymentData(method: array_PaymentMethods[sender.tag] as? String ?? "")
        }
        
        if sender.tag == 4 {
            DataManager.isGiftCard = sender.isOn
        }
        
        if !isPaxSelected && sender.tag == 10 {
            DataManager.isInternalGift = sender.isOn
        }
        
        if isPaxSelected && sender.tag == 13 {
            DataManager.isInternalGift = sender.isOn
        }
        if isPaxSelected && sender.tag == 14 {
            DataManager.isCardReader = sender.isOn
        }
        if !isPaxSelected && sender.tag == 11 {
            DataManager.isCardReader = sender.isOn
        }
        
        if isPaxSelected && (sender.tag == 10 || sender.tag == 11 || sender.tag == 12) {
            
            if sender.tag == 12 {
                DataManager.isPaxPayGiftCard = sender.isOn
            }
            
            let selectedPaymentName = (array_PaymentMethods[sender.tag] as! String).replacingOccurrences(of: " ", with: "")
            
            if DataManager.selectedPAX.contains((selectedPaymentName))
            {
                if DataManager.selectedPAX.count < 2 {
                    self.updatePaymentArray()
                    return
                }
                let index = DataManager.selectedPAX.index(of: (selectedPaymentName))
                DataManager.selectedPAX.remove(at: index!)
            }
            else
            {
                DataManager.selectedPAX.append(selectedPaymentName)
            }
            
            self.updatePaymentArray()
            return
        }
        
        if arraySelectedPaymet.contains(array_PaymentMethods[sender.tag] as! String)
        {
            if arraySelectedPaymet.count < 2 {
                self.updatePaymentArray()
                return
            }
            let index = arraySelectedPaymet.index(of: array_PaymentMethods[sender.tag] as! String)
            arraySelectedPaymet.remove(at: index!)
        }
        else
        {
            selectedPaymentMethodIndex = sender.tag
            arraySelectedPaymet.append(array_PaymentMethods[selectedPaymentMethodIndex] as! String)
        }
        
        self.updatePaymentArray()
        
    }
    
}


