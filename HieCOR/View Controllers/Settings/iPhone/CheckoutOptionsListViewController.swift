//
//  CheckoutOptionsListViewController.swift
//  HieCOR
//
//  Created by Hiecor on 17/02/21.
//  Copyright © 2021 HyperMacMini. All rights reserved.
//

import Foundation


class CheckoutOptionsListViewController: BaseViewController, SettingViewControllerDelegate, SWRevealViewControllerDelegate {
    
    //MARK: IBOutlet
    @IBOutlet var tbl_Settings: UITableView!
    @IBOutlet weak var backBtnLeadingContraint: NSLayoutConstraint!
    @IBOutlet weak var backBtn: UIButton!
    
    //MARK: Variables
    private var array_List = Array<Any>()
    private var array_iconsList = Array<Any>()
    
    //MARK: Delegate
    var checkoutOptionsSettingDelegate: SettingViewControllerDelegate?
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        array_List = ["Taxes", "Customer Management", "Enable Auth", "Collect Tips", "Swipe To Pay", "Payment Method", "Signature And Receipt"]
        tbl_Settings.rowHeight = 50
        if UI_USER_INTERFACE_IDIOM() == .pad {
            backBtnLeadingContraint.constant = 20
            backBtn.isHidden = true
        }else {
            backBtnLeadingContraint.constant = 60
            backBtn.isHidden = false
        }
        
    }
    @IBAction func checkoutBackAction(_ sender: Any) {
        checkoutOptionsSettingDelegate?.didMoveToNextScreen?(with: "Setting")
    }
    
    @objc func btn_switchAction(sender: UISwitch)
    {
        if sender.tag == 0 {
            DataManager.isTaxOn = sender.isOn
        }
        if sender.tag == 1 {
            DataManager.isCustomerManagementOn = sender.isOn
            // for Prompt Add Customer
            if !DataManager.isCustomerManagementOn{
                DataManager.isPromptAddCustomer = false
            }
        }
        
        if sender.tag == 2 {
            DataManager.isAuthentication = sender.isOn
        }
        if sender.tag == 3 {
            DataManager.collectTips = sender.isOn
            DataManager.tempCollectTips = sender.isOn
        }
        
        
        if sender.tag == 4 {
            DataManager.isSwipeToPay = sender.isOn
        }
        
    }
    
}
//MARK: UITableViewDataSource, UITableViewDelegate
extension CheckoutOptionsListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_List.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_Settings.dequeueReusableCell(withIdentifier: "SettingsTableCell", for: indexPath) as! SettingsTableCell
        
        let lbl_Name = cell.contentView.viewWithTag(1) as? UILabel
        let switchBtn = cell.contentView.viewWithTag(2) as? UISwitch
        let img_Arrow = cell.contentView.viewWithTag(4) as? UIImageView
      
        lbl_Name?.text = array_List[indexPath.row] as? String
        
        if indexPath.row == 0 {
            switchBtn?.setOn(DataManager.isTaxOn, animated: false)
        }
        if indexPath.row == 1 {
            switchBtn?.setOn(DataManager.isCustomerManagementOn, animated: false)
        }
        if indexPath.row == 2 {
            switchBtn?.setOn(DataManager.isAuthentication, animated: false)
        }
        if indexPath.row == 3 {
            switchBtn?.setOn(DataManager.collectTips, animated: false)
        }
        if indexPath.row == 4 {
            switchBtn?.setOn(DataManager.isSwipeToPay, animated: false)
        }
        
        if array_List[indexPath.row] as? String == "Payment Method" {
            switchBtn?.isHidden = true
            img_Arrow?.isHidden = false
        }
        if array_List[indexPath.row] as? String == "Signature And Receipt" {
            switchBtn?.isHidden = true
            img_Arrow?.isHidden = false
        }
        
        switchBtn?.addTarget(self, action:#selector(btn_switchAction(sender:)), for: .touchUpInside)
        switchBtn?.tag = indexPath.row
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        checkoutOptionsSettingDelegate?.didMoveToNextScreen?(with: array_List[indexPath.row] as! String)
        
    }
    
}

