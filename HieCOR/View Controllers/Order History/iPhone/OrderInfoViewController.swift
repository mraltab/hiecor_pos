//
//  OrderInfoViewController.swift
//  HieCOR
//
//  Created by HyperMacMini on 05/12/17.
//  Copyright © 2017 HyperMacMini. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SafariServices
import CoreBluetooth
import CoreBluetooth.CBService
import FirebaseCore
import FirebaseAnalytics
import Crashlytics

class OrderInfoViewController: BaseViewController, TipViewCellDelegate {
    func didFinishTask() {
        callAPItoGetOrderInfo()
    }
    
    
    let tipCustomView = TipCustomView()
    //MARK: IBOutlets
    @IBOutlet weak var tipButton: UIButton!
    
    @IBOutlet weak var tvAddressBilling: UITextView!
    @IBOutlet weak var tvAddressCustomer: UITextView!
    @IBOutlet weak var tvAddressShipping: UITextView!
    @IBOutlet weak var btnCapture: UIButton!
    @IBOutlet weak var btnClearHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewTotal: UIView!
    @IBOutlet weak var btnRefundHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewFooterUpLine: UIView!
    @IBOutlet var viewAdminComment: UIView!
    // @IBOutlet var viewHeightConstraintRefundExchngeView: NSLayoutConstraint!
    // @IBOutlet var viewLineAdminTop: UIView!
    @IBOutlet var labelHeadAdminComment: UILabel!
    @IBOutlet var stackViewCust: UIStackView!
    @IBOutlet weak var labelUserId: UILabel!
    @IBOutlet weak var viewAddressSeparator: UIView!
    @IBOutlet weak var viewAddressSeparatorOne: UIView!
    @IBOutlet weak var CustomerAddressLabel: UILabel!
    @IBOutlet weak var CustomerHeadLabel: UILabel!
    @IBOutlet weak var billingHeadLabel: UILabel!
    @IBOutlet weak var shippingHeadLabel: UILabel!
    @IBOutlet weak var refundButton: UIButton!
    @IBOutlet weak var receiptButton: UIButton!
    @IBOutlet weak var orderIDlabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var orderSummaryButton: UIButton!
    @IBOutlet weak var transactionButton: UIButton!
    @IBOutlet weak var orderSummaryLineView: UIView!
    @IBOutlet weak var transactionLineView: UIView!
    @IBOutlet weak var orderDetailView: UIView!
    @IBOutlet weak var orderAddressDetailView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var labelAdminComment: UILabel!
    @IBOutlet weak var tableFooterView: UIView!
    @IBOutlet weak var totalFooterLabel: UILabel!
    @IBOutlet weak var orderDateLabel: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var transactionTypeLabel: UILabel!
    @IBOutlet weak var saleLocationLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblCompanyHeader: UILabel!
    @IBOutlet weak var shippingAddressLabel: UILabel!
    @IBOutlet weak var billingAddressLabel: UILabel!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var addressBackView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var tableBackView: UIView!
    @IBOutlet var tableViewheight: NSLayoutConstraint!
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var totalLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet var totalLabelStackViewRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableBackgroundTopImageView: UIImageView!
    @IBOutlet weak var tableBackgroundBottomImageView: UIImageView!
    
    @IBOutlet weak var viewDueDateIphone: UIView!
    @IBOutlet weak var ViewDueDate: UIStackView!
    @IBOutlet weak var lblPaymentTerms: UILabel!
    @IBOutlet weak var viewPaymentTerms: UIView!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblHeaderDueDate: UILabel!
  
    @IBOutlet var saleLocationView: UIView!
    @IBOutlet var companyNameView: UIView!
    @IBOutlet var refundExchangeButtonView: UIStackView!
    @IBOutlet var totalLabelConstraint: NSLayoutConstraint!
    @IBOutlet var noTransactionFoundImageView: UIImageView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var addressviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var orderDetailBackView: UIView!
    @IBOutlet var btnClear: UIButton!
    
    @IBOutlet var adminViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var userContainer: UIView!
    
    @IBOutlet weak var refundSummaryHeightContraints: NSLayoutConstraint!
    @IBOutlet weak var lblRefundSumarryType: UILabel!
    @IBOutlet weak var lblRefundSummaryAmount: UILabel!
    @IBOutlet weak var ViewRefundsumarryAmontConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewRefundSummaryTypeConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var viewUserComment: UIView!
    @IBOutlet weak var lblUserComment: UILabel!
    @IBOutlet weak var lblHeadUserComment: UILabel!
    @IBOutlet weak var constantUserCommentHeight: NSLayoutConstraint!
    
    //MARK: Variables
    var orderInfoModelObj = OrderInfoModel()
    var receiptModel = ReceiptContentModel()
    var transactionInfoArray = [TransactionsDetailModel]()
    var isTapOnTransactionButton = Bool()
    var isOrderSummery = Bool()
    var orderID = String()
    var transactionID = String()
    var status = String()
    var isLandscape = true
    var selectedIndex = Int()
    var index = IndexPath()
    var refundOrder: JSONDictionary?
    var versionOb = Int()
    var defualtViewFlag = false
    var tableFooterViewHeight = 0.0
    var refundAmoutTotal = 0.0
    var arrTransactionData : NSMutableArray = []
    var uncheckedRefund = true
    
    //MARK: Delegate
    var orderInfoDelegate : OrderInfoViewControllerDelegate?
    var catAndProductDelegate: CatAndProductsViewControllerDelegate?
    
    var userDetailDelegate : UserDetailsDelegate?
    
    
    //MARK: PrinterManager
    static var printerManager: PrinterManager?
    static var printerArray = [PrinterStruct]()
    static var printerUUID: UUID? = nil
    
    static var centeralManager: CBCentralManager?
    
    //MARK: Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
         LoadStarPrinter.settingManager.load()
        versionOb = Int(DataManager.appVersion)!
        tableView.dataSource = self
        tableView.delegate = self
        isLandscape = UIScreen.main.bounds.size.width > UIScreen.main.bounds.size.height
        //loadPrinter()
        if DataManager.isBluetoothPrinter {
            loadPrinter()
        }
        customizeUI()
        //Call API
        //Crashlytics.sharedInstance().crash()
        
        //btnClear.backgroundColor = UIColor.gray
        //btnClear.borderColor = UIColor.gray
        //btnClear.isUserInteractionEnabled = false
        //refundButton.isUserInteractionEnabled = false
        
        if UI_USER_INTERFACE_IDIOM() == .phone {
            callAPItoGetOrderInfo()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UI_USER_INTERFACE_IDIOM() == .pad ? .default : .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.isLandscape = UIScreen.main.bounds.size.width > UIScreen.main.bounds.size.height
        self.scrollView.alpha = 0
        self.reloadTableData(animate: false)
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            self.reloadTableData(animate: true)
        }
        
        if appDelegate.strIphoneApi == true {
            callAPItoGetOrderInfo()
            appDelegate.strIphoneApi = false
        }
        
        self.updateView()
        DispatchQueue.main.async(){
            self.updateUI()
        }
        if DataManager.isBluetoothPrinter {
            self.checkPrinterConnection()
        }
        //self.checkPrinterConnection()
        DataManager.collectTips = DataManager.collectTips ? DataManager.collectTips : DataManager.tempCollectTips
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("Data Is value ")
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if UI_USER_INTERFACE_IDIOM() == .pad {
            self.isLandscape = UIScreen.main.bounds.size.width > UIScreen.main.bounds.size.height
            self.scrollView.alpha = 0
            self.reloadTableData(animate: false)
            DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
                self.reloadTableData(animate: true)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "transactioninfo" {
            let vc = segue.destination as! TransactionsInfoViewController
            vc.transactionInfo = transactionInfoArray[selectedIndex]
        }
        
        if segue.identifier == "cart" {
            let vc = segue.destination as! CartViewController
            
            if DataManager.isCaptureButton {
                vc.orderType = .newOrder
            } else {
                vc.orderType = .refundOrExchangeOrder
            }
            
            vc.refundOrder = self.refundOrder
        }
        
        if segue.identifier == "UserDetailViewController" {
            let vc = segue.destination as! UserDetailViewController
            vc.userDetailDelegate = self
            vc.orderInfoDetail = orderInfoModelObj
            userDetailDelegate = vc
        }
    }
    
    //MARK: Private Functions
    private func customizeUI() {
        self.tableView.isScrollEnabled = false
        self.scrollView.isScrollEnabled = true
        isOrderSummery = true
        self.scrollView.alpha = 0
        receiptButton.isUserInteractionEnabled = DataManager.receipt
        receiptButton.alpha = DataManager.receipt ? 1.0 : 0.5
        //Orientationsds
        isLandscape = UIScreen.main.bounds.size.width > UIScreen.main.bounds.size.height
        
        //Offline
        if !NetworkConnectivity.isConnectedToInternet() && DataManager.isOffline {
            blurView.backgroundColor = UIColor.white
            blurView.isHidden = false
        }
        
        //Add Shadow
        if UI_USER_INTERFACE_IDIOM() == .pad {
            self.addressBackView.layer.shadowColor = UIColor.darkGray.cgColor
            self.addressBackView.layer.shadowOffset = CGSize.zero
            self.addressBackView.layer.shadowOpacity = 0.3
            self.addressBackView.layer.shadowRadius = 2
            
            self.viewAdminComment.layer.shadowColor = UIColor.darkGray.cgColor
            self.viewAdminComment.layer.shadowOffset = CGSize.zero
            self.viewAdminComment.layer.shadowOpacity = 0.3
            self.viewAdminComment.layer.shadowRadius = 2
        } else {
            self.orderDetailBackView.layer.shadowColor = UIColor.darkGray.cgColor
            self.orderDetailBackView.layer.shadowOffset = CGSize.zero
            self.orderDetailBackView.layer.shadowOpacity = 0.5
            self.orderDetailBackView.layer.shadowRadius = 5
            
            self.viewAdminComment.layer.shadowColor = UIColor.darkGray.cgColor
            self.viewAdminComment.layer.shadowOffset = CGSize.zero
            self.viewAdminComment.layer.shadowOpacity = 0.5
            self.viewAdminComment.layer.shadowRadius = 5
        }
    }
    
    private func checkPrinterConnection() {
        if PrintersViewController.centeralManager == nil || PrintersViewController.printerManager == nil {
            return
        }
        
        if let manager = PrintersViewController.printerManager, !manager.canPrint {
            if let index = PrintersViewController.printerArray.firstIndex(where: {$0.identifier == PrintersViewController.printerUUID}) {
                DispatchQueue.main.async {
                    PrintersViewController.printerManager?.connect(PrintersViewController.printerArray[index])
                }
            }
        }
    }
    
    private func handleOrderSummaryButtonAction() {
        if UI_USER_INTERFACE_IDIOM() == .pad {
            //             if orderInfoModelObj.isBillingSame == 0{
            //              self.addressviewHeightConstraint.constant = 220 //220
            //            }
            transactionLineView.backgroundColor = #colorLiteral(red: 0.9371728301, green: 0.9373074174, blue: 0.9371433854, alpha: 1)
        } else {
            transactionLineView.isHidden = true
            orderSummaryLineView.isHidden = false
        }
        isOrderSummery = true
        self.tableView.isScrollEnabled = false
        self.scrollView.isScrollEnabled = true
        noTransactionFoundImageView.isHidden = true
        orderDetailView.isHidden = false
        tableFooterView.isHidden = false
        //  orderAddressDetailView.isHidden = false
        
        
        transactionButton.setTitleColor(#colorLiteral(red: 0.5489655137, green: 0.5647396445, blue: 0.603846848, alpha: 1), for: .normal)
        orderSummaryButton.setTitleColor(UIColor.HieCORColor.blue.colorWith(alpha: 1.0), for: .normal)
        orderSummaryLineView.backgroundColor = UIColor.HieCORColor.blue.colorWith(alpha: 1.0)
        
        if orderInfoModelObj.paymentStatus == "INVOICE" {
            tableView.tableHeaderView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat(120))
        } else {
            tableView.tableHeaderView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat(100))
        }
        scrollView.alpha = 0
        reloadTableData(animate: false)
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            self.reloadTableData(animate: true)
        }
    }
    
    private func updateView() {
        
//        if UI_USER_INTERFACE_IDIOM() == .pad {
//
//            //            if orderInfoModelObj.isBillingSame == 0{
//            //                self.addressviewHeightConstraint.constant = 350 //220
//            //            }
//            if isLandscape {
//                totalLabelWidthConstraint.isActive = true
//                totalLabelStackViewRightConstraint.isActive = false
//                //ViewRefundsumarryAmontConstraint.isActive = true
//                if isOrderSummery {
//                    tableViewTopConstraint.constant = 30
//                    tableViewLeftConstraint.constant = 15
//                    tableViewRightConstraint.constant = 15
//                    //  tableViewBottomConstraint.constant = 30
//                    tableBackgroundTopImageView.isHidden = false
//                    tableBackgroundBottomImageView.isHidden = false
//                }else {
//                    tableViewTopConstraint.constant = 0
//                    tableViewLeftConstraint.constant = 0
//                    tableViewRightConstraint.constant = 0
//                    //tableViewBottomConstraint.constant = 0
//                    tableBackgroundTopImageView.isHidden = true
//                    tableBackgroundBottomImageView.isHidden = true
//                }
//            }else {
//                tableViewTopConstraint.constant = 0
//                tableViewLeftConstraint.constant = 0
//                tableViewRightConstraint.constant = 0
//                // tableViewBottomConstraint.constant = 0
//                totalLabelWidthConstraint.isActive = false
//                totalLabelStackViewRightConstraint.isActive = true
//                tableBackgroundTopImageView.isHidden = true
//                tableBackgroundBottomImageView.isHidden = true
//                //                if ViewRefundsumarryAmontConstraint.isActive {
//                //                    ViewRefundsumarryAmontConstraint.isActive = false
//                //                }
//
//            }
//        }
        self.updateUI()
    }
    
    func resetAllValues() {
        
        for i in 0..<orderInfoModelObj.productsArray.count {
            orderInfoModelObj.productsArray[i].isExchangeSelected = false
            orderInfoModelObj.productsArray[i].isRefundSelected = false
        }
        
        for i in 0..<orderInfoModelObj.transactionArray.count {
            orderInfoModelObj.transactionArray[i].isPartialRefundSelected = false
            orderInfoModelObj.transactionArray[i].isFullRefundSelected = false
            orderInfoModelObj.transactionArray[i].isVoidSelected = false
            orderInfoModelObj.transactionArray[i].partialAmount = ""
        }
        
        tableView.reloadData()
    }
    
    private func updateUI() {
        
        arrTransactionData.removeAllObjects()
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            userContainer.isHidden = true
        }
        //self.btnCapture.isHidden = true
        //hide for this release
                if orderInfoModelObj.paymentStatus == "AUTH" {
                    self.btnCapture.isHidden = true
                }else{
                    self.btnCapture.isHidden = true
                }
        
        
        if versionOb < 4 {
            if UI_USER_INTERFACE_IDIOM() == .phone {
                //self.refundButton.isHidden = true
                //self.btnClear.isHidden = true
                //self.btnClearHeightConstraint.constant = 0
                //self.btnRefundHeightConstraint.constant = 0
                tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: 30)
                view.layoutIfNeeded()
            }else{
                //self.refundButton.isHidden = true
                //self.btnClear.isHidden = true
                tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: 110)
            }
        }else{
            //self.refundButton.isHidden = false
            //self.btnClear.isHidden = false
            tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: 0)

//            if UI_USER_INTERFACE_IDIOM() == .phone {
//                tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: 70)
//            }else{
//                tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: 0)
//            }
            // tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: 120)
            //tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat((orderInfoModelObj.paymentStatus.lowercased() == "invoice" || !orderInfoModelObj.isOrderRefund) ? 40 : 250))
        }
        if defualtViewFlag {
            orderInfoDelegate?.didUpdateHeaderText?(with: "Orders")
        }else{
            orderInfoDelegate?.didUpdateHeaderText?(with: "#\(orderInfoModelObj.orderID)")
        }
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let newDate = dateFormatter.date(from: orderInfoModelObj.orderDate) ?? Date()
        
        orderDateLabel.text = newDate.stringFromDate(format: .mdyDate, type: .local)
        orderIDlabel.text = "#\(orderInfoModelObj.orderID)"
        orderIdLabel.text = "#\(orderInfoModelObj.orderID)"
        //transactionTypeLabel.text = orderInfoModelObj.transactionType
        saleLocationLabel.text = orderInfoModelObj.saleLocation
        companyNameLabel.text = orderInfoModelObj.companyName
        //totalFooterLabel.text = orderInfoModelObj.total.currencyFormat
        lblPaymentStatus.text = orderInfoModelObj.paymentStatus
        lblOrderStatus.text = orderInfoModelObj.status
        
        if lblPaymentStatus.text == "INVOICE" {
            if orderInfoModelObj.invoiceDueDate == "" {
                if UI_USER_INTERFACE_IDIOM() == .phone {
                    viewDueDateIphone.isHidden = true
                }else{
                    ViewDueDate.isHidden = true
                }
                lblDueDate.isHidden = true
                lblHeaderDueDate.isHidden = true
                view.setNeedsLayout()
                view.setNeedsDisplay()
                view.layoutIfNeeded()
            }else{
                if UI_USER_INTERFACE_IDIOM() == .phone {
                    viewDueDateIphone.isHidden = false
                }else{
                    ViewDueDate.isHidden = false
                }
                lblDueDate.isHidden = false
                lblHeaderDueDate.isHidden = false
                lblDueDate?.text = orderInfoModelObj.invoiceDueDate
                view.setNeedsLayout()
                view.setNeedsDisplay()
                view.layoutIfNeeded()
            }
            if orderInfoModelObj.invoiceterms == ""{
                //            if UI_USER_INTERFACE_IDIOM() == .phone {
                //                viewDueDateIphone.isHidden = true
                //            }else{
                //                ViewDueDate.isHidden = true
                //            }
                viewPaymentTerms.isHidden = true
                view.setNeedsLayout()
                view.setNeedsDisplay()
                view.layoutIfNeeded()
                
            }else{
                if UI_USER_INTERFACE_IDIOM() == .phone {
                    viewDueDateIphone.isHidden = false
                }else{
                    ViewDueDate.isHidden = false
                }
                viewPaymentTerms.isHidden = false
                lblPaymentTerms.text = orderInfoModelObj.invoiceterms
                view.setNeedsLayout()
                view.setNeedsDisplay()
                view.layoutIfNeeded()
            }
        } else {
            if UI_USER_INTERFACE_IDIOM() == .phone {
                viewDueDateIphone.isHidden = true
            }else{
                ViewDueDate.isHidden = true
            }
            viewPaymentTerms.isHidden = true
        }
        
        let orderRefund : [RefundTransactionList] = orderInfoModelObj.refundTransactionList
        print(orderRefund)
        btnPayNow.isHidden = true
        if orderRefund.count > 0 {
            print(orderRefund[0].amount)
            var amount = ""
            var type = ""
            var changeDueAmt = ""
            refundAmoutTotal = 0.0
            var isCashPaid = false
            
            if orderRefund.count == 1 {
                //refundSummaryHeightContraints.constant = 40
            } else {
                //refundSummaryHeightContraints.constant = CGFloat(orderRefund.count * 65)
            }
            
            for i in 0..<orderRefund.count {
                
                if orderRefund[i].card_type  == "Paid by Cash" || orderRefund[i].card_type  == "Paid by cash" {
                    isCashPaid = true
                }
                
                let val = Double(orderRefund[i].amount)!
                refundAmoutTotal += val
                
                if orderRefund.count == 1 {
                    amount = "\(val.currencyFormat)"
                    type = "\(orderRefund[i].card_type)"
                } else {
                    amount = "\(amount) \n \(val.currencyFormat)\n"
                    type = "\(type) \n \(orderRefund[i].card_type)\n"
                }
                
                let responseMessages = ["amount": "\(val.currencyFormat)",
                                        "type": "\(orderRefund[i].card_type)",
                                        "date": "\(orderRefund[i].date)",
                                        "txn_id": "\(orderRefund[i].txn_id)",
                                        "isTypeEVM": "\(orderRefund[i].is_txn_type_EMV)",
                                        "cardNumber": "\(orderRefund[i].card_number)",
                                        "isTipBtn": "\(orderRefund[i].show_tip_button)",
                                        "isShowPaxDevice": "\(orderRefund[i].show_pax_device)"]
                
                arrTransactionData.add(responseMessages)
            }
            
            print(amount)
            print(type)
            print(refundAmoutTotal)
            
            if isCashPaid {
                
                let hh = orderInfoModelObj.changeDue.replacingOccurrences(of: ",", with: "")
                
                if hh != "" {
                    //let jk = Float(hh)
                    let chnageDueVal : Double = Double(hh)!
                    
                    if chnageDueVal > 0{
                        
                        if orderRefund.count == 1 {
                            //refundSummaryHeightContraints.constant += 30
                        }
                        changeDueAmt = "\(chnageDueVal.currencyFormat)"
                        amount = "\(amount) \n \(changeDueAmt)\n"
                        type = "\(type) \nChange\n"
                        
                        if changeDueAmt != "$0.00" {
                            let responseMessages = ["amount": "\(chnageDueVal.currencyFormat)",
                                "type": "Change",
                                "date": "---",
                                "isTipBtn": false] as [String : Any]
                            
                            arrTransactionData.add(responseMessages)
                        }
                    }
                }
            }
            
            //                        amount = "\(amount) \n \(refundAmoutTotal.currencyFormat)\n"
            //                        type = "\(type) \n Total Refund Amount\n"
            
            let balDue = orderInfoModelObj.balanceDue.replacingOccurrences(of: ",", with: "")
            
            if balDue != "" {
                //let jk = Float(hh)
                let balDueVal : Double = Double(balDue)!
                
                if balDueVal > 0 {
                    print(balDue)
                    //refundSummaryHeightContraints.constant += 30
                    btnPayNow.isHidden = false
                    amount = "\(amount) \n \(balDueVal.currencyFormat)"
                    //changeDueAmt = "\(balDueVal.currencyFormat)"
                    type = "\(type) \nBalance Due\n"
                    
                    let responseMessages = ["amount": "\(balDueVal.currencyFormat)",
                        "type": "Balance Due",
                        "date": "---",
                        "isTipBtn": false] as [String : Any]
                    
                    arrTransactionData.add(responseMessages)
                    
                    
                    tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: 46)
                    btnPayNow.isHidden = false
                }
            }
            
            //lblRefundSumarryType.text = type
            //lblRefundSummaryAmount.text = "\(amount)"
            
        } else {
            //refundSummaryHeightContraints.constant = 0

            let balDue = orderInfoModelObj.balanceDue.replacingOccurrences(of: ",", with: "")
            
            var balData = ""
            var typeDue = ""
            
            if balDue != "" {
                //let jk = Float(hh)
                let balDueVal : Double = Double(balDue)!
                
                if balDueVal > 0 {
                    print(balDue)
                    btnPayNow.isHidden = false
                    //refundSummaryHeightContraints.constant = 50
                    balData = "\(balDueVal.currencyFormat)"
                    typeDue = "Balance Due"
                    
                    let responseMessages = ["amount": "\(balDueVal.currencyFormat)",
                        "type": "Balance Due",
                        "date": "---",
                        "isTipBtn": false] as [String : Any]
                    
                    arrTransactionData.add(responseMessages)
                }
            }
            
            //lblRefundSumarryType.text = typeDue
            //lblRefundSummaryAmount.text = balData
        }
        
        if UI_USER_INTERFACE_IDIOM() == .phone {
            if orderInfoModelObj.admin_comments == ""{
                labelAdminComment.isHidden = true
                labelHeadAdminComment.isHidden = true
                viewAdminComment.isHidden = true
                adminViewHeightConstraint.constant = 0
                view.setNeedsLayout()
                view.setNeedsDisplay()
                view.layoutIfNeeded()
            }else{
                viewAdminComment.isHidden = false
                labelAdminComment.isHidden = false
                labelHeadAdminComment.isHidden = false
                labelAdminComment.text = orderInfoModelObj.admin_comments
                //                adminViewHeightConstraint.constant = 128
                //                view.setNeedsLayout()
                //                view.setNeedsDisplay()
            }
            
            if orderInfoModelObj.user_comments == ""{
                lblUserComment.isHidden = true
                lblHeadUserComment.isHidden = true
                viewUserComment.isHidden = true
                constantUserCommentHeight.constant = 0
                view.setNeedsLayout()
                view.setNeedsDisplay()
                view.layoutIfNeeded()
                
            }else{
                viewUserComment.isHidden = false
                lblUserComment.isHidden = false
                lblHeadUserComment.isHidden = false
                lblUserComment.text = orderInfoModelObj.user_comments
                //                adminViewHeightConstraint.constant = 128
                //                view.setNeedsLayout()
                //                view.setNeedsDisplay()
            }
        }else{
            if orderInfoModelObj.admin_comments == ""{
                labelAdminComment.isHidden = true
                labelHeadAdminComment.isHidden = true
                viewAdminComment.isHidden = true
                adminViewHeightConstraint.constant = 0
                view.setNeedsLayout()
                view.setNeedsDisplay()
                view.layoutIfNeeded()
                
            }else{
                viewAdminComment.isHidden = false
                labelAdminComment.isHidden = false
                labelHeadAdminComment.isHidden = false
                labelAdminComment.text = orderInfoModelObj.admin_comments
                //                adminViewHeightConstraint.constant = 128
                //                view.setNeedsLayout()
                //                view.setNeedsDisplay()
            }
            
            if orderInfoModelObj.user_comments == ""{
                lblUserComment.isHidden = true
                lblHeadUserComment.isHidden = true
                viewUserComment.isHidden = true
                constantUserCommentHeight.constant = 0
                view.setNeedsLayout()
                view.setNeedsDisplay()
                view.layoutIfNeeded()
                
            }else{
                viewUserComment.isHidden = false
                lblUserComment.isHidden = false
                lblHeadUserComment.isHidden = false
                lblUserComment.text = orderInfoModelObj.user_comments
                //                adminViewHeightConstraint.constant = 128
                //                view.setNeedsLayout()
                //                view.setNeedsDisplay()
            }
        }
        //saleLocationView.isHidden = saleLocationLabel.text == ""
        
        companyNameView.isHidden = companyNameLabel.text == ""
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            if companyNameLabel.text == "" {
                lblCompanyHeader.text = "Sale Location:"
                companyNameLabel.text = orderInfoModelObj.saleLocation
            } else {
                lblCompanyHeader.text = "Company:"
            }
        }
        
        
        var shipping = String()
        var billing = String()
        var custInfo = String()
        var custBillingInfo = String()
        
        let custfsname = orderInfoModelObj.str_first_name
        let custlsname = orderInfoModelObj.str_last_name
        let custphone = orderInfoModelObj.str_phone
        let custemail = orderInfoModelObj.str_email
        
        if custfsname.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            custInfo += custfsname
        }
        
        if custlsname.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            custInfo += " " + custlsname + "\n"
        }
        
        if custlsname == "" {
            custInfo += "\n"
        }
        
        if custphone.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            custInfo += formattedPhoneNumber(number: custphone) + "\n"
        }
        
        if custemail.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            custInfo += custemail
        }
        
        //MARK: Shipping
        
        //let name = orderInfoModelObj.shippingFirstName + " " + orderInfoModelObj.shippingLastName
        let fsname = orderInfoModelObj.shippingFirstName
        let lsname = orderInfoModelObj.shippingLastName
        let address1 = orderInfoModelObj.shippingAddressLine1
        let address2 = orderInfoModelObj.shippingAddressLine2
        let city = orderInfoModelObj.shippingCity
        let region = orderInfoModelObj.shippingRegion
        let country = orderInfoModelObj.shippingCountry
        let postalCode = orderInfoModelObj.shippingPostalCode
        let phone = orderInfoModelObj.shippingPhone
        let email = orderInfoModelObj.shippingEmail
        
        //        if name.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
        //            shipping += name + "\n"
        //        }
        
        
        if fsname.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            shipping += fsname + " "
        }
        if lsname.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            shipping += lsname + "\n"
        }
        //        if phone.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
        //            shipping += phone + "\n"
        //        }
        //        if email.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
        //            shipping += email + "\n"
        //        }
        
        if address2 == ""{
            if address1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                shipping += address1  +  "\n"
            }
        }else{
            if address1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                shipping +=  address1 +  "\n"
            }
        }
        
        if address2.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            shipping +=   address2 + "\n"
        }
        
        if city.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            shipping +=  city
        }
        
        if region.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            shipping += ", " + region
        }
        
        if country.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            shipping += ", " + country
        }
        
        if postalCode.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            shipping +=  ", " + formattedZIPCodeNumber(number: postalCode)
        }
        print("shipping info ",shipping)
        //MARK: cust Billing
        
        //let billingName = orderInfoModelObj.firstName + " " +  orderInfoModelObj.lastName
        let fbillingName = orderInfoModelObj.firstName
        let lbillingName = orderInfoModelObj.lastName
        let billingAddress1 = orderInfoModelObj.addressLine1
        let billingAddress2 = orderInfoModelObj.addressLine2
        let billingCity = orderInfoModelObj.city
        let billingRegion = orderInfoModelObj.region
        let billingCountry = orderInfoModelObj.country
        let billingPostalCode = orderInfoModelObj.postalCode
        let billingPhone = orderInfoModelObj.phone
        let billingEmail = orderInfoModelObj.email
        let companyName = orderInfoModelObj.companyName
        
        
        if fbillingName != "" && lbillingName != "" && billingEmail != "" && companyName != "" && billingPhone != "" && billingCountry != ""  {
            
            if billingEmail.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                custBillingInfo +=  billingEmail + "\n"
            }
            if billingPhone.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                custBillingInfo += phoneNumberFormateRemoveText(number: billingPhone) + "\n"
            }
            if companyName.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                custBillingInfo += companyName + "\n"
            }
            
            
            
        }else{
            //*priya
            if fbillingName != "" && lbillingName != "" && billingPhone != ""{
                if billingPhone.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                    custBillingInfo += phoneNumberFormateRemoveText(number: billingPhone) + "\n"
                }
            }
            //            if fbillingName != "" && billingPhone != "" {
            //                if billingPhone.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            //                    custBillingInfo += phoneNumberFormateRemoveText(number: billingPhone) + "\n"
            //                }
            //            }
            //*
            if fbillingName != "" && lbillingName != "" && billingCountry != "" && companyName != "" && billingPhone != "" {
                if billingPhone.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                    custBillingInfo += phoneNumberFormateRemoveText(number: billingPhone) + "\n"
                }
                if companyName.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                    custBillingInfo += companyName + "\n"
                }
                
            }else{
                if fbillingName != "" && lbillingName != "" && billingCountry != ""   {
                    if billingEmail.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                        custBillingInfo +=  billingEmail + "\n"
                    }
                    //*priya
                    if fbillingName != "" && lbillingName != "" && companyName != ""{
                        if companyName.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                            custBillingInfo += companyName + "\n"
                        }
                    }
                    
                    if fbillingName != "" && lbillingName != "" && billingCity != ""{
                        if billingCity.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                            custBillingInfo +=  billingCity + "\n"
                        }
                    }
                    if fbillingName != "" && lbillingName != "" && billingPostalCode  != ""{
                        if billingPostalCode.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                            custBillingInfo += formattedZIPCodeNumber(number: billingPostalCode) + "\n"
                        }
                    }
                    
                    if fbillingName != "" && lbillingName != "" && billingRegion  != ""{
                        if billingRegion.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                            custBillingInfo +=  billingRegion + "\n"
                        }
                    }
                    
                    //*
                }else{
                    if billingEmail != "" {
                        if billingEmail.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                            custBillingInfo +=  billingEmail + "\n"
                        }
                        if companyName.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                            custBillingInfo += companyName + "\n"
                        }
                    }
                    if billingPhone != "" {
                        if billingPhone.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                            custBillingInfo += phoneNumberFormateRemoveText(number: billingPhone)
                        }
                    }
                }
            }
            
        }
        
        if billingAddress2 == ""{
            if billingAddress1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                custBillingInfo += billingAddress1 +  "\n"
            }
        }else{
            if billingAddress1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                custBillingInfo +=  billingAddress1 +  "\n"
            }
        }
        if billingAddress2.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            custBillingInfo +=   billingAddress2 +  "\n"
        }
        
        
        //        if lbillingName != "" && billingAddress1 != "" && billingAddress2 != "" && billingCity != "" {
        //            if billingCity.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
        //                custBillingInfo +=  billingCity + ", "
        //            }
        //        }
        
        if fbillingName != "" && lbillingName != "" && companyName != "" && billingPhone != "" && billingCountry != "" && billingCity != "" && billingRegion != "" && billingPostalCode != "" {
            if billingCity.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                custBillingInfo +=  billingCity + ", "
            }
            
            if billingRegion.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                custBillingInfo +=  billingRegion + ", "
            }
            
            if billingPostalCode.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                custBillingInfo += formattedZIPCodeNumber(number: billingPostalCode) + "\n"
            }
            //            if billingCountry.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            //                custBillingInfo +=   "," + billingCountry
            //            }
            print("custBillingInfo info ",custBillingInfo)
            /*
             // by priya
             if fbillingName != "" && lbillingName != ""  && billingCountry != "" && billingCity != ""  && billingPostalCode != "" {
             
             if companyName.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
             custBillingInfo += companyName + "\n"
             }
             if billingCity.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
             custBillingInfo +=  billingCity + ", "
             }
             if billingPostalCode.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
             custBillingInfo += formattedZIPCodeNumber(number: billingPostalCode)
             }
             }
             if fbillingName != "" && companyName != "" && billingPostalCode != "" && billingRegion != ""{
             if companyName.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
             custBillingInfo += companyName + "\n"
             }
             if billingRegion.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
             custBillingInfo +=  billingRegion + ", "
             }
             if billingPostalCode.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
             custBillingInfo += formattedZIPCodeNumber(number: billingPostalCode)
             }
             
             }
             if fbillingName != "" && lbillingName != "" && billingPhone != "" && billingCountry != ""  {
             
             if billingPhone.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
             custBillingInfo += phoneNumberFormateRemoveText(number: billingPhone) + "\n"
             }
             if billingCountry.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
             custBillingInfo +=   billingCountry
             }
             }
             */
            
        }
        else{
            
            if billingAddress1 != "" && billingAddress2 != "" && billingCity != "" && billingRegion != ""{
                //            if billingAddress2 == ""{
                //                if billingAddress1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                //                    custBillingInfo += billingAddress1 +  "\n"
                //                }
                //            }else{
                //                if billingAddress1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                //                    custBillingInfo +=  billingAddress1 +  "\n"
                //                }
                //            }
                //            if billingAddress2.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                //                custBillingInfo +=   billingAddress2 +  "\n"
                //            }
                if billingCity.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                    custBillingInfo +=  billingCity + ", "
                }
                if billingRegion.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                    custBillingInfo +=  billingRegion + ", "
                }
                if billingCountry.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                    custBillingInfo +=   billingCountry
                }
            }
            
            
            //            if billingCountry != ""{
            //                self.viewAddressSeparator.isHidden = true
            //                self.addressBackView.isHidden = true
            //                self.orderAddressDetailView.isHidden = true
            //                self.view.layoutIfNeeded()
            //                self.view.setNeedsLayout()
            //                self.view.setNeedsDisplay()
            //
            //            }
            
            
        }
        
        
        //        if orderInfoModelObj.str_first_name == "" && orderInfoModelObj.str_last_name == "" && orderInfoModelObj.str_phone == "" && orderInfoModelObj.str_email == "" && orderInfoModelObj.shippingAddressLine1 == "" && orderInfoModelObj.shippingAddressLine1 == ""{
        //            self.viewAddressSeparator.isHidden = true
        //            self.addressBackView.isHidden = true
        //            self.orderAddressDetailView.isHidden = true
        //
        //        }else {
        //            self.addressBackView.isHidden = false
        //            self.orderAddressDetailView.isHidden = false
        //            self.view.layoutIfNeeded()
        //            self.view.setNeedsLayout()
        //            self.view.setNeedsDisplay()
        //        }
        
        
        //Mark Billing info....
        
        if fbillingName.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            billing += fbillingName + " "
        }
        if lbillingName.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            billing += lbillingName + "\n"
        }
        
        if billingAddress2 == ""{
            if billingAddress1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                billing += billingAddress1 +  "\n"
            }
        }else{
            if billingAddress1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                billing +=  billingAddress1 +  "\n"
            }
        }
        
        if billingAddress2.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            billing +=   billingAddress2 + "\n"
        }
        
        if billingCity.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            billing += billingCity
        }
        
        if billingRegion.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            billing += ", " + billingRegion
        }
        
        if billingCountry.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            billing += ", " + billingCountry
        }
        
        if billingPostalCode.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            billing += ", " +  formattedZIPCodeNumber(number: billingPostalCode)
        }
        
        print("billing info ",billing)
        if UI_USER_INTERFACE_IDIOM() == .pad {
            //totalLabelConstraint.constant = orderInfoModelObj.discount <= 0 ? 80 : 100
            totalAmountLabel.text = orderInfoModelObj.total.currencyFormat
            //viewRefundSummaryTypeConstraint.constant = orderInfoModelObj.discount <= 0 ? 130 : 150
        }
        labelUserId.text = "#\(orderInfoModelObj.userID)"
        self.addressBackView.isHidden = true
        self.orderAddressDetailView.isHidden = true
        
        //        if UI_USER_INTERFACE_IDIOM() == .pad {
        //            if orderInfoModelObj.isBillingSame == 0{
        //                self.addressBackView.isHidden = false
        //                self.orderAddressDetailView.isHidden = false
        //                self.shippingHeadLabel.isHidden = false
        //                self.tvAddressShipping.isHidden = false
        //
        //                self.billingHeadLabel.isHidden = false
        //                self.tvAddressBilling.isHidden = false
        //                self.viewAddressSeparator.isHidden = false
        //                self.viewAddressSeparatorOne.isHidden = false
        //
        //                self.CustomerHeadLabel.isHidden = false
        //                self.tvAddressCustomer.isHidden = false
        //                self.viewAddressSeparatorOne.isHidden = false
        //                self.stackViewCust.isHidden = false
        //                self.CustomerHeadLabel.text = " \(fbillingName.capitalized + " " + lbillingName.capitalized)"
        //                self.tvAddressCustomer.text = (custBillingInfo == "" || custBillingInfo == " \n") ? "" : custBillingInfo
        //                self.tvAddressShipping.text = (shipping == "" || shipping == " \n") ? "" : shipping //" \nUS, "
        //                self.tvAddressBilling.text = (billing == "" || billing == " \n") ? "" : billing
        //                self.tvAddressCustomer.sizeToFit()
        //                self.tvAddressShipping.sizeToFit()
        //                self.tvAddressShipping.sizeToFit()
        //
        //            }else{
        //                self.billingHeadLabel.isHidden = true
        //                self.tvAddressBilling.isHidden = true
        //                self.shippingHeadLabel.isHidden = true
        //                self.tvAddressShipping.isHidden = true
        //                self.viewAddressSeparator.isHidden = false
        //                self.orderAddressDetailView.isHidden = false
        //                self.addressBackView.isHidden = false
        //                self.CustomerHeadLabel.isHidden = false
        //                self.tvAddressCustomer.isHidden = false
        //                self.viewAddressSeparatorOne.isHidden = true
        //                self.viewAddressSeparator.isHidden = true
        //                self.stackViewCust.isHidden = false
        //                self.CustomerHeadLabel.numberOfLines = 0
        //                //self.customerHeadWidthConstraint.constant = 300
        //                self.CustomerHeadLabel.text = " \(fbillingName.capitalized + " " + lbillingName.capitalized)"
        //                self.tvAddressCustomer.text = (custBillingInfo == "" || custBillingInfo == " \n") ? "" : custBillingInfo
        //                self.tvAddressCustomer.sizeToFit()
        //                self.tvAddressShipping.sizeToFit()
        //                self.tvAddressShipping.sizeToFit()
        //
        //            }
        //
        //            if self.tvAddressShipping.text == "" &&   self.tvAddressCustomer.text == "" &&  self.tvAddressBilling.text == "" && self.CustomerHeadLabel.text == ""{
        //                self.viewAddressSeparator.isHidden = true
        //                self.addressBackView.isHidden = true
        //                self.orderAddressDetailView.isHidden = true
        //
        //                self.view.layoutIfNeeded()
        //                self.view.setNeedsLayout()
        //                self.view.setNeedsDisplay()
        //            }
        //
        //        }else{
        //            if orderInfoModelObj.isBillingSame == 0{
        //                self.addressBackView.isHidden = false
        //                self.orderAddressDetailView.isHidden = false
        //                self.CustomerHeadLabel.isHidden = false
        ////                self.CustomerAddressLabel.isHidden = false
        //                self.viewAddressSeparatorOne.isHidden = false
        //                //                self.stackViewCust.isHidden = false
        //                self.viewAddressSeparator.isHidden = false
        //                self.CustomerAddressLabel.numberOfLines = 0
        //                self.shippingAddressLabel.numberOfLines = 0
        //                self.billingAddressLabel.numberOfLines = 0
        //                self.CustomerHeadLabel.text =  fbillingName.capitalized + " " + lbillingName.capitalized
        //                self.CustomerAddressLabel.text = (custBillingInfo == "" || custBillingInfo == " \n") ? "" : custBillingInfo
        //                self.shippingAddressLabel.text = (shipping == "" || shipping == " \n") ? "" : shipping //" \nUS, "
        //                self.billingAddressLabel.text = (billing == "" || billing == " \n") ? "" : billing
        //
        //            }else{
        //                self.shippingHeadLabel.isHidden = true
        //                self.shippingAddressLabel.isHidden = true
        //                self.viewAddressSeparator.isHidden = true
        //                self.orderAddressDetailView.isHidden = false
        //                self.addressBackView.isHidden = false
        //                self.billingHeadLabel.isHidden = true
        //                self.billingAddressLabel.isHidden = true
        //                self.CustomerAddressLabel.numberOfLines = 0
        //                self.CustomerHeadLabel.isHidden = false
        //                self.CustomerAddressLabel.isHidden = false
        //                self.viewAddressSeparatorOne.isHidden = false
        //                self.CustomerHeadLabel.text =  fbillingName.capitalized + " " + lbillingName.capitalized
        //                self.CustomerAddressLabel.text = (custBillingInfo == "" || custBillingInfo == " \n") ? "" : custBillingInfo
        //            }
        //
        //
        //            if self.shippingAddressLabel.text == "" &&   self.CustomerAddressLabel.text == "" &&  self.billingAddressLabel.text == ""{
        //                self.viewAddressSeparator.isHidden = true
        //                self.addressBackView.isHidden = true
        //                self.orderAddressDetailView.isHidden = true
        //
        //                self.view.layoutIfNeeded()
        //                self.view.setNeedsLayout()
        //                self.view.setNeedsDisplay()
        //            }
        //        }
        //
        //
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            if orderInfoModelObj.isBillingSame == 0{
                
                if orderInfoModelObj.str_first_name == "" && orderInfoModelObj.str_last_name == "" && orderInfoModelObj.str_phone == "" && orderInfoModelObj.str_email == ""{
                    self.viewAddressSeparator.isHidden = true
                    self.orderAddressDetailView.isHidden = true
                    self.addressBackView.isHidden = true
                    self.CustomerHeadLabel.isHidden = true
                    self.tvAddressCustomer.isHidden = true
                    self.viewAddressSeparatorOne.isHidden = true
                    self.stackViewCust.isHidden = true
                }else {
                    self.addressBackView.isHidden = false
                    self.orderAddressDetailView.isHidden = false
                    self.CustomerHeadLabel.isHidden = false
                    self.tvAddressCustomer.isHidden = false
                    self.viewAddressSeparatorOne.isHidden = false
                    self.stackViewCust.isHidden = false
                    self.CustomerHeadLabel.text = " \(fbillingName.capitalized + " " + lbillingName.capitalized)"
                    self.tvAddressCustomer.text = (custBillingInfo == "" || custBillingInfo == " \n") ? "" : custBillingInfo
                }
                
                if orderInfoModelObj.shippingAddressLine1 == "" {
                    self.viewAddressSeparatorOne.isHidden = true
                    self.viewAddressSeparator.isHidden = true
                    self.addressBackView.isHidden = true
                    self.shippingHeadLabel.isHidden = true
                    self.tvAddressShipping.isHidden = true
                } else {
                    self.viewAddressSeparatorOne.isHidden = false
                    self.addressBackView.isHidden = false
                    self.shippingHeadLabel.isHidden = false
                    self.tvAddressShipping.isHidden = false
                    self.tvAddressShipping.text = (shipping == "" || shipping == " \n") ? "" : shipping //" \nUS, "
                    
                }
                if orderInfoModelObj.addressLine1 == "" {
                    self.viewAddressSeparator.isHidden = true
                    self.viewAddressSeparatorOne.isHidden = true
                    self.addressBackView.isHidden = true
                    self.billingHeadLabel.isHidden = true
                    self.tvAddressBilling.isHidden = true
                    self.viewAddressSeparator.isHidden = true
                } else {
                    self.billingHeadLabel.isHidden = false
                    self.tvAddressBilling.isHidden = false
                    self.viewAddressSeparator.isHidden = false
                    self.viewAddressSeparatorOne.isHidden = false
                    self.tvAddressBilling.text = (billing == "" || billing == " \n") ? "" : billing
                }
                if orderInfoModelObj.str_first_name == "" && orderInfoModelObj.str_last_name == "" && orderInfoModelObj.str_phone == "" && orderInfoModelObj.str_email == "" && orderInfoModelObj.shippingAddressLine1 == "" && orderInfoModelObj.shippingAddressLine1 == ""{
                    self.viewAddressSeparator.isHidden = true
                    self.addressBackView.isHidden = true
                    self.orderAddressDetailView.isHidden = true
                    
                }else {
                    self.addressBackView.isHidden = false
                    self.orderAddressDetailView.isHidden = false
                    self.view.layoutIfNeeded()
                    self.view.setNeedsLayout()
                    self.view.setNeedsDisplay()
                }
            }else{
                if orderInfoModelObj.str_first_name == "" && orderInfoModelObj.str_last_name == "" && orderInfoModelObj.str_phone == "" && orderInfoModelObj.str_email == ""{
                    self.viewAddressSeparator.isHidden = true
                    self.orderAddressDetailView.isHidden = true
                    self.addressBackView.isHidden = true
                    self.CustomerHeadLabel.isHidden = true
                    self.tvAddressCustomer.isHidden = true
                    self.viewAddressSeparatorOne.isHidden = true
                    self.stackViewCust.isHidden = true
                    
                    if billingAddress1 != ""  && billingAddress2 != "" && billingCity != "" && billingRegion != ""{
                        self.addressBackView.isHidden = false
                        self.orderAddressDetailView.isHidden = false
                        self.CustomerHeadLabel.isHidden = false
                        self.tvAddressCustomer.isHidden = false
                        self.stackViewCust.isHidden = false
                        self.tvAddressBilling.isHidden = true
                        self.tvAddressShipping.isHidden = true
                        self.billingHeadLabel.isHidden = true
                        self.shippingHeadLabel.isHidden = true
                        self.CustomerHeadLabel.text = " \(fbillingName.capitalized + " " + lbillingName.capitalized)"
                        self.tvAddressCustomer.text = (custBillingInfo == "" || custBillingInfo == " \n") ? "" : custBillingInfo
                    }
                    
                }else {
                    self.addressBackView.isHidden = false
                    self.orderAddressDetailView.isHidden = false
                    self.CustomerHeadLabel.isHidden = false
                    self.tvAddressCustomer.isHidden = false
                    self.viewAddressSeparatorOne.isHidden = true
                    self.stackViewCust.isHidden = false
                    self.tvAddressShipping.isHidden = true
                    self.shippingHeadLabel.isHidden = true
                    self.tvAddressBilling.isHidden = true
                    self.billingHeadLabel.isHidden = true
                    self.viewAddressSeparator.isHidden = true
                    self.CustomerHeadLabel.text = " \(fbillingName.capitalized + " " + lbillingName.capitalized)"
                    self.tvAddressCustomer.text = (custBillingInfo == "" || custBillingInfo == " \n") ? "" : custBillingInfo
                    
                }
            }
        }
        else{
            if orderInfoModelObj.isBillingSame == 0{
                if orderInfoModelObj.str_first_name == "" && orderInfoModelObj.str_last_name == "" && orderInfoModelObj.str_phone == "" && orderInfoModelObj.str_email == ""{
                    self.viewAddressSeparator.isHidden = true
                    self.orderAddressDetailView.isHidden = true
                    self.addressBackView.isHidden = true
                    self.CustomerHeadLabel.isHidden = true
                    self.CustomerAddressLabel.isHidden = true
                    self.viewAddressSeparatorOne.isHidden = true
                    //                self.stackViewCust.isHidden = true
                }else {
                    self.addressBackView.isHidden = false
                    self.orderAddressDetailView.isHidden = false
                    self.CustomerHeadLabel.isHidden = false
                    self.CustomerAddressLabel.isHidden = false
                    self.viewAddressSeparatorOne.isHidden = false
                    //                self.stackViewCust.isHidden = false
                    self.CustomerAddressLabel.numberOfLines = 0
                    self.CustomerAddressLabel.text = (custBillingInfo == "" || custBillingInfo == " \n") ? "" : custBillingInfo
                }
                
                if orderInfoModelObj.shippingAddressLine1 == "" {
                    self.viewAddressSeparator.isHidden = true
                    self.addressBackView.isHidden = true
                    self.shippingHeadLabel.isHidden = true
                    self.shippingAddressLabel.isHidden = true
                } else {
                    self.viewAddressSeparatorOne.isHidden = false
                    self.addressBackView.isHidden = false
                    self.shippingHeadLabel.isHidden = false
                    self.shippingAddressLabel.isHidden = false
                    self.shippingAddressLabel.numberOfLines = 0
                    self.shippingAddressLabel.text = (shipping == "" || shipping == " \n") ? "N/A" : shipping //" \nUS, "
                    
                }
                if orderInfoModelObj.addressLine1 == "" {
                    self.viewAddressSeparator.isHidden = true
                    self.addressBackView.isHidden = true
                    self.billingHeadLabel.isHidden = true
                    self.billingAddressLabel.isHidden = true
                    self.viewAddressSeparator.isHidden = true
                } else {
                    self.billingHeadLabel.isHidden = false
                    self.billingAddressLabel.isHidden = false
                    self.billingAddressLabel.numberOfLines = 0
                    self.viewAddressSeparator.isHidden = false
                    self.billingAddressLabel.text = (billing == "" || billing == " \n") ? "N/A" : billing
                }
                if orderInfoModelObj.str_first_name == "" && orderInfoModelObj.str_last_name == "" && orderInfoModelObj.str_phone == "" && orderInfoModelObj.str_email == "" && orderInfoModelObj.shippingAddressLine1 == "" && orderInfoModelObj.shippingAddressLine1 == ""{
                    self.viewAddressSeparator.isHidden = true
                    self.addressBackView.isHidden = true
                    self.orderAddressDetailView.isHidden = true
                    
                }else {
                    self.addressBackView.isHidden = false
                    self.orderAddressDetailView.isHidden = false
                    
                }
            }else{
                if orderInfoModelObj.str_first_name == "" && orderInfoModelObj.str_last_name == "" && orderInfoModelObj.str_phone == "" && orderInfoModelObj.str_email == ""{
                    self.viewAddressSeparator.isHidden = true
                    self.orderAddressDetailView.isHidden = true
                    self.addressBackView.isHidden = true
                    self.CustomerHeadLabel.isHidden = true
                    self.billingAddressLabel.isHidden = true
                    self.viewAddressSeparatorOne.isHidden = true
                    //self.stackViewCust.isHidden = true
                    
                    if billingAddress1 != ""  && billingAddress2 != "" && billingCity != "" && billingRegion != ""{
                        self.addressBackView.isHidden = false
                        self.orderAddressDetailView.isHidden = false
                        self.CustomerHeadLabel.isHidden = false
                        self.viewAddressSeparator.isHidden = false
                        self.CustomerAddressLabel.isHidden = false
                        //self.stackViewCust.isHidden = false
                        self.billingAddressLabel.isHidden = true
                        self.shippingAddressLabel.isHidden = true
                        self.billingHeadLabel.isHidden = true
                        self.shippingHeadLabel.isHidden = true
                        self.CustomerAddressLabel.numberOfLines = 0
                        self.CustomerHeadLabel.text = "\(fbillingName.capitalized + " " + lbillingName.capitalized)"
                        self.CustomerAddressLabel.text = (custBillingInfo == "" || custBillingInfo == " \n") ? "" : custBillingInfo
                    }
                    
                }else {
                    self.addressBackView.isHidden = false
                    self.orderAddressDetailView.isHidden = false
                    self.CustomerHeadLabel.isHidden = false
                    self.CustomerAddressLabel.isHidden = false
                    self.viewAddressSeparatorOne.isHidden = true
                    self.viewAddressSeparator.isHidden = false
                    // self.stackViewCust.isHidden = false
                    self.shippingAddressLabel.isHidden = true
                    self.shippingHeadLabel.isHidden = true
                    self.billingAddressLabel.isHidden = true
                    self.billingHeadLabel.isHidden = true
                    self.CustomerAddressLabel.numberOfLines = 0
                    self.CustomerHeadLabel.text = "\(fbillingName.capitalized + " " + lbillingName.capitalized)"
                    self.CustomerAddressLabel.text = (custBillingInfo == "" || custBillingInfo == " \n") ? "" : custBillingInfo
                    
                }
            }
        }
        
        if versionOb < 4 {
            //self.refundButton.isHidden = true
            //self.btnClear.isHidden = true
            if UI_USER_INTERFACE_IDIOM() == .pad {
                tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: 90)
            }else{
                tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: 70)
            }
        }else{
            //self.refundButton.isHidden = false
            //self.btnClear.isHidden = false
            tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: 0)

//            if UI_USER_INTERFACE_IDIOM() == .phone {
//                tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: 70)
//            }else{
//                tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: 0)
//            }
            //tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat((orderInfoModelObj.paymentStatus.lowercased() == "invoice" || !orderInfoModelObj.isOrderRefund) ? 40 : 250))
        }
        
        if versionOb < 4 {
            //refundButton.setTitle("Refund", for: .normal)
            //refundExchangeButtonView.isHidden = false
            //btnClear.isHidden = false
            //refundButton.isHidden = false
            //refundButton.isUserInteractionEnabled = true
//            if orderInfoModelObj.paymentStatus.lowercased() == "invoice" || !orderInfoModelObj.isOrderRefund {
//                refundButton.isHidden = true
//            }
            
//            if orderInfoModelObj.showRefundButton == "true" {
//                refundButton.isHidden = false
//            } else {
//                refundButton.isHidden = true
//            }

        } else {
            
            if orderInfoModelObj.paymentStatus.lowercased() == "invoice" || !orderInfoModelObj.isOrderRefund {
                //btnClear.isHidden = true
                //refundButton.isHidden = true
            }
            
//            refundExchangeButtonView.isHidden = orderInfoModelObj.paymentStatus.lowercased() == "invoice" || !orderInfoModelObj.isOrderRefund
        }
        if orderInfoModelObj.showPayNow == "true" {
            btnPayNow.isHidden = false
            tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: 46)
        } else {
            btnPayNow.isHidden = true
            tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: 0)

        }
        
        self.tableView.reloadData()
        
        //        tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat((orderInfoModelObj.paymentStatus.lowercased() == "invoice" || !orderInfoModelObj.isOrderRefund) ? 40 : 100))
        //tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat((orderInfoModelObj.paymentStatus.lowercased() == "invoice" || !orderInfoModelObj.isOrderRefund) ? 40 : 100))
        
        
        var billingAddress = String()
        var shippingAddress = String()
        var customerAddress = String()
        var customerInformation = String()
        
        let fbillingCustName = orderInfoModelObj.firstName
        let lbillingCustName = orderInfoModelObj.lastName
        let billingCustAddress1 = orderInfoModelObj.addressLine1
        let billingCustAddress2 = orderInfoModelObj.addressLine2
        let billingCustCity = orderInfoModelObj.city
        let billingCustRegion = orderInfoModelObj.region
        let billingCustCountry = orderInfoModelObj.country
        let billingCustPostalCode = orderInfoModelObj.postalCode
        let billingCustPhone = orderInfoModelObj.phone
        let billingCustEmail = orderInfoModelObj.email
        let companyCustName = orderInfoModelObj.companyName
        
        
        let shippingfsname = orderInfoModelObj.shippingFirstName
        let shippinglsname = orderInfoModelObj.shippingLastName
        let shippingaddress1 = orderInfoModelObj.shippingAddressLine1
        let shippingaddress2 = orderInfoModelObj.shippingAddressLine2
        let shippingcity = orderInfoModelObj.shippingCity
        let shippingregion = orderInfoModelObj.shippingRegion
        let shippingcountry = orderInfoModelObj.shippingCountry
        let shippingpostalCode = orderInfoModelObj.shippingPostalCode
        //        let shippingphone = orderInfoModelObj.shippingPhone
        //        let shippingemail = orderInfoModelObj.shippingEmail
        
        
        
        // customer billing
        
        if fbillingCustName.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            billingAddress +=   fbillingCustName + " "
        }
        
        if lbillingCustName.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            billingAddress +=   lbillingCustName
        }
        
        if billingCustAddress2 == ""{
            if billingAddress != ""{
                if billingCustAddress1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                    billingAddress += "\n" + billingCustAddress1
                }
            }else{
                if billingCustAddress1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                    billingAddress += billingCustAddress1
                }
            }
        }else{
            if billingCustAddress1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                if billingAddress != ""{
                    billingAddress +=  "\n" + billingCustAddress1
                }else{
                    billingAddress +=  billingCustAddress1
                }
            }
        }
        
        if billingCustAddress2.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            if billingAddress != ""{
                billingAddress +=   "\n" + billingCustAddress2
            }else{
                billingAddress +=   billingCustAddress2
            }
        }
        
        if billingCustCity.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            if billingAddress != ""{
                billingAddress += "\n" + billingCustCity
            }else{
                billingAddress += billingCustCity
            }
        }
        
        if billingCustRegion.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            if billingAddress != ""{
                billingAddress += ", " + billingCustRegion
            }else{
                billingAddress +=  billingCustRegion
            }
        }
        if billingCustPostalCode.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            if billingAddress != ""{
                billingAddress += ", " + formattedZIPCodeNumber(number: billingCustPostalCode)
            }else{
                billingAddress +=  formattedZIPCodeNumber(number: billingCustPostalCode)
            }
        }
        if billingCustAddress1 != "" && billingCustCity != "" && billingCustRegion != "" && billingCustPostalCode != "" {
            if billingCustCountry.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                if billingAddress != ""{
                    billingAddress += "\n" + billingCustCountry
                }else{
                    billingAddress += billingCustCountry
                }
            }
        }
        if billingCustAddress1 != ""{
            if billingCustCountry.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                if billingAddress != ""{
                    billingAddress += "\n" + billingCustCountry
                }else{
                    billingAddress += billingCustCountry
                }
            }
        }
        
        
        
        // customer shipping
        
        if shippingfsname.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            shippingAddress +=   shippingfsname + " "
        }
        
        if shippinglsname.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            shippingAddress +=   shippinglsname
        }
        
        if shippingaddress2 == ""{
            if shippingaddress1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                if shippingAddress != ""{
                    shippingAddress += "\n" + shippingaddress1
                }else{
                    shippingAddress += shippingaddress1
                }
            }
        }else{
            if shippingaddress1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                if shippingAddress != ""{
                    shippingAddress +=  "\n" + shippingaddress1
                }else{
                    shippingAddress +=  shippingaddress1
                }
            }
        }
        
        if shippingaddress2.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            if shippingAddress != ""{
                shippingAddress +=   "\n" + shippingaddress2
            }else{
                shippingAddress +=   shippingaddress2
            }
        }
        
        if shippingcity.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            if shippingAddress != ""{
                shippingAddress += "\n" + shippingcity
            }else{
                shippingAddress +=  shippingcity
            }
        } else {
            if shippingAddress != ""{
                shippingAddress += "\n"
            }
        }
        
        if shippingregion.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            
            if shippingAddress.last == "\n" {
                if shippingAddress != ""{
                    shippingAddress += shippingregion
                }else{
                    shippingAddress +=  shippingregion
                }
            } else {
                if shippingAddress != ""{
                    shippingAddress += ", " + shippingregion
                }else{
                    shippingAddress +=  shippingregion
                }
            }
        }
        if shippingpostalCode.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            if shippingAddress.last == "\n" {
                print("enterrrrr")
                if shippingAddress != ""{
                    shippingAddress += formattedZIPCodeNumber(number: shippingpostalCode)
                }else{
                    shippingAddress +=  formattedZIPCodeNumber(number: shippingpostalCode)
                }
            } else {
                print("enterrrrrout")
                if shippingAddress != ""{
                    if shippingregion.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                        shippingAddress += " " + formattedZIPCodeNumber(number: shippingpostalCode)
                    }else{
                        shippingAddress += ", " + formattedZIPCodeNumber(number: shippingpostalCode)
                    }
                    
                }else{
                    shippingAddress +=  formattedZIPCodeNumber(number: shippingpostalCode)
                }
            }
            
        }
        //        if shippingaddress1 != "" && shippingcity != "" && shippingregion != "" && shippingpostalCode != "" {
        //            if shippingcountry.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
        //                if shippingAddress != ""{
        //                    shippingAddress += "\n" + shippingcountry
        //                }else{
        //                    shippingAddress +=  shippingcountry
        //                }
        //            }
        //        }
        
        if shippingaddress1 != ""{
            if shippingcountry.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                if shippingAddress.last == "\n" {
                    if shippingAddress != ""{
                        shippingAddress +=  shippingcountry
                    }else{
                        shippingAddress +=  shippingcountry
                    }
                } else {
                    if shippingAddress != ""{
                        shippingAddress += "\n" + shippingcountry
                    }else{
                        shippingAddress +=  shippingcountry
                    }
                }
            }
        }
        
        // customer information
        if billingCustPhone.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            if customerAddress != ""{
                customerAddress +=  formattedPhoneNumber(number: billingCustPhone)
            }else{
                customerAddress +=  formattedPhoneNumber(number: billingCustPhone)
            }
            
        }
        
        if billingCustEmail.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            if customerAddress != ""{
                customerAddress +=  "\n" + billingCustEmail
            }else{
                customerAddress +=  billingCustEmail
            }
        }
        
        if companyCustName.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            if customerAddress != ""{
                customerAddress += "\n" + companyCustName
            }else{
                customerAddress +=  companyCustName
            }
        }
        
        if billingCustAddress2 == ""{
            if billingCustAddress1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                if customerAddress != ""{
                    customerAddress += "\n" + billingCustAddress1
                }else{
                    customerAddress += billingCustAddress1
                }
            }
        }else{
            if billingCustAddress1.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                if customerAddress != ""{
                    customerAddress += "\n" + billingCustAddress1
                }else{
                    customerAddress +=  billingCustAddress1
                }
            }
        }
        
        if billingCustAddress2.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            if customerAddress != ""{
                customerAddress +=  "\n" + billingCustAddress2
            }else{
                customerAddress +=   billingCustAddress2
            }
        }
        
        if billingCustCity.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            if customerAddress != ""{
                customerAddress += "\n" + billingCustCity
            }else{
                customerAddress +=  billingCustCity
            }
        } else {
            if customerAddress != ""{
                customerAddress += "\n"
            }
        }
        
        if billingCustRegion.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            if customerAddress.last == "\n" {
                if customerAddress != ""{
                    customerAddress +=  billingCustRegion
                }else{
                    customerAddress +=  billingCustRegion
                }
            } else {
                if customerAddress != ""{
                    customerAddress += ", " + billingCustRegion
                }else{
                    customerAddress +=  billingCustRegion
                }
            }
            
            
        }
        
        if billingCustPostalCode.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            
            if customerAddress.last == "\n" {
                if customerAddress != ""{
                    customerAddress += formattedZIPCodeNumber(number: billingCustPostalCode)
                }else{
                    customerAddress +=  formattedZIPCodeNumber(number: billingCustPostalCode)
                }
            } else {
                if customerAddress != ""{
                    if billingCustRegion.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                        customerAddress += " " + formattedZIPCodeNumber(number: billingCustPostalCode)
                    }else{
                        customerAddress += ", " + formattedZIPCodeNumber(number: billingCustPostalCode)
                    }
                    
                    
                }else{
                    customerAddress +=  formattedZIPCodeNumber(number: billingCustPostalCode)
                }
            }
            
        }
        
        //        if billingCustAddress1 != "" && billingCustCity != "" && billingCustRegion != "" && billingCustPostalCode != "" {
        //            if billingCustCountry.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
        //                if customerAddress != ""{
        //                    customerAddress += "\n" + billingCustCountry
        //                }else{
        //                    customerAddress +=  billingCustCountry
        //                }
        //            }
        //        }
        if billingCustAddress1 != ""{
            if billingCustCountry.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                
                if customerAddress.last == "\n" {
                    if customerAddress != ""{
                        customerAddress +=  billingCustCountry
                    }else{
                        customerAddress +=  billingCustCountry
                    }
                } else {
                    if customerAddress != ""{
                        customerAddress += "\n" + billingCustCountry
                    }else{
                        customerAddress +=  billingCustCountry
                    }
                }
                
                
            }
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            
            tvAddressBilling.isHidden = true
            billingHeadLabel.isHidden = true
            viewAddressSeparator.isHidden = true
            
            
            if fbillingCustName == "" && lbillingCustName == ""{
                CustomerHeadLabel.isHidden = true
            }else{
                CustomerHeadLabel.isHidden = false
                self.CustomerHeadLabel.text = " \(fbillingCustName.capitalized + " " + lbillingCustName.capitalized)"
            }
            
            //            if billingAddress == "" {
            //                tvAddressBilling.isHidden = true
            //                billingHeadLabel.isHidden = true
            //                viewAddressSeparator.isHidden = true
            //                viewAddressSeparatorOne.isHidden = true
            //            }
            
            if shippingAddress == "" {
                tvAddressShipping.isHidden = true
                shippingHeadLabel.isHidden = true
                viewAddressSeparator.isHidden = true
                viewAddressSeparatorOne.isHidden = true
            }
            
            if billingAddress == "" && shippingAddress == "" && customerAddress == ""{
                orderAddressDetailView.isHidden = true
                addressBackView.isHidden = true
            }
            
            if orderInfoModelObj.isBillingSame == 0 {
                //                if billingAddress == "" {
                //                    tvAddressBilling.isHidden = true
                //                    billingHeadLabel.isHidden = true
                //                    viewAddressSeparator.isHidden = true
                //                    viewAddressSeparatorOne.isHidden = true
                //                } else {
                //                    tvAddressBilling.isHidden = false
                //                    billingHeadLabel.isHidden = false
                //                    viewAddressSeparator.isHidden = false
                //                    viewAddressSeparatorOne.isHidden = false
                //                    tvAddressCustomer.text = customerAddress
                //                    tvAddressBilling.text = billingAddress
                //                }
                
                if shippingAddress == "" {
                    tvAddressShipping.isHidden = true
                    shippingHeadLabel.isHidden = true
                    // viewAddressSeparator.isHidden = true
                    viewAddressSeparatorOne.isHidden = true
                } else {
                    tvAddressShipping.isHidden = false
                    shippingHeadLabel.isHidden = false
                    tvAddressCustomer.isHidden = false
                    
                    //viewAddressSeparator.isHidden = false
                    viewAddressSeparatorOne.isHidden = false
                    tvAddressCustomer.text = customerAddress
                    // tvAddressBilling.text = billingAddress
                    tvAddressShipping.text = shippingAddress
                    orderAddressDetailView.isHidden = false
                    addressBackView.isHidden = false
                    stackViewCust.isHidden = false
                }
                if shippingAddress != "" && billingAddress == "" && customerAddress == ""{
                    tvAddressShipping.isHidden = false
                    shippingHeadLabel.isHidden = false
                    // tvAddressBilling.isHidden = true
                    // billingHeadLabel.isHidden = true
                    tvAddressCustomer.isHidden = true
                    CustomerHeadLabel.isHidden = true
                    stackViewCust.isHidden = true
                    // viewAddressSeparator.isHidden = true
                    viewAddressSeparatorOne.isHidden = true
                    tvAddressShipping.text = shippingAddress
                    
                }
                
                
                
            } else {
                
                if billingAddress == "" {
                    //   tvAddressBilling.isHidden = true
                    //   billingHeadLabel.isHidden = true
                    //  viewAddressSeparator.isHidden = true
                    viewAddressSeparatorOne.isHidden = true
                    if customerAddress != ""{
                        stackViewCust.isHidden = false
                        tvAddressBilling.isHidden = true
                        tvAddressCustomer.isHidden = false
                        tvAddressShipping.isHidden = true
                        billingHeadLabel.isHidden = true
                        shippingHeadLabel.isHidden = true
                        viewAddressSeparator.isHidden = true
                        viewAddressSeparatorOne.isHidden = true
                        tvAddressCustomer.text = customerAddress
                        orderAddressDetailView.isHidden = false
                        addressBackView.isHidden = false
                    }
                } else {
                    stackViewCust.isHidden = false
                    tvAddressBilling.isHidden = true
                    tvAddressCustomer.isHidden = false
                    tvAddressShipping.isHidden = true
                    billingHeadLabel.isHidden = true
                    shippingHeadLabel.isHidden = true
                    viewAddressSeparator.isHidden = true
                    viewAddressSeparatorOne.isHidden = true
                    tvAddressCustomer.text = customerAddress
                    orderAddressDetailView.isHidden = false
                    addressBackView.isHidden = false
                }
            }
        }else{
            
            billingAddressLabel.isHidden = true
            billingHeadLabel.isHidden = true
            viewAddressSeparator.isHidden = true
            
            if fbillingCustName == "" && lbillingCustName == ""{
                CustomerHeadLabel.isHidden = true
            }else{
                CustomerHeadLabel.isHidden = false
                self.CustomerHeadLabel.text = "\(fbillingCustName.capitalized + " " + lbillingCustName.capitalized)"
            }
            
            if billingAddress == "" {
                billingAddressLabel.isHidden = true
                billingHeadLabel.isHidden = true
                viewAddressSeparator.isHidden = true
                viewAddressSeparatorOne.isHidden = true
            }
            
            if shippingAddress == "" {
                shippingAddressLabel.isHidden = true
                shippingHeadLabel.isHidden = true
                viewAddressSeparator.isHidden = true
                viewAddressSeparatorOne.isHidden = true
            }
            
            if billingAddress == "" && shippingAddress == "" && customerAddress == ""{
                orderAddressDetailView.isHidden = true
                addressBackView.isHidden = true
            }
            
            if orderInfoModelObj.isBillingSame == 0 {
                if billingAddress == "" {
                    billingAddressLabel.isHidden = true
                    billingHeadLabel.isHidden = true
                    viewAddressSeparator.isHidden = true
                    viewAddressSeparatorOne.isHidden = true
                } else {
                    //                    billingAddressLabel.isHidden = false
                    //                    billingHeadLabel.isHidden = false
                    viewAddressSeparator.isHidden = false
                    viewAddressSeparatorOne.isHidden = false
                    CustomerAddressLabel.numberOfLines = 0
                    //      billingAddressLabel.numberOfLines = 0
                    CustomerAddressLabel.text = customerAddress
                    //   billingAddressLabel.text = billingAddress
                }
                
                if shippingAddress == "" {
                    shippingAddressLabel.isHidden = true
                    shippingHeadLabel.isHidden = true
                    viewAddressSeparator.isHidden = true
                    viewAddressSeparatorOne.isHidden = true
                } else {
                    shippingAddressLabel.isHidden = false
                    shippingHeadLabel.isHidden = false
                    CustomerAddressLabel.isHidden = false
                    viewAddressSeparator.isHidden = false
                    viewAddressSeparatorOne.isHidden = false
                    CustomerAddressLabel.numberOfLines = 0
                    //      billingAddressLabel.numberOfLines = 0
                    shippingAddressLabel.numberOfLines = 0
                    CustomerAddressLabel.text = customerAddress
                    //       billingAddressLabel.text = billingAddress
                    shippingAddressLabel.text = shippingAddress
                    orderAddressDetailView.isHidden = false
                    addressBackView.isHidden = false
                    //stackViewCust.isHidden = false
                }
                if shippingAddress != "" && billingAddress == "" && customerAddress == ""{
                    shippingAddressLabel.isHidden = false
                    shippingHeadLabel.isHidden = false
                    billingAddressLabel.isHidden = true
                    billingHeadLabel.isHidden = true
                    CustomerAddressLabel.isHidden = true
                    CustomerHeadLabel.isHidden = true
                    // stackViewCust.isHidden = true
                    viewAddressSeparator.isHidden = true
                    viewAddressSeparatorOne.isHidden = true
                    shippingAddressLabel.numberOfLines = 0
                    shippingAddressLabel.text = shippingAddress
                    
                }
            } else {
                if billingAddress == "" {
                    billingAddressLabel.isHidden = true
                    billingHeadLabel.isHidden = true
                    viewAddressSeparator.isHidden = true
                    viewAddressSeparatorOne.isHidden = true
                    if customerAddress != ""{
                        billingAddressLabel.isHidden = true
                        CustomerAddressLabel.isHidden = false
                        shippingAddressLabel.isHidden = true
                        billingHeadLabel.isHidden = true
                        shippingHeadLabel.isHidden = true
                        viewAddressSeparator.isHidden = true
                        viewAddressSeparatorOne.isHidden = true
                        CustomerAddressLabel.numberOfLines = 0
                        CustomerAddressLabel.text = customerAddress
                        orderAddressDetailView.isHidden = false
                        addressBackView.isHidden = false
                    }
                    
                } else {
                    // stackViewCust.isHidden = false
                    billingAddressLabel.isHidden = true
                    CustomerAddressLabel.isHidden = false
                    shippingAddressLabel.isHidden = true
                    billingHeadLabel.isHidden = true
                    shippingHeadLabel.isHidden = true
                    viewAddressSeparator.isHidden = true
                    viewAddressSeparatorOne.isHidden = true
                    CustomerAddressLabel.numberOfLines = 0
                    CustomerAddressLabel.text = customerAddress
                    orderAddressDetailView.isHidden = false
                    addressBackView.isHidden = false
                }
            }
            
        }
    }
    
    private func handlePrinterAction() {
        if BluetoothPrinter.sharedInstance.isConnected() || DataManager.isStarPrinterConnected
        {
            let str_footerText = receiptModel.card_agreement_text + "<br>" + receiptModel.refund_policy_text + "<br>" + receiptModel.footer_text
            
            var add = ""
            if self.receiptModel.city == "" && self.receiptModel.region == "" && self.receiptModel.postal_code == "" {
                add = ""
            } else if self.receiptModel.city != "" && (self.receiptModel.region != "" || self.receiptModel.postal_code != "") {
                add = ","
            } else if self.receiptModel.city == "" && (self.receiptModel.region != "" || self.receiptModel.postal_code != ""){
                add = ""
            }
            
            var orderAddress = ""
            
            if self.receiptModel.address_line1 != "" {
                orderAddress = self.receiptModel.address_line1
            }
            
            if self.receiptModel.address1 != "" {
                if orderAddress == "" {
                    orderAddress =  self.receiptModel.address1
                } else {
                    orderAddress =  orderAddress +  "\n" + self.receiptModel.address1
                }
                
            }
            
            if self.receiptModel.address2 != "" {
                if orderAddress == "" {
                    orderAddress =  self.receiptModel.address2
                } else {
                    orderAddress =  orderAddress +  "\n" + self.receiptModel.address2
                }
            }
            
            let order: JSONDictionary = ["title" : self.receiptModel.packing_slip_title ,
                                         "address" : "\(orderAddress)" ,
                "addressDetail" : "\(self.receiptModel.city)\(add)\(self.receiptModel.region) \(self.receiptModel.postal_code)",
                "date" : self.receiptModel.date_added ,
                "orderid": self.receiptModel.order_id ,
                "products" : self.receiptModel.array_ReceiptContent ,
                "total" : self.receiptModel.total ,
                "tip" : self.receiptModel.tip ,
                "subtotal" : self.receiptModel.sub_total ,
                "discount" : self.receiptModel.discount ,
                "shipping" : self.receiptModel.shipping ,
                "tax" : self.receiptModel.tax ,
                "footertext" : str_footerText ,
                "card_agreement_text" : receiptModel.card_agreement_text,
                "refund_policy_text" : receiptModel.refund_policy_text,
                "footer_text" : receiptModel.footer_text,
                "transactiontype" : self.receiptModel.transaction_type,
                "paymentmode" : self.receiptModel.payment_type,
                "order_time" : self.receiptModel.order_time,
                "sale_location" : self.receiptModel.sale_location,
                "bar_code" : self.receiptModel.bar_code,
                "card_details" : self.receiptModel.array_ReceiptCardContent,
                "entry_method" : self.receiptModel.entry_method,
                "card_number" : self.receiptModel.card_number,
                "approval_code" : self.receiptModel.approval_code,
                "coupon_code" : self.receiptModel.coupon_code,
                "change_due" : self.receiptModel.change_due,
                "signature" : self.receiptModel.signature,
                "card_holder_name" : self.receiptModel.card_holder_name,
                "card_holder_label" : self.receiptModel.card_holder_label,
                "company_name" : self.receiptModel.company_name,
                "city" : self.receiptModel.city,
                "region" :self.receiptModel.region,
                "customer_service_phone" :self.receiptModel.customer_service_phone,
                "customer_service_email" :self.receiptModel.customer_service_email,
                "postal_code" :self.receiptModel.postal_code,
                "header_text" :self.receiptModel.header_text,
                "balance_due" :self.receiptModel.balance_Due,
                "payment_status" :self.receiptModel.payment_status,
                "source" :self.receiptModel.source,
                "show_company_address" : self.receiptModel.show_company_address,
                "show_all_final_transactions" :self.receiptModel.show_all_final_transactions,
                "print_all_transactions" :self.receiptModel.print_all_transactions,
                "extra_merchant_copy" :self.receiptModel.extra_merchant_copy,
                "showtipline_status" : self.receiptModel.showtipline_status,
                "total_refund_amount" : self.receiptModel.total_refund_amount,
                "notes" : self.receiptModel.notes,
                "isNotes" : self.receiptModel.isNotes,
                "lowvaluesig_status" : self.receiptModel.lowvaluesig_status,
                "ACCT" : self.receiptModel.ACCT,
                 "AID" : self.receiptModel.AID,
                 "TC" : self.receiptModel.TC,
                 "AppName" : self.receiptModel.Appname,
                 "lowvaluesig_status_setting_flag" : self.receiptModel.lowvaluesig_status_setting_flag

            ]
            
            BluetoothPrinter.sharedInstance.printContent(dict:order)
        }
        else if DataManager.isStarPrinterConnected
        {
            
            //            self.showAlert(message: "No Printer Found.")
            //  {
            //   {
//            let commands: Data
//
//            let emulation: StarIoExtEmulation = LoadStarPrinter.getEmulation()
//            print(emulation)
//
//            let width: Int = LoadStarPrinter.getSelectedPaperSize().rawValue
//
//            let paperSize: PaperSizeIndex = LoadStarPrinter.getSelectedPaperSize()
//            let language: LanguageIndex = LanguageIndex.english//LoadStarPrinter.getSelectedLanguage()
//            let localizeReceipts: ILocalizeReceipts = LocalizeReceipts.createLocalizeReceipts(language,
//                                                                                              paperSizeIndex: paperSize)
//            commands = PrinterFunctions.createTextReceiptApiData(emulation,utf8: true)
//
//            //self.blind = true
//
//            //  if #available(iOS 13.0, *) {
//            Communication.sendCommandsForPrintReDirection(commands,
//                                                          timeout: 10000) { (communicationResultArray) in
//                                                            // self.blind = false
//
//                                                            var message: String = ""
//
//                                                            for i in 0..<communicationResultArray.count {
//                                                                if i == 0 {
//                                                                    message += "[Destination]\n"
//                                                                }
//                                                                else {
//                                                                    message += "[Backup]\n"
//                                                                }
//
//                                                                message += "Port Name: " + communicationResultArray[i].0 + "\n"
//
//                                                                switch communicationResultArray[i].1.result {
//                                                                case .success:
//                                                                    message += "----> Success!\n\n";
//                                                                case .errorOpenPort:
//                                                                    message += "----> Fail to openPort\n\n";
//                                                                case .errorBeginCheckedBlock:
//                                                                    message += "----> Printer is offline (beginCheckedBlock)\n\n";
//                                                                case .errorEndCheckedBlock:
//                                                                    message += "----> Printer is offline (endCheckedBlock)\n\n";
//                                                                case .errorReadPort:
//                                                                    message += "----> Read port error (readPort)\n\n";
//                                                                case .errorWritePort:
//                                                                    message += "----> Write port error (writePort)\n\n";
//                                                                default:
//                                                                    message += "----> Unknown error\n\n";
//                                                                }
//                                                            }
//
//
//
//                                                            //                                                                    self.showSimpleAlert(title: "Communication Result",
//                                                            //                                                                                         message: message,
//                                                            //                                                                                         buttonTitle: "OK",
//                                                            //                                                                                         buttonStyle: .cancel)
//                                                            appDelegate.showToast(message: message)
//            }
//            //            } else {
//            //                // Fallback on earlier versions
//            //                 appDelegate.showToast(message: "#available(iOS 13.0, *)")
//            //            }
//            //  }
//            // }
//
//            if  HomeVM.shared.receiptModel.extra_merchant_copy == "true" {
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                    let commands: Data
//
//                    let emulation: StarIoExtEmulation = LoadStarPrinter.getEmulation()
//                    print(emulation)
//
//                    let width: Int = LoadStarPrinter.getSelectedPaperSize().rawValue
//
//                    let paperSize: PaperSizeIndex = LoadStarPrinter.getSelectedPaperSize()
//                    let language: LanguageIndex = LanguageIndex.english//LoadStarPrinter.getSelectedLanguage()
//                    let localizeReceipts: ILocalizeReceipts = LocalizeReceipts.createLocalizeReceipts(language,
//                                                                                                      paperSizeIndex: paperSize)
//                    commands = PrinterFunctions.createTextReceiptApiData(emulation,utf8: true)
//
//                    //self.blind = true
//
//                    //  if #available(iOS 13.0, *) {
//                    Communication.sendCommandsForPrintReDirection(commands,
//                                                                  timeout: 10000) { (communicationResultArray) in
//                                                                    // self.blind = false
//
//                                                                    var message: String = ""
//
//                                                                    for i in 0..<communicationResultArray.count {
//                                                                        if i == 0 {
//                                                                            message += "[Destination]\n"
//                                                                        }
//                                                                        else {
//                                                                            message += "[Backup]\n"
//                                                                        }
//
//                                                                        message += "Port Name: " + communicationResultArray[i].0 + "\n"
//
//                                                                        switch communicationResultArray[i].1.result {
//                                                                        case .success:
//                                                                            message += "----> Success!\n\n";
//                                                                        case .errorOpenPort:
//                                                                            message += "----> Fail to openPort\n\n";
//                                                                        case .errorBeginCheckedBlock:
//                                                                            message += "----> Printer is offline (beginCheckedBlock)\n\n";
//                                                                        case .errorEndCheckedBlock:
//                                                                            message += "----> Printer is offline (endCheckedBlock)\n\n";
//                                                                        case .errorReadPort:
//                                                                            message += "----> Read port error (readPort)\n\n";
//                                                                        case .errorWritePort:
//                                                                            message += "----> Write port error (writePort)\n\n";
//                                                                        default:
//                                                                            message += "----> Unknown error\n\n";
//                                                                        }
//                                                                    }
//
//
//
//                                                                    //                                                                    self.showSimpleAlert(title: "Communication Result",
//                                                                    //                                                                                         message: message,
//                                                                    //                                                                                         buttonTitle: "OK",
//                                                                    //                                                                                         buttonStyle: .cancel)
//                                                                    appDelegate.showToast(message: message)
//                    }
//                }
//            }
//
//            //appDelegate.showToast(message: "No Printer Found.")
        } else {
            appDelegate.showToast(message: "No Printer Found.")
        }
    }
    
    private func reloadTableData(animate: Bool = false) {
        DispatchQueue.main.async {
            if animate {
                self.scrollView.alpha = 0
            }
            //Reload Table
            self.tableView.reloadData {
                self.tableView.contentOffset = .zero
                if !self.isOrderSummery {
                    self.tableViewheight.constant = self.view.bounds.size.height - (UI_USER_INTERFACE_IDIOM() == .pad ? 120 : 210)
                }else {
                    self.tableView.layoutIfNeeded()
                    self.tableViewheight.constant = self.tableView.contentSize.height
                }
                self.scrollView.scrollToTop()
                if animate {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.scrollView.alpha = 1.0
                    })
                }
            }
            //Update Shadow
            if self.isLandscape {
                if !self.isOrderSummery {
                    //Remove Shadow
                    self.tableBackView.layer.shadowColor = UIColor.clear.cgColor
                    self.tableBackView.layer.shadowOffset = CGSize.zero
                    self.tableBackView.layer.shadowOpacity = 0
                    self.tableBackView.layer.shadowRadius = 0
                }else {
                    //Add Shadow
                    self.tableBackView.layer.shadowColor = UIColor.darkGray.cgColor
                    self.tableBackView.layer.shadowOffset = CGSize.zero
                    self.tableBackView.layer.shadowOpacity = 0.3
                    self.tableBackView.layer.shadowRadius = 2
                }
            }else {
                //Remove Shadow
                self.tableBackView.layer.shadowColor = UIColor.clear.cgColor
                self.tableBackView.layer.shadowOffset = CGSize.zero
                self.tableBackView.layer.shadowOpacity = 0
                self.tableBackView.layer.shadowRadius = 0
            }
            self.updateView()
            
        }
    }
    
    private func getProductData() -> (ProductsModel) {
        
        let obj =  ProductsModel()
        
        for product in orderInfoModelObj.productsArray {
            
            obj.variationsData = product.variationsData
            
            self.catAndProductDelegate?.didAddNewProduct?(data: obj, productDetail: obj)  //DDDD
            
        }
        
        return obj
    }
    
    private func getUpdatedData() -> (Bool, Bool, Bool, Bool, OrderInfoModel) { //1. Exchange 2. Refund 3. Transactions 4. Partial Amount 5. Updated Obj
        
        var isExchangeProductFound = false
        var isRefundProductFound = false
        var isTransactiontFound = false
        var isPartialAmountFound = false
        
        if DataManager.isCaptureButton {
            let customerObj = [
                "country": orderInfoModelObj.cust_country,
                "billingCountry": orderInfoModelObj.country,
                "shippingCountry":orderInfoModelObj.shippingCountry,
                "coupan": "",
                "str_first_name":orderInfoModelObj.cust_first_name,
                "str_last_name":orderInfoModelObj.cust_last_name,
                "str_address":orderInfoModelObj.cust_address1,
                "str_bpid":orderInfoModelObj.bpId,
                "str_city":orderInfoModelObj.cust_city,
                "str_order_id":orderInfoModelObj.orderID,
                "str_email":orderInfoModelObj.str_email,
                "str_userID":orderInfoModelObj.userID,
                "str_phone":orderInfoModelObj.str_phone,
                "str_region":orderInfoModelObj.cust_region,
                "str_address2":orderInfoModelObj.cust_address2,
                "str_Billingcity":orderInfoModelObj.city,
                "str_postal_code":orderInfoModelObj.postalCode,
                "str_Billingphone":orderInfoModelObj.phone,
                "str_Billingaddress":orderInfoModelObj.addressLine1,
                "str_Billingaddress2":orderInfoModelObj.addressLine2,
                "str_Billingregion":orderInfoModelObj.region,
                "str_Billingpostal_code":orderInfoModelObj.postalCode,
                "shippingPhone": orderInfoModelObj.shippingPhone,
                "str_company": orderInfoModelObj.companyName,
                "shippingAddress" : orderInfoModelObj.shippingAddressLine2,
                "shippingAddress2": orderInfoModelObj.shippingAddressLine1,
                "shippingCity": orderInfoModelObj.shippingCity,
                "shippingRegion" : orderInfoModelObj.shippingRegion,
                "shippingPostalCode": orderInfoModelObj.shippingPostalCode,
                "billing_first_name":orderInfoModelObj.firstName,
                "billing_last_name":orderInfoModelObj.lastName,
                "user_custom_tax":orderInfoModelObj.tax,
                "shipping_first_name":orderInfoModelObj.shippingFirstName,
                "shipping_last_name":orderInfoModelObj.shippingLastName,
                "shippingEmail": orderInfoModelObj.shippingEmail,
                "str_Billingemail": orderInfoModelObj.email] as [String : Any]
            
            DataManager.customerObj = customerObj
        }
        
        let obj = OrderInfoModel()
        obj.firstName = orderInfoModelObj.firstName
        obj.lastName = orderInfoModelObj.lastName
        obj.addressLine1 = orderInfoModelObj.addressLine1
        obj.addressLine2 = orderInfoModelObj.addressLine2
        obj.city = orderInfoModelObj.city
        obj.region = orderInfoModelObj.region
        obj.country = orderInfoModelObj.country
        obj.postalCode = orderInfoModelObj.postalCode
        obj.email = orderInfoModelObj.email
        obj.phone = orderInfoModelObj.phone
        
        obj.shippingFirstName = orderInfoModelObj.shippingFirstName
        obj.shippingLastName = orderInfoModelObj.shippingLastName
        obj.shippingAddressLine1 = orderInfoModelObj.shippingAddressLine1
        obj.shippingAddressLine2 = orderInfoModelObj.shippingAddressLine2
        obj.shippingCity = orderInfoModelObj.shippingCity
        obj.shippingRegion = orderInfoModelObj.shippingRegion
        obj.shippingCountry = orderInfoModelObj.shippingCountry
        obj.shippingPostalCode = orderInfoModelObj.shippingPostalCode
        obj.shippingEmail = orderInfoModelObj.shippingEmail
        obj.shippingPhone = orderInfoModelObj.shippingPhone
        
        obj.orderDate = orderInfoModelObj.orderDate
        obj.orderID = orderInfoModelObj.orderID
        obj.merchantId = orderInfoModelObj.merchantId
        obj.transactionType = orderInfoModelObj.transactionType
        obj.cardHolderName = orderInfoModelObj.cardHolderName
        obj.saleLocation = orderInfoModelObj.saleLocation
        obj.companyName = orderInfoModelObj.companyName
        obj.invoiceterms = orderInfoModelObj.invoiceterms
        obj.invoiceDueDate = orderInfoModelObj.invoiceDueDate
        obj.appName = orderInfoModelObj.appName
        obj.approvalCode = orderInfoModelObj.approvalCode
        obj.entryMethod = orderInfoModelObj.entryMethod
        obj.userID = orderInfoModelObj.userID
        obj.bpId = orderInfoModelObj.bpId
        obj.paymentMethod = orderInfoModelObj.paymentMethod
        obj.googleReceipturl = orderInfoModelObj.googleReceipturl
        obj.showSubscriptionsCancel = orderInfoModelObj.showSubscriptionsCancel
        
        obj.couponCode = orderInfoModelObj.couponCode
        obj.couponPrice = orderInfoModelObj.couponPrice
        obj.couponId = orderInfoModelObj.couponId
        obj.couponTitle = orderInfoModelObj.couponTitle
        
        obj.total = orderInfoModelObj.total
        obj.subTotal = orderInfoModelObj.subTotal
        obj.discount = orderInfoModelObj.discount
        obj.shipping = orderInfoModelObj.shipping
        obj.tax = orderInfoModelObj.tax
        obj.customTax = orderInfoModelObj.customTax
        obj.tip = orderInfoModelObj.tip
        obj.isOrderRefund = orderInfoModelObj.isOrderRefund
        obj.isshippingRefundSelected = orderInfoModelObj.isshippingRefundSelected
        obj.isTipRefundSelected = orderInfoModelObj.isTipRefundSelected
        obj.showRefundTip = orderInfoModelObj.showRefundTip
        obj.showRefundShipping = orderInfoModelObj.showRefundShipping
        
        obj.isSubscription = orderInfoModelObj.isSubscription
        obj.isProductReturn = orderInfoModelObj.isProductReturn
        obj.refundPaymentTypeArray = orderInfoModelObj.refundPaymentTypeArray
        obj.cardDetail = orderInfoModelObj.cardDetail
        obj.Exchange_Payment_Method_arr = orderInfoModelObj.Exchange_Payment_Method_arr
        obj.refundTransactionList = orderInfoModelObj.refundTransactionList
        obj.transactionArray = orderInfoModelObj.transactionArray
        DataManager.defaultTaxID = orderInfoModelObj.customTax
        
        if DataManager.isCaptureButton {
            obj.productsArray = orderInfoModelObj.productsArray
            //DataManager.selectedPayment = obj.paymentMethod
        }
        
        for arrpax in orderInfoModelObj.paxDeviceListRefund{
            obj.paxDeviceListRefund.append(arrpax)
        }
        
        for transaction in orderInfoModelObj.transactionArray {
            if transaction.isPartialRefundSelected || transaction.isFullRefundSelected || transaction.isVoidSelected {
                if transaction.isPartialRefundSelected && transaction.partialAmount != "" {
                    isPartialAmountFound = true
                }
                if DataManager.isshippingRefundOnly {
                    obj.transactionArray.append(transaction)
                }
                isTransactiontFound = true
            }
        }
        
        for product in orderInfoModelObj.productsArray {
            if product.isExchangeSelected {
                obj.productsArray.append(product)
                isExchangeProductFound = true
            }
        }
        
        if !isExchangeProductFound {
            for product in orderInfoModelObj.productsArray {
                if product.isRefundSelected {
                    obj.productsArray.append(product)
                    isRefundProductFound = true
                }
            }
        }else {
            for product in orderInfoModelObj.productsArray {
                if product.isRefundSelected {
                    obj.productsArray.append(product)
                    isRefundProductFound = false
                }
            }
        }
        
        return (isExchangeProductFound, isRefundProductFound, isTransactiontFound, isPartialAmountFound, obj)
    }
    
    
    func ClearbButtonColorChange() {
        
        var isCheck = false
        
        for i in 0..<orderInfoModelObj.productsArray.count {
            if orderInfoModelObj.productsArray[i].isExchangeSelected {
                print(orderInfoModelObj.productsArray[i].isExchangeSelected)
                isCheck = true
            }
            
            if orderInfoModelObj.productsArray[i].isRefundSelected {
                print(orderInfoModelObj.productsArray[i].isRefundSelected)
                isCheck = true
            }
        }
        
        for i in 0..<orderInfoModelObj.transactionArray.count {
            if orderInfoModelObj.transactionArray[i].isFullRefundSelected {
                print(orderInfoModelObj.transactionArray[i].isFullRefundSelected)
                isCheck = true
            }
            
            if orderInfoModelObj.transactionArray[i].isPartialRefundSelected {
                print(orderInfoModelObj.transactionArray[i].isPartialRefundSelected)
                isCheck = true
            }
            
            if orderInfoModelObj.transactionArray[i].isVoidSelected {
                print(orderInfoModelObj.transactionArray[i].isVoidSelected)
                isCheck = true
            }
        }
        
//        if isCheck {
//            btnClear.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.462745098, blue: 0.7882352941, alpha: 1)
//            btnClear.borderColor = #colorLiteral(red: 0.0431372549, green: 0.462745098, blue: 0.7882352941, alpha: 1)
//            btnClear.isUserInteractionEnabled = true
//            refundButton.isUserInteractionEnabled = true
//            refundButton.backgroundColor = #colorLiteral(red: 0.007843137255, green: 0.6196078431, blue: 0.137254902, alpha: 1)
//            refundButton.borderColor = #colorLiteral(red: 0.007843137255, green: 0.6196078431, blue: 0.137254902, alpha: 1)
//        } else {
//            btnClear.backgroundColor = UIColor.gray
//            btnClear.borderColor = UIColor.gray
//            btnClear.isUserInteractionEnabled = false
//            refundButton.isUserInteractionEnabled = false
//            refundButton.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.462745098, blue: 0.7882352941, alpha: 1)
//            refundButton.borderColor = #colorLiteral(red: 0.0431372549, green: 0.462745098, blue: 0.7882352941, alpha: 1)
//        }
    }
    
    //MARK: IBAction
    
    @IBAction func actionUserId(_ sender: Any) {
        
        // Note : -- this code is right, only disable some time
        
        //        if UI_USER_INTERFACE_IDIOM() == .pad {
        //            userDetailDelegate?.didgetUserOrderId!(with: orderID)
        //            self.userContainer.isHidden = false
        //        }else{
        //
        //            appDelegate.strIphoneApi = false
        //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //            let vc = storyboard.instantiateViewController(withIdentifier: "UserDetailViewController") as! UserDetailViewController
        //            vc.orderIdStr = orderID
        //            self.present(vc, animated: true, completion: nil)
        //
        //        }
    }
    
    @IBAction func clearButtonAction(_ sender: Any) {
        self.view.endEditing(true)
//        self.refundButton.isUserInteractionEnabled = false
//        self.refundButton.alpha = 1.0
//        btnClear.backgroundColor = UIColor.gray
//        refundButton.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.462745098, blue: 0.7882352941, alpha: 1)
//        btnClear.borderColor = UIColor.gray
//        refundButton.borderColor = #colorLiteral(red: 0.0431372549, green: 0.462745098, blue: 0.7882352941, alpha: 1)
//        btnClear.isUserInteractionEnabled = false
        resetAllValues()
        reloadTableData()
        
        ClearbButtonColorChange()
    }
    
    @IBAction func btnPayNow_Action(_ sender: Any) {
        let Url = self.orderInfoModelObj.payNowUrl
        let str = Url.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        if let url:NSURL = NSURL(string: str!) {
            UIApplication.shared.open(url as URL)
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        if let vcs = self.navigationController?.viewControllers {
            for vc in vcs {
                if vc.isKind(of: OrdersViewController.self) {
                    self.navigationController?.popToViewController(vc, animated: true)
                    return
                }
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func refundButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        
        if versionOb < 4 {
            let dataChange = self.orderInfoModelObj.refundUrl.replacingOccurrences(of: "userID", with: "custID")
            let Url = dataChange
            let str = Url.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
            if let url:NSURL = NSURL(string: str!) {
                UIApplication.shared.open(url as URL)
            }
            return
        }
        
        DataManager.isCaptureButton = false
        
        if (self.revealViewController().frontViewPosition != FrontViewPositionLeft) {
            self.revealViewController()?.revealToggle(animated: true)
        }
        let updatedData = getUpdatedData()
        
        //        if !updatedData.0 && !updatedData.1 {
        //            self.showAlert(message: "Please select an item.")
        //            return
        //        }
        
        if !updatedData.0 && !updatedData.2 {
//            self.showAlert(message: "Please select refund type.")
            appDelegate.showToast(message: "Please select refund type.")
            return
        }
        
        let refundselectedArray = orderInfoModelObj.transactionArray.filter({$0.isPartialRefundSelected == true})
        let refundselectedAmountArray = orderInfoModelObj.transactionArray.filter({$0.isPartialRefundSelected == true && $0.partialAmount != ""})
        
        if !updatedData.3 && refundselectedArray.count > 0 {
//            self.showAlert(message: "Please enter partial amount.")
            appDelegate.showToast(message: "Please enter partial amount.")
            return
        }
        
        if updatedData.3 && refundselectedArray.count != refundselectedAmountArray.count {
//            self.showAlert(message: "Please enter partial amount.")
            appDelegate.showToast(message: "Please enter partial amount.")
            return
        }
        
        
        if updatedData.3 && refundselectedArray.count > 0 {
            
            var totalRefundAmount = Double()
            
            for product in orderInfoModelObj.productsArray {
                if product.isRefundSelected || product.isExchangeSelected {
                    totalRefundAmount += product.refundAmount
                }
            }
            totalRefundAmount = Double(totalRefundAmount.roundToTwoDecimal) ?? 0
            
            for transaction in orderInfoModelObj.transactionArray {
                if transaction.isPartialRefundSelected {
                    if (Double(transaction.partialAmount) ?? 0) < totalRefundAmount {
//                        self.showAlert(message: "Transaction amount must be greater than or equal to refunded amount.")
                        appDelegate.showToast(message: "Transaction amount must be greater than or equal to refunded amount.")
                        return
                    }
                    
                    if (Double(transaction.partialAmount) ?? 0) > transaction.availableRefundAmount {
//                        self.showAlert(message: "Transaction amount must be less than or equal to refunded amount $\(transaction.availableRefundAmount)")
                        appDelegate.showToast(message: "Transaction amount must be less than or equal to refunded amount $\(transaction.availableRefundAmount)")
                        return
                    }
                }
            }
            
        }
        
        if updatedData.3 && refundselectedArray.count > 0 {
            for transaction in orderInfoModelObj.transactionArray {
                if transaction.isPartialRefundSelected && (Double(transaction.partialAmount) ?? 0) > transaction.availableRefundAmount {
//                    self.showAlert(message: "Transaction amount must be less than or equal to refunded amount $\(transaction.availableRefundAmount)")
                    appDelegate.showToast(message: "Transaction amount must be less than or equal to refunded amount $\(transaction.availableRefundAmount)")
                    return
                }
            }
        }
        
        
        if updatedData.0 {
//            showAlert(message: "Feature is not available for this release.")
            appDelegate.showToast(message: "Feature is not available for this release.")
            //code comment for exchage bcoz work in going on. so that we add dialog // by sudama 14-12-2019
            /*PaymentsViewController.paymentDetailDict.removeAll()
             
             if let cardDetail = orderInfoModelObj.cardDetail {
             var number = cardDetail.number ?? ""
             
             if number.count == 4 {
             number = "************" + number
             }
             
             PaymentsViewController.paymentDetailDict["key"] = "CREDIT"
             PaymentsViewController.paymentDetailDict["data"] = ["cardnumber":number,"mm":cardDetail.month, "yyyy":cardDetail.year, "cvv": ""]
             }
             
             let dict: JSONDictionary = ["data":updatedData.4]
             
             if UI_USER_INTERFACE_IDIOM() == .pad {
             appDelegate.str_Refundvalue = "refund"
             self.orderInfoDelegate?.didMoveToCartScreen?(with: dict)
             } else {
             appDelegate.str_Refundvalue = "refund"
             self.refundOrder = dict
             self.performSegue(withIdentifier: "cart", sender: nil)
             }*/
            return
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let dict: JSONDictionary = ["show":true, "data":updatedData.4]
            self.orderInfoDelegate?.didUpdateRefundScreen?(with: dict)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RefundViewController") as! RefundViewController
            vc.orderInfoModelObj = updatedData.4
            vc.orderInfoDelegate = self
            //self.present(vc, animated: true, completion: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func actionCapture(_ sender: Any) {
        
        
        //self.showAlert(message: "Capture under Developement")
        
        DataManager.isCaptureButton = true
        appDelegate.str_Refundvalue = "refund"
        print(orderInfoModelObj.transactionArray)
        print(OrderInfoModel.self)
        
        let update = getUpdatedData()
        
        let dict: JSONDictionary = ["data":update.4]
        
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            self.orderInfoDelegate?.didMoveToCartScreen?(with: dict)
        } else {
            
            //self.showAlert(message: "Capture under Developement")
            
            self.refundOrder = dict
            self.performSegue(withIdentifier: "cart", sender: nil)
        }
        
    }
    
    
    @IBAction func receiptButtonAction(_ sender: Any) {
        self.blurView.isHidden = true
        blurView.backgroundColor = UIColor.white
        
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let text: UIAlertAction = UIAlertAction(title: "Text", style: .default) { action -> Void in
            UIView.animate(withDuration: 0.2, animations: {
                self.blurView.alpha = 0
            }) { (_) in
                self.blurView.isHidden = true
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ReceiptViewController") as! ReceiptViewController
            controller.isReceiptText = true
            controller.orderID = self.orderID
            controller.selectedButonType = .text
            controller.userPhone = self.orderInfoModelObj.phone
            controller.userEmail = self.orderInfoModelObj.email
            controller.strGooglePrinterURL = self.orderInfoModelObj.googleReceipturl
            if #available(iOS 13.0, *) {
                controller.modalPresentationStyle = .fullScreen
            }
            self.navigationController?.present(controller, animated: true, completion: nil)
        }
        
        let email: UIAlertAction = UIAlertAction(title: "Email", style: .default) { action -> Void in
            UIView.animate(withDuration: 0.2, animations: {
                self.blurView.alpha = 0
            }) { (_) in
                self.blurView.isHidden = true
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ReceiptViewController") as! ReceiptViewController
            controller.isReceiptEmail = true
            controller.orderID = self.orderID
            controller.selectedButonType = .email
            controller.userPhone = self.orderInfoModelObj.phone
            controller.strGooglePrinterURL = self.orderInfoModelObj.googleReceipturl
            controller.userEmail = self.orderInfoModelObj.email
            if #available(iOS 13.0, *) {
                controller.modalPresentationStyle = .fullScreen
            }
            self.navigationController?.present(controller, animated: true, completion: nil)
        }
        
        let print: UIAlertAction = UIAlertAction(title: "Print", style: .default) { action -> Void in
            UIView.animate(withDuration: 0.2, animations: {
                self.blurView.alpha = 0
            }) { (_) in
                self.blurView.isHidden = true
            }
            
            if !DataManager.isBluetoothPrinter && !DataManager.isCloudReceiptPrinter {
//                self.showAlert(message: "Please first enable the printer from settings")
                appDelegate.showToast(message: "Please first enable the printer from settings")
                return
            }
            
            if DataManager.isBluetoothPrinter {
                
                
                self.callAPItoGetReceiptContent()
            }
            
            if DataManager.isCloudReceiptPrinter {
                self.callAPItoGetReceiptContent()
//                let Url = self.orderInfoModelObj.googleReceipturl
//                //let str = Url.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
//                if let url:NSURL = NSURL(string: Url) {
//                    UIApplication.shared.open(url as URL)
//                }
            }
        }
        
        var cancelAction = UIAlertAction()
        if UI_USER_INTERFACE_IDIOM() == .pad
        {
            cancelAction.setValue(UIColor.init(red: 11.0/255.0, green: 118.0/255.0, blue: 201.0/255.0, alpha: 1.0), forKey: "titleTextColor")
            cancelAction = UIAlertAction(title: "Cancel", style: .default) { action -> Void in
                UIView.animate(withDuration: 0.2, animations: {
                    self.blurView.alpha = 0
                }) { (_) in
                    self.blurView.isHidden = true
                }
            }
        }
        else
        {
            cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
                UIView.animate(withDuration: 0.2, animations: {
                    self.blurView.alpha = 0
                }) { (_) in
                    self.blurView.isHidden = true
                }
            }
        }
        
        actionSheetController.view.tintColor = UIColor.init(red: 11.0/255.0, green: 118.0/255.0, blue: 201.0/255.0, alpha: 1.0)
        actionSheetController.addAction(text)
        actionSheetController.addAction(email)
        
        if DataManager.isBluetoothPrinter || DataManager.isCloudReceiptPrinter {
            actionSheetController.addAction(print)
        }
        actionSheetController.addAction(cancelAction)
        
        if UI_USER_INTERFACE_IDIOM() == .pad
        {
            if let popoverController = actionSheetController.popoverPresentationController
            {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.frame.size.height, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
        }
        
        if let popoverController = actionSheetController.popoverPresentationController {
            popoverController.delegate = self
        }
        
        DispatchQueue.main.async {
            self.present(actionSheetController, animated: true, completion: {
                self.blurView.alpha = 0
                self.blurView.isHidden = false
                UIView.animate(withDuration: 0.2, animations: {
                    self.blurView.alpha = 0.4
                })
            })
        }
    }
    
    @IBAction func orderSummaryButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        self.scrollView.scrollToTop()
        
        if versionOb < 4 {
            if UI_USER_INTERFACE_IDIOM() == .pad {
                //            tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat(orderInfoModelObj.paymentStatus.lowercased() == "invoice" ? 40 : 100))
                tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat((orderInfoModelObj.paymentStatus.lowercased() == "invoice" || !orderInfoModelObj.isOrderRefund) ? 40 : 40))
                
            }else{
                tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat((orderInfoModelObj.paymentStatus.lowercased() == "invoice" || !orderInfoModelObj.isOrderRefund) ? 40 : 70))
                
            }
        } else {
            tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat((orderInfoModelObj.paymentStatus.lowercased() == "invoice" || !orderInfoModelObj.isOrderRefund) ? 40 : 120))

            
//            if UI_USER_INTERFACE_IDIOM() == .pad {
//                //            tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat(orderInfoModelObj.paymentStatus.lowercased() == "invoice" ? 40 : 100))
//                tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat((orderInfoModelObj.paymentStatus.lowercased() == "invoice" || !orderInfoModelObj.isOrderRefund) ? 40 : 120))
//
//            }else{
//                tableView.tableFooterView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat((orderInfoModelObj.paymentStatus.lowercased() == "invoice" || !orderInfoModelObj.isOrderRefund) ? 40 : 70))
//
//            }
        }
        
        
        self.handleOrderSummaryButtonAction()
    }
    
    @IBAction func transactionButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        self.tableView.contentOffset = .zero
        self.scrollView.contentOffset = .zero
        self.scrollView.scrollToTop()
        
        if versionOb < 4 {
            if UI_USER_INTERFACE_IDIOM() == .pad {
                self.tableView.tableFooterView?.frame.size = CGSize(width: self.tableView.frame.width, height: 40)
                // self.addressviewHeightConstraint.constant = 0
                self.orderSummaryLineView.backgroundColor = #colorLiteral(red: 0.9371728301, green: 0.9373074174, blue: 0.9371433854, alpha: 1)
            } else {
                self.transactionLineView.isHidden = false
                self.orderSummaryLineView.isHidden = true
            }
        } else {
            self.tableView.tableFooterView?.frame.size = CGSize(width: self.tableView.frame.width, height: 120)

            if UI_USER_INTERFACE_IDIOM() == .pad {
                // self.addressviewHeightConstraint.constant = 0
                self.orderSummaryLineView.backgroundColor = #colorLiteral(red: 0.9371728301, green: 0.9373074174, blue: 0.9371433854, alpha: 1)
            } else {
                self.transactionLineView.isHidden = false
                self.orderSummaryLineView.isHidden = true
            }
        }
        
        
        self.isOrderSummery = false
        self.isTapOnTransactionButton = true
        self.tableView.isScrollEnabled = true
        self.scrollView.isScrollEnabled = false
        
        self.orderDetailView.isHidden = true
        self.tableFooterView.isHidden = true
        self.orderAddressDetailView.isHidden = true
        
        self.orderSummaryButton.setTitleColor(#colorLiteral(red: 0.5489655137, green: 0.5647396445, blue: 0.603846848, alpha: 1), for: .normal)
        self.transactionButton.setTitleColor(UIColor.HieCORColor.blue.colorWith(alpha: 1.0), for: .normal)
        self.transactionLineView.backgroundColor = UIColor.HieCORColor.blue.colorWith(alpha: 1.0)
        self.tableView.tableHeaderView?.frame.size = CGSize(width: self.tableView.frame.width, height: CGFloat(0))
        
        self.transactionInfoArray.removeAll()
        self.reloadTableData(animate: true)
        
        self.callAPItoGetTransactionInfo()
    }
}

//MARK: UIPopoverPresentationControllerDelegate
extension OrderInfoViewController {
    override func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        UIView.animate(withDuration: 0.2, animations: {
            self.blurView.alpha = 0
        }) { (_) in
            self.blurView.isHidden = true
        }
    }
}

//MARK: UserDetailsDelegate
extension OrderInfoViewController: UserDetailsDelegate {
    func didHideUserDetailView(isRefresh: Bool) {
        
        if isRefresh {
            callAPItoGetOrderInfo()
            //orderInfoDelegate?.didGetOrderInformation?(with: orderID, defualtView: true)
        }
        userContainer.isHidden = true
    }
}


//MARK: OrderInfoViewControllerDelegate
extension OrderInfoViewController: OrderInfoViewControllerDelegate {
    func didHideRefundView(isRefresh: Bool) {
        if isRefresh {
            self.scrollView.scrollToTop()
            self.didGetOrderInformation(with: orderID, defualtView : true)
        }
    }
    
    func didGetOrderInformation(with orderId: String, defualtView : Bool) {
        isTapOnTransactionButton = false
        orderID = orderId
        orderIdLabel.text = "#\(orderID)"
        orderIDlabel.text = "#\(orderID)"
        if defualtView {
            defualtViewFlag = true
            orderInfoDelegate?.didUpdateHeaderText?(with: "Orders")
        }else{
            defualtViewFlag = false
            orderInfoDelegate?.didUpdateHeaderText?(with: "#\(orderID)")
        }
        
        
        //Offline
        if !NetworkConnectivity.isConnectedToInternet() && DataManager.isOffline {
            Indicator.isEnabledIndicator = true
            Indicator.sharedInstance.hideIndicator()
            return
        }
        
        //Online
        blurView.backgroundColor = UIColor.black
        blurView.isHidden = true
        callAPItoGetOrderInfo()
    }
    
    func didProductRefunded() {
        isTapOnTransactionButton = false
        callAPItoGetOrderInfo()
    }
    
    func didProductReturned() {
        isTapOnTransactionButton = false
        callAPItoGetOrderInfo()
    }
}

//MARK: UITableViewDataSource, UITableViewDelegate
extension OrderInfoViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return isOrderSummery ? 3 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isOrderSummery {
            switch section {
            case 0: return orderInfoModelObj.productsArray.count + 1
            case 1: return 1
            case 2: return arrTransactionData.count ?? 0
            default: return orderInfoModelObj.transactionArray.count
            }
        }else {
            return transactionInfoArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isOrderSummery {
            switch indexPath.section {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: UI_USER_INTERFACE_IDIOM() == .phone ? "ItemsDetailTableCell" : isLandscape ? "ItemsDetailTableCell" : "ItemsDetailTableCell") as! ItemsDetailTableCell
                
                if UI_USER_INTERFACE_IDIOM() == .pad {
                    cell.headerView.isHidden = indexPath.row != 0
                    cell.footerView.isHidden = indexPath.row == 0
                    cell.lineView.isHidden = indexPath.row != 0
                } else {
                    cell.refundExchangeStackView.axis = UIScreen.main.bounds.width < 365 ? .vertical : .horizontal
                }
                
                cell.refundButton.isSelected = false
                if versionOb < 4{
                    if isLandscape {
                        cell.labelAction.text = "      "
                    }
                    if indexPath.row > 0 {
                        //cell.crossButton.tag = indexPath.row - 1
                        //cell.crossButton.addTarget(self, action: #selector(handleCrossAction(sender:)), for: .touchUpInside)
                        //cell.exchangeButton.tag = indexPath.row - 1
                        //cell.exchangeButton.addTarget(self, action: #selector(handleExchangeAction(sender:)), for: .touchUpInside)
                        
                        cell.refundButton.tag = indexPath.row - 1
                        cell.refundButton.addTarget(self, action: #selector(handleRefundAction(sender:)), for: .touchUpInside)
                        
                        
                        if UI_USER_INTERFACE_IDIOM() == .phone {
//                            if orderInfoModelObj.productsArray[indexPath.row - 1].isExchangeSelected {
//                                cell.exchangeButton.backgroundColor = UIColor.init(red: 11/255, green: 118/255, blue: 201/255, alpha: 1.0)
//                                cell.exchangeButton.setTitleColor(UIColor.white, for: .normal)
//                            } else {
//                                cell.exchangeButton.backgroundColor = UIColor.white
//                                cell.exchangeButton.setTitleColor(UIColor.init(red: 11/255, green: 118/255, blue: 201/255, alpha: 1.0), for: .normal)
//                            }
                            
//                            if orderInfoModelObj.productsArray[indexPath.row - 1].isRefundSelected {
//                                cell.refundButton.backgroundColor = UIColor.init(red: 11/255, green: 118/255, blue: 201/255, alpha: 1.0)
//                                cell.refundButton.setTitleColor(UIColor.white, for: .normal)
//                            } else {
//                                cell.refundButton.backgroundColor = UIColor.white
//                                cell.refundButton.setTitleColor(UIColor.init(red: 11/255, green: 118/255, blue: 201/255, alpha: 1.0), for: .normal)
//                            }
                            cell.refundButton.isSelected = orderInfoModelObj.productsArray[indexPath.row - 1].isRefundSelected

                        }else {
                            //cell.exchangeButton.isSelected = orderInfoModelObj.productsArray[indexPath.row - 1].isExchangeSelected
                            cell.refundButton.isSelected = orderInfoModelObj.productsArray[indexPath.row - 1].isRefundSelected
                        }
                        
                        let title = orderInfoModelObj.productsArray[indexPath.row - 1].title
                        cell.itemNameLabel.text = title == "" ? "   " : title
                        cell.qtyLabel.text = orderInfoModelObj.productsArray[indexPath.row - 1].qty.newValue
                        cell.priceLabel.text = orderInfoModelObj.productsArray[indexPath.row - 1].mainPrice.currencyFormat
                        cell.totalLabel.text = (orderInfoModelObj.productsArray[indexPath.row - 1].qty * orderInfoModelObj.productsArray[indexPath.row - 1].price).currencyFormat
                        cell.variationTitleLabel.text = orderInfoModelObj.productsArray[indexPath.row - 1].attribute.replacingOccurrences(of: ",", with: "\n")
                        let value  =  "\(orderInfoModelObj.productsArray[indexPath.row - 1].code) \n \(orderInfoModelObj.productsArray[indexPath.row - 1].man_desc)"
                        cell.labelNotes.text = value
                        if UI_USER_INTERFACE_IDIOM() == .phone {
                            if cell.variationTitleLabel.text == "" {
                                cell.variationTitleLabel.isHidden = true
                            }
                            if orderInfoModelObj.productsArray[indexPath.row - 1].code == "" && orderInfoModelObj.productsArray[indexPath.row - 1].man_desc == "" {
                                cell.labelNotes.isHidden = true
                            }
                        }
                        if !orderInfoModelObj.isOrderRefund || orderInfoModelObj.productsArray[indexPath.row - 1].isRefunded || orderInfoModelObj.productsArray[indexPath.row - 1].isExchanged || orderInfoModelObj.paymentStatus.lowercased() == "invoice" {
//                            cell.crossButton.isHidden = true
//                            cell.exchangeButton.isHidden = true
                            cell.refundButton.isHidden = true
                            cell.refundedLabel.isHidden = true
                            cell.contentView.layoutIfNeeded()
                            // cell.refundedLabel.text = orderInfoModelObj.paymentStatus.lowercased() == "invoice" ? "No Transaction" : !orderInfoModelObj.isOrderRefund ? "No Transaction" : orderInfoModelObj.productsArray[indexPath.row - 1].isExchanged ? "Exchanged" : "Refunded"
                            
                            if orderInfoModelObj.paymentStatus.lowercased() == "invoice" || !orderInfoModelObj.isOrderRefund {
                                cell.refundedLabel.text = " "
                            }
                            
                            if orderInfoModelObj.productsArray[indexPath.row - 1].isExchanged || orderInfoModelObj.productsArray[indexPath.row - 1].isRefunded {
                                cell.refundedLabel.text = orderInfoModelObj.productsArray[indexPath.row - 1].isExchanged ? "   Exchanged" : "   Refunded"
                            }
                            
                            cell.contentView.alpha = (orderInfoModelObj.paymentStatus.lowercased() == "invoice" || !orderInfoModelObj.isOrderRefund || orderInfoModelObj.productsArray[indexPath.row - 1].isExchanged || orderInfoModelObj.productsArray[indexPath.row - 1].isRefunded) ? 0.5 : 1.0
                        }else {
                            //cell.crossButton.isHidden = true
                           // cell.exchangeButton.isHidden = true
                            cell.refundButton.isHidden = true
                            cell.refundedLabel.isHidden = true
                            cell.contentView.layoutIfNeeded()
                            
                            //cell.crossButton.isUserInteractionEnabled = orderInfoModelObj.productsArray[indexPath.row - 1].isRefundSelected || orderInfoModelObj.productsArray[indexPath.row - 1].isExchangeSelected
                            //cell.crossButton.alpha = (orderInfoModelObj.productsArray[indexPath.row - 1].isRefundSelected || orderInfoModelObj.productsArray[indexPath.row - 1].isExchangeSelected) ? 1 : 0
                            cell.contentView.alpha = 1.0
                        }
                        if indexPath.row == orderInfoModelObj.productsArray.count && UI_USER_INTERFACE_IDIOM() == .phone {
                            cell.bottomLine.isHidden = true
                        }
                    }
                }else{
                    if indexPath.row > 0 {
                        
                        cell.refundButton.tag = indexPath.row - 1
                        cell.refundButton.addTarget(self, action: #selector(handleRefundAction(sender:)), for: .touchUpInside)
                        cell.refundButton.isSelected = orderInfoModelObj.productsArray[indexPath.row - 1].isExchangeSelected

                        let title = orderInfoModelObj.productsArray[indexPath.row - 1].title
                        cell.itemNameLabel.text = title == "" ? "   " : title
                        cell.qtyLabel.text = orderInfoModelObj.productsArray[indexPath.row - 1].qty.newValue
                        cell.priceLabel.text = orderInfoModelObj.productsArray[indexPath.row - 1].mainPrice.currencyFormat
                        cell.totalLabel.text = (orderInfoModelObj.productsArray[indexPath.row - 1].qty * orderInfoModelObj.productsArray[indexPath.row - 1].price).currencyFormat
                        cell.variationTitleLabel.text = orderInfoModelObj.productsArray[indexPath.row - 1].attribute.replacingOccurrences(of: ",", with: "\n")
                        let value  =  "\(orderInfoModelObj.productsArray[indexPath.row - 1].code) \n \(orderInfoModelObj.productsArray[indexPath.row - 1].man_desc)"
                        
                        
                        var dataVal = ""
                        
                        if orderInfoModelObj.productsArray[indexPath.row - 1].code != "" && orderInfoModelObj.productsArray[indexPath.row - 1].man_desc != "" {
                            dataVal = "\(orderInfoModelObj.productsArray[indexPath.row - 1].code) \n \(orderInfoModelObj.productsArray[indexPath.row - 1].man_desc)"
                        } else if orderInfoModelObj.productsArray[indexPath.row - 1].man_desc != ""{
                            dataVal = orderInfoModelObj.productsArray[indexPath.row - 1].man_desc
                        } else if orderInfoModelObj.productsArray[indexPath.row - 1].code != "" {
                            dataVal = orderInfoModelObj.productsArray[indexPath.row - 1].code
                        }
                        cell.labelNotes.text = dataVal
                        if UI_USER_INTERFACE_IDIOM() == .phone {
                            if cell.variationTitleLabel.text == "" {
                                cell.variationTitleLabel.isHidden = true
                            }
                            if orderInfoModelObj.productsArray[indexPath.row - 1].code == "" && orderInfoModelObj.productsArray[indexPath.row - 1].man_desc == "" {
                                cell.labelNotes.isHidden = true
                            }
                        }
                        if !orderInfoModelObj.isOrderRefund || orderInfoModelObj.productsArray[indexPath.row - 1].isRefunded || orderInfoModelObj.productsArray[indexPath.row - 1].isExchanged || orderInfoModelObj.paymentStatus.lowercased() == "invoice" {
                            cell.refundButton.isHidden = true
                            cell.refundedLabel.isHidden = false
                            //                        cell.refundedLabel.text = orderInfoModelObj.paymentStatus.lowercased() == "invoice" ? "No Transaction" : !orderInfoModelObj.isOrderRefund ? "No Transaction" : orderInfoModelObj.productsArray[indexPath.row - 1].isExchanged ? "Exchanged" : "Refunded"
                            
                            if orderInfoModelObj.paymentStatus.lowercased() == "invoice" || !orderInfoModelObj.isOrderRefund {
                                cell.refundButton.isHidden = true
                                cell.refundedLabel.text = " "
                                cell.layoutIfNeeded()
                            }
                            
                            if orderInfoModelObj.isOrderRefund == true && orderInfoModelObj.product_status == "" {
                                self.view.layoutIfNeeded()
                                cell.refundButton.isHidden = false
                                cell.refundedLabel.isHidden = true
                            }
                            if orderInfoModelObj.paymentStatus == "AUTH" {
                                cell.refundedLabel.isHidden = true
                            }
                            if orderInfoModelObj.paymentStatus == "ERROR" {
                                cell.refundedLabel.isHidden = true
                            }
                            if orderInfoModelObj.paymentStatus == "VOID" {
                                cell.refundedLabel.isHidden = true
                            }
                            
                            
                            if orderInfoModelObj.productsArray[indexPath.row - 1].isExchanged || orderInfoModelObj.productsArray[indexPath.row - 1].isRefunded {
                                cell.refundedLabel.text = orderInfoModelObj.productsArray[indexPath.row - 1].isExchanged ? "   Exchanged" : "   Refunded"
                                cell.refundedLabel.isHidden = false
                                cell.refundButton.isHidden = true
                                cell.layoutIfNeeded()
                                
                            }
                            
                            cell.contentView.alpha = ( !orderInfoModelObj.isOrderRefund || orderInfoModelObj.productsArray[indexPath.row - 1].isExchanged || orderInfoModelObj.productsArray[indexPath.row - 1].isRefunded) ? 0.5 : 1.0
                        }else {
                            self.view.layoutIfNeeded()
                            cell.refundButton.isHidden = false
                            cell.refundedLabel.isHidden = true
                            cell.contentView.alpha = 1.0
                            if orderInfoModelObj.productsArray[indexPath.row - 1].availableQtyRefund <= 0.0 {
                                cell.refundButton.isHidden = true
                                cell.refundedLabel.isHidden = true
                            }
                        }
                        
                        if indexPath.row == orderInfoModelObj.productsArray.count && UI_USER_INTERFACE_IDIOM() == .phone {
                            cell.bottomLine.isHidden = true
                        }
                    }
                }
                
                cell.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
                
                if(UI_USER_INTERFACE_IDIOM() == .pad)
                {
                    if (UIInterfaceOrientationIsLandscape(UIApplication.shared.statusBarOrientation)) {
                        cell.constraintHeadItem.constant = 220
                        cell.ConstraintProductName.constant = 220
                    } else {
                        cell.constraintHeadItem.constant = 110
                        cell.ConstraintProductName.constant = 110
                    }
                }
                
                
                return cell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentDetailTableCell") as! PaymentDetailTableCell
                
                var isRefund = false
                var isShippingRefund = false
                var isTipRefund = false
                for i in 0..<orderInfoModelObj.productsArray.count {
                    if orderInfoModelObj.productsArray[i].isExchangeSelected == true {
                        isRefund = true
                    }
                }
                if orderInfoModelObj.isshippingRefundSelected == true {
                    isShippingRefund = true
                    isRefund = true
                }
                
                if orderInfoModelObj.isTipRefundSelected == true {
                    isTipRefund = true
                    isRefund = true
                }
                
                cell.subtotalLabel.text = orderInfoModelObj.subTotal.currencyFormat
                cell.discountLabel.text =  orderInfoModelObj.discount.currencyFormat
                cell.shippingLabel.text =  orderInfoModelObj.shipping.currencyFormat
                cell.taxLabel.text = orderInfoModelObj.tax.currencyFormat
                cell.labelTotalMain.text = orderInfoModelObj.total.currencyFormat
                
                cell.discountView.isHidden = orderInfoModelObj.discount <= 0
                cell.shippingView.isHidden = orderInfoModelObj.shipping <= 0
                
                DataManager.shippingValue = orderInfoModelObj.shipping
                
                cell.taxView.isHidden = orderInfoModelObj.tax <= 0
                
                cell.couponLabel.isHidden = orderInfoModelObj.couponCode == ""
                cell.couponView.isHidden = orderInfoModelObj.couponTitle == ""
                cell.tipLabel.text = orderInfoModelObj.tip.currencyFormat
                cell.tipView.isHidden = orderInfoModelObj.tip <= 0
                
                cell.btnRefund.tag = indexPath.row - 1
                cell.btnRefund.addTarget(self, action: #selector(handleRefundProductAction(sender:)), for: .touchUpInside)

                if orderInfoModelObj.productsArray.count == 0 {
                    cell.refundShipping.tag = indexPath.row + 1
                    cell.btnTipRefund.tag = indexPath.row + 1
                } else {
                    cell.refundShipping.tag = indexPath.row
                    cell.btnTipRefund.tag = indexPath.row
                }
                //cell.refundShipping.tag = indexPath.row
                cell.refundShipping.addTarget(self, action: #selector(handleShippingRefundAction(sender:)), for: .touchUpInside)
                cell.btnTipRefund.addTarget(self, action: #selector(handelTipRefundAction(sender:)), for: .touchUpInside)

                if orderInfoModelObj.paymentStatus == "AUTH" {
                    cell.btnRefund.isHidden = true
                    cell.btnVoid.isHidden = true
                } else {
                    if isRefund {
                        cell.btnRefund.isHidden = false
                        cell.btnVoid.isHidden = false
                    } else {
                        cell.btnRefund.isHidden = true
                        cell.btnVoid.isHidden = true
                    }
                }
                
                if isShippingRefund {
                    cell.refundShipping.isSelected = true
                } else {
                    cell.refundShipping.isSelected = false
                }
                
                if isTipRefund {
                    cell.btnTipRefund.isSelected = true
                } else {
                    cell.btnTipRefund.isSelected = false
                }
                
                if versionOb < 4 {
                    cell.btnRefund.isHidden = false
                    cell.btnVoid.isHidden = false
                    cell.btnRefund.setTitle("Refund", for: .normal)
                    if orderInfoModelObj.paymentStatus == "AUTH" {
                        cell.btnRefund.isHidden = true
                        cell.btnVoid.isHidden = true
                    }
                    
//                    if orderInfoModelObj.transactionArray.count == 0 {
//                        cell.btnRefund.isHidden = true
//                        cell.btnVoid.isHidden = true
//                    }
                }

                if UI_USER_INTERFACE_IDIOM() == .pad {
                   cell.couponLabel.text = "Coupon Applied - (\(orderInfoModelObj.couponCode))"
                    //cell.subtotalWidthConstraint.isActive = true
                    //cell.subtotalStackViewRightConstraint.isActive = !isLandscape
                    
                    //cell.subtotalAmountWidthConstraint.isActive = isLandscape
                    //cell.subtotalWidthConstraint.constant = orderInfoModelObj.discount <= 0 ? 80 : 100
                    cell.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
                } else {
                    // cell.couponView.isHidden = orderInfoModelObj.couponTitle == ""
                    // cell.couponLabel.text = orderInfoModelObj.couponPrice.currencyFormat
                    
//                    if orderInfoModelObj.couponTitle == ""{
//                        cell.couponView.isHidden = orderInfoModelObj.couponTitle == ""
//                    }else{
                        cell.couponView.isHidden = true
                        cell.labelTitleDiscount.text = "Manual Discount Coupon Applied - (\(orderInfoModelObj.couponCode))"
                        cell.couponLabel.text = orderInfoModelObj.couponPrice.currencyFormat
                        
                   // }
                }
                
                if  !orderInfoModelObj.isOrderRefund {
                    cell.btnRefund.isHidden = true
                    cell.btnVoid.isHidden = true
                }
                
                if orderInfoModelObj.showRefundShipping{
                    cell.refundShipping.isHidden = false
                    cell.refundShipping.setTitle("Refund", for: .normal)
                    cell.refundShipping.isUserInteractionEnabled = true
                    //cell.refundShipping.imageView?.image = #imageLiteral(resourceName: "Groupuncheck")
                    if isShippingRefund {
                        cell.refundShipping.setImage(#imageLiteral(resourceName: "Groupcheck"), for: .normal)
                    } else {
                        cell.refundShipping.setImage(#imageLiteral(resourceName: "Groupuncheck"), for: .normal)
                    }
                    
                } else {
                    cell.refundShipping.setTitle(" ", for: .normal)
                    cell.refundShipping.setImage(nil, for: .normal)
                    cell.refundShipping.isUserInteractionEnabled = false
                    //cell.refundShipping.imageView?.image = #imageLiteral(resourceName: "Groupcheck")
                }
                
                if orderInfoModelObj.showRefundTip{
                    cell.btnTipRefund.isHidden = false
                    cell.btnTipRefund.setTitle("Refund", for: .normal)
                    cell.btnTipRefund.isUserInteractionEnabled = true
                    //cell.refundShipping.imageView?.image = #imageLiteral(resourceName: "Groupuncheck")
                    if isTipRefund {
                        cell.btnTipRefund.setImage(#imageLiteral(resourceName: "Groupcheck"), for: .normal)
                    } else {
                        cell.btnTipRefund.setImage(#imageLiteral(resourceName: "Groupuncheck"), for: .normal)
                    }
                    
                } else {
                    cell.btnTipRefund.setTitle(" ", for: .normal)
                    cell.btnTipRefund.setImage(nil, for: .normal)
                    cell.btnTipRefund.isUserInteractionEnabled = false
                    //cell.refundShipping.imageView?.image = #imageLiteral(resourceName: "Groupcheck")
                }
                
                return cell
                
            case 2:
                //let cell = tableView.dequeueReusableCell(withIdentifier: UI_USER_INTERFACE_IDIOM() == .phone ? "TransactionListCell" : isLandscape ? "TransactionListCell" : "TransactionListCell")
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionListCell", for: indexPath)
                
                if arrTransactionData.count == 0 {
                    return cell
                }
                
                let lbl_Name = cell.contentView.viewWithTag(701) as? UILabel
                let lbl_Amount = cell.contentView.viewWithTag(702) as? UILabel
                let lbl_Date = cell.contentView.viewWithTag(703) as? UILabel
                let tipBtn = cell.contentView.viewWithTag(704) as? UIButton
                let tipBtnView = cell.contentView.viewWithTag(705) as? UIView
                
                let data = arrTransactionData[indexPath.row] as? NSDictionary
                lbl_Name?.text = data?.value(forKey: "type") as? String
                lbl_Amount?.text = data?.value(forKey: "amount") as? String
                lbl_Date?.text = data?.value(forKey: "date") as? String
                let isTipBtnShow = data?.value(forKey: "isTipBtn") as? String
                
                
                tipBtn?.tag = indexPath.row
                tipBtn?.addTarget(self, action: #selector(handleOpenCustomAlert(sender:)), for: .touchUpInside)
                
               // if (lbl_Name?.text != nil) {
                    if isTipBtnShow == "true"  {
                        tipBtnView?.isHidden = false
                    } else {
                        tipBtnView?.isHidden = true
                    }
                //}
                
                if UI_USER_INTERFACE_IDIOM() == .phone {
                    lbl_Amount?.textAlignment = .center
                    
                }
                
                if versionOb < 4 {
                    tipBtnView?.isHidden = true
                }
                
                return cell
            default:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: UI_USER_INTERFACE_IDIOM() == .phone ? "TransactionCell" : isLandscape ? "TransactionCell" : "TransactionCell")
                
                return cell!
            }
        }
        
        //Transactions
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionsCell", for: indexPath)
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = UIColor.lightGray
        
        
        let lbl_Status = cell.contentView.viewWithTag(503) as? UILabel
        lbl_Status?.text = transactionInfoArray[indexPath.row].approval
        lbl_Status?.layer.borderWidth = 1.0
        lbl_Status?.layer.cornerRadius = 10
        lbl_Status?.layer.masksToBounds = true
        
        let transactionPrintBtnView = cell.contentView.viewWithTag(511)
        let printBtn = cell.contentView.viewWithTag(510) as? UIButton
        printBtn?.tag = indexPath.row
        printBtn?.addTarget(self, action: #selector(handleTransactionPrintBtnAction(sender: )), for: .touchUpInside)
        
        if transactionInfoArray[indexPath.row].paymentType=="AUTH_CAPTURE" && transactionInfoArray[indexPath.row].approval == "Approved" {
            transactionPrintBtnView?.isHidden = false
            printBtn?.isHidden = false
        } else {
            transactionPrintBtnView?.isHidden = true
            printBtn?.isHidden = true
        }
        
        if transactionInfoArray[indexPath.row].approval == "Approved" {
            transactionPrintBtnView?.isHidden = true
            lbl_Status?.layer.borderColor = UIColor.init(red: 76.0/255.0, green: 217.0/255.0, blue: 100.0/255.0, alpha: 1.0).cgColor
            lbl_Status?.backgroundColor = UIColor.init(red: 206.0/255.0, green: 245.0/255.0, blue: 213.0/255.0, alpha: 1.0)
            lbl_Status?.textColor = UIColor.init(red: 76.0/255.0, green: 217.0/255.0, blue: 100.0/255.0, alpha: 1.0)
        }else {
            transactionPrintBtnView?.isHidden = false
            lbl_Status?.layer.borderColor = UIColor.init(red: 220.0/255.0, green: 142.0/255.0, blue: 139.0/255.0, alpha: 1.0).cgColor
            lbl_Status?.backgroundColor = UIColor.init(red: 245.0/255.0, green: 211.0/255.0, blue: 206.0/255.0, alpha: 1.0)
            lbl_Status?.textColor = UIColor.init(red: 220.0/255.0, green: 142.0/255.0, blue: 139.0/255.0, alpha: 1.0)
        }
        
        if versionOb < 4 {
            transactionPrintBtnView?.isHidden = true
            printBtn?.isHidden = true
        }
        
        let lbl_TransactionID = cell.contentView.viewWithTag(501) as? UILabel
        lbl_TransactionID?.text = "#\(transactionInfoArray[indexPath.row].txnId)"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let newDate = dateFormatter.date(from: transactionInfoArray[indexPath.row].dateAdded) ?? Date()
        
        let lbl_TransactionDate = cell.contentView.viewWithTag(502) as? UILabel
        lbl_TransactionDate?.text = newDate.stringFromDate(format: .newDateTime1, type: .local)
        //
        //        501 - id
        //        502- date
        //        503- approved
        //        504- total
        //        505- card type
        //        506- payment type
        //        507- view 1
        //        508- view 2
        //        509 - view 3
        //
        
        let lbl_Total = cell.contentView.viewWithTag(504) as? UILabel
        lbl_Total?.text = "\(transactionInfoArray[indexPath.row].amount.currencyFormat)"
        
        let lbl_CardType = cell.contentView.viewWithTag(505) as? UILabel
        lbl_CardType?.text = transactionInfoArray[indexPath.row].cardType
        
        let lbl_PaymentType = cell.contentView.viewWithTag(506) as? UILabel
        lbl_PaymentType?.text = transactionInfoArray[indexPath.row].paymentType
        
        // *********
        cell.contentView.backgroundColor = transactionInfoArray[indexPath.row].test_order == 1 ? #colorLiteral(red: 0.9882352941, green: 0.9764705882, blue: 0.5843137255, alpha: 1) : UIColor.white

        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isOrderSummery {
            if(UI_USER_INTERFACE_IDIOM() == .pad)
            {
                let txnData = ["show":true, "data": transactionInfoArray[indexPath.row]] as [String : Any]
                orderInfoDelegate?.didUpdateTransactionScreen?(with: txnData)
            }else
            {
                selectedIndex = indexPath.row
                self.performSegue(withIdentifier: "transactioninfo", sender: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UI_USER_INTERFACE_IDIOM() == .phone && isOrderSummery {
            if indexPath.section == 0 {
                if indexPath.row == 0 {
                    return 0
                }
            }
        }
        if UI_USER_INTERFACE_IDIOM() == .pad {
            if indexPath.row == 2{
                
                //                if versionOb < 4 {
                //                    return 40
                //                }
            }
            return UITableViewAutomaticDimension
        }else{
            if indexPath.row == 2{
                return isOrderSummery ? UITableViewAutomaticDimension : UITableViewAutomaticDimension
            }
        }
//        if UIScreen.main.bounds.width >= 365 && indexPath.section == 2 && isOrderSummery && UI_USER_INTERFACE_IDIOM() == .phone && !orderInfoModelObj.transactionArray[indexPath.row].isVoid {
//            return UITableViewAutomaticDimension
//        }
        return isOrderSummery ? UITableViewAutomaticDimension : UITableViewAutomaticDimension
    }
    
    
    @objc func handleTransactionPrintBtnAction(sender: UIButton) {
        let transationID = transactionInfoArray[sender.tag].txnId
        print ("transationID =",transationID)
        
        if !DataManager.isBluetoothPrinter && !DataManager.isCloudReceiptPrinter {
            appDelegate.showToast(message: "Please first enable the printer from settings")
            return
        }
        
        if DataManager.isBluetoothPrinter {
            self.callAPItoGetTransactionPrintReceiptContent(transactionID: transationID)
        }
        
        if DataManager.isCloudReceiptPrinter {
            self.callAPItoGetTransactionPrintReceiptContent(transactionID: transationID)
//            let Url = transactionInfoArray[sender.tag].transactionGoogle_receipt_url
//            //let str = Url.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
//            if let url:NSURL = NSURL(string: Url) {
//                UIApplication.shared.open(url as URL)
//            }
        }
        //            // for socket
        //            if DataManager.socketAppUrl != "" {
        //                MainSocketManager.shared.oncloseRecieptModal{
        //                    MainSocketManager.shared.onreset()
        //                    self.showHomeScreeen()
        //                }
        //            }
        
    }
    
    @objc func handleCrossAction(sender: UIButton) {
        self.view.endEditing(true)
        orderInfoModelObj.productsArray[sender.tag].isRefundSelected = false
        orderInfoModelObj.productsArray[sender.tag].isExchangeSelected = false
        updateRefundAmount()
        ClearbButtonColorChange()
    }
    
    @objc func handleExchangeAction(sender: UIButton) {
        self.view.endEditing(true)
        orderInfoModelObj.productsArray[sender.tag].isRefundSelected = false
        orderInfoModelObj.productsArray[sender.tag].isExchangeSelected = true
        updateRefundAmount()
        ClearbButtonColorChange()
    }
    
    @objc func handelTipRefundAction(sender: UIButton) {
        self.view.endEditing(true)
        if !sender.isSelected {
            orderInfoModelObj.isTipRefundSelected = true
        } else {
            orderInfoModelObj.isTipRefundSelected = false
        }
        tableView.reloadData()
        
        //Reload Table
        self.tableView.reloadData {
            self.tableView.contentOffset = .zero
            if !self.isOrderSummery {
                self.tableViewheight.constant = self.view.bounds.size.height - (UI_USER_INTERFACE_IDIOM() == .pad ? 120 : 210)
            }else {
                self.tableView.layoutIfNeeded()
                self.tableViewheight.constant = self.tableView.contentSize.height
            }
        }
    }
    
    @objc func handleShippingRefundAction(sender: UIButton) {
        self.view.endEditing(true)
        
        if !sender.isSelected {
            //sender.setImage(UIImage(named:"shipping-check.png"), for: .normal)
            //orderInfoModelObj.productsArray[sender.tag].isRefundSelected = false
            //orderInfoModelObj.productsArray[sender.tag].isExchangeSelected = true
            orderInfoModelObj.isshippingRefundSelected = true
//            updateRefundAmount()
        } else {
            //sender.setImage( UIImage(named:"shipping-uncheck.png"), for: .normal)
            //orderInfoModelObj.productsArray[sender.tag].isRefundSelected = false
            //orderInfoModelObj.productsArray[sender.tag].isExchangeSelected = false
            orderInfoModelObj.isshippingRefundSelected = false
//            updateRefundAmount()
        }
        tableView.reloadData()
        
        //Reload Table
        self.tableView.reloadData {
            self.tableView.contentOffset = .zero
            if !self.isOrderSummery {
                self.tableViewheight.constant = self.view.bounds.size.height - (UI_USER_INTERFACE_IDIOM() == .pad ? 120 : 210)
            }else {
                self.tableView.layoutIfNeeded()
                self.tableViewheight.constant = self.tableView.contentSize.height
            }
        }
    }
    
    @objc func handleRefundAction(sender: UIButton) {
        self.view.endEditing(true)
        
        if !sender.isSelected {
            //sender.setImage(UIImage(named:"shipping-check.png"), for: .normal)
            orderInfoModelObj.productsArray[sender.tag].isRefundSelected = false
            orderInfoModelObj.productsArray[sender.tag].isExchangeSelected = true
            updateRefundAmount()
        } else {
            //sender.setImage( UIImage(named:"shipping-uncheck.png"), for: .normal)
            orderInfoModelObj.productsArray[sender.tag].isRefundSelected = false
            orderInfoModelObj.productsArray[sender.tag].isExchangeSelected = false
            updateRefundAmount()
        }
    }
    
    @objc func handlePaymentCrossAction(sender: UIButton) {
        self.view.endEditing(true)
        orderInfoModelObj.transactionArray[sender.tag].isFullRefundSelected = false
        orderInfoModelObj.transactionArray[sender.tag].isPartialRefundSelected = false
        orderInfoModelObj.transactionArray[sender.tag].isVoidSelected = false
        orderInfoModelObj.transactionArray[sender.tag].partialAmount = ""
        tableView.reloadData()
        ClearbButtonColorChange()
    }
    
    @objc func handlePartialRefundAction(sender: UIButton) {
        self.view.endEditing(true)
        orderInfoModelObj.transactionArray[sender.tag].isFullRefundSelected = false
        orderInfoModelObj.transactionArray[sender.tag].isPartialRefundSelected = true
        orderInfoModelObj.transactionArray[sender.tag].isVoidSelected = false
        tableView.reloadData()
        ClearbButtonColorChange()
    }
    
    @objc func handleRefundProductAction(sender: UIButton) {
        self.view.endEditing(true)
        self.view.endEditing(true)
        appDelegate.str_Refundvalue = "refund"
        let tag = sender.tag + 1
        let isrefundShipping = orderInfoModelObj.isshippingRefundSelected
        let isRefundTip = orderInfoModelObj.isTipRefundSelected
        var isrefundOnly = false

        for i in 0..<orderInfoModelObj.productsArray.count {
            if orderInfoModelObj.productsArray[i].isExchangeSelected == true {
                isrefundOnly = true
            }
        }
    
        appDelegate.shippingRefundOnly = orderInfoModelObj.refundShippingAmount
        appDelegate.tipRefundOnly = orderInfoModelObj.tip
        if isrefundOnly && isrefundShipping {
            DataManager.isshippingRefundOnly = false
        } else if isrefundShipping {
            DataManager.isshippingRefundOnly = true
        } else {
            DataManager.isshippingRefundOnly = false
        }
        
        
        if isrefundOnly {
            DataManager.isshippingRefundOnly = false
            DataManager.isTipRefundOnly = false
        } else if isrefundShipping && isRefundTip {
            DataManager.isshippingRefundOnly = true
            DataManager.isTipRefundOnly = true
            appDelegate.shippingRefundOnly = orderInfoModelObj.refundShippingAmount
            appDelegate.tipRefundOnly = orderInfoModelObj.tip
        } else if isrefundShipping && !isRefundTip {
            DataManager.isshippingRefundOnly = true
            DataManager.isTipRefundOnly = false
            appDelegate.shippingRefundOnly = orderInfoModelObj.refundShippingAmount
            appDelegate.tipRefundOnly = orderInfoModelObj.tip
        } else if !isrefundShipping && isRefundTip {
            DataManager.isshippingRefundOnly = false
            DataManager.isTipRefundOnly = true
            appDelegate.shippingRefundOnly = orderInfoModelObj.shipping
            appDelegate.tipRefundOnly = orderInfoModelObj.tip
        }
        
        if versionOb < 4 {
            let dataChange = self.orderInfoModelObj.refundUrl.replacingOccurrences(of: "userID", with: "custID")
            let Url = dataChange
            let str = Url.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
            if let url:NSURL = NSURL(string: str!) {
                UIApplication.shared.open(url as URL)
            }
            return
        }
        
        DataManager.isCaptureButton = false
        
        if (self.revealViewController().frontViewPosition != FrontViewPositionLeft) {
            self.revealViewController()?.revealToggle(animated: true)
        }
        let updatedData = getUpdatedData()
        
        //        if !updatedData.0 && !updatedData.1 {
        //            self.showAlert(message: "Please select an item.")
        //            return
        //        }
        
        if !updatedData.0 && !updatedData.2 {
//            self.showAlert(message: "Please select refund type.")
            //appDelegate.showToast(message: "Please select refund type.")
            //return
        }
        
        let refundselectedArray = orderInfoModelObj.transactionArray.filter({$0.isPartialRefundSelected == true})
        let refundselectedAmountArray = orderInfoModelObj.transactionArray.filter({$0.isPartialRefundSelected == true && $0.partialAmount != ""})
        
        if !updatedData.3 && refundselectedArray.count > 0 {
//            self.showAlert(message: "Please enter partial amount.")
            appDelegate.showToast(message: "Please enter partial amount.")
            return
        }
        
        if updatedData.3 && refundselectedArray.count != refundselectedAmountArray.count {
//            self.showAlert(message: "Please enter partial amount.")
            appDelegate.showToast(message: "Please enter partial amount.")
            return
        }
        
        
        if updatedData.3 && refundselectedArray.count > 0 {
            
            var totalRefundAmount = Double()
            
            for product in orderInfoModelObj.productsArray {
                if product.isRefundSelected || product.isExchangeSelected {
                    totalRefundAmount += product.refundAmount
                }
            }
            totalRefundAmount = Double(totalRefundAmount.roundToTwoDecimal) ?? 0
            
            for transaction in orderInfoModelObj.transactionArray {
                if transaction.isPartialRefundSelected {
                    if (Double(transaction.partialAmount) ?? 0) < totalRefundAmount {
//                        self.showAlert(message: "Transaction amount must be greater than or equal to refunded amount.")
                        //appDelegate.showToast(message: "Transaction amount must be greater than or equal to refunded amount.")
                        //return
                    }
                    
                    if (Double(transaction.partialAmount) ?? 0) > transaction.availableRefundAmount {
//                        self.showAlert(message: "Transaction amount must be less than or equal to refunded amount $\(transaction.availableRefundAmount)")
                        //appDelegate.showToast(message: "Transaction amount must be less than or equal to refunded amount $\(transaction.availableRefundAmount)")
                        //return
                    }
                }
            }
            
        }
        
        if updatedData.3 && refundselectedArray.count > 0 {
            for transaction in orderInfoModelObj.transactionArray {
                if transaction.isPartialRefundSelected && (Double(transaction.partialAmount) ?? 0) > transaction.availableRefundAmount {
//                    self.showAlert(message: "Transaction amount must be less than or equal to refunded amount $\(transaction.availableRefundAmount)")
                    appDelegate.showToast(message: "Transaction amount must be less than or equal to refunded amount $\(transaction.availableRefundAmount)")
                    return
                }
            }
        }
        
        
        if updatedData.0 {
            //showAlert(message: "Feature is not available for this release.")
            //code comment for exchage bcoz work in going on. so that we add dialog // by sudama 14-12-2019
            PaymentsViewController.paymentDetailDict.removeAll()
            
            if let cardDetail = orderInfoModelObj.cardDetail {
                var number = cardDetail.number ?? ""
                
                if number.count == 4 {
                    number = "************" + number
                }
                
                PaymentsViewController.paymentDetailDict["key"] = "CREDIT"
                PaymentsViewController.paymentDetailDict["data"] = ["cardnumber":number,"mm":cardDetail.month, "yyyy":cardDetail.year, "cvv": ""]
            }
            
            let dict: JSONDictionary = ["data":updatedData.4]
            
            if UI_USER_INTERFACE_IDIOM() == .pad {
                appDelegate.str_Refundvalue = "refund"
                self.orderInfoDelegate?.didMoveToCartScreen?(with: dict)
            } else {
                appDelegate.str_Refundvalue = "refund"
                self.refundOrder = dict
                self.performSegue(withIdentifier: "cart", sender: nil)
            }
            return
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let dict: JSONDictionary = ["data":updatedData.4]
            self.orderInfoDelegate?.didMoveToCartScreen?(with: dict)
            //self.orderInfoDelegate?.didUpdateRefundScreen?(with: dict)
        } else {
            let dict: JSONDictionary = ["data":updatedData.4]
            appDelegate.str_Refundvalue = "refund"
            self.refundOrder = dict
            self.performSegue(withIdentifier: "cart", sender: nil)
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RefundViewController") as! RefundViewController
//            vc.orderInfoModelObj = updatedData.4
//            vc.orderInfoDelegate = self
//            //self.present(vc, animated: true, completion: nil)
//            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func handleOpenCustomAlert(sender: UIButton){
//        if sender.tag ==
        let dataTransfer = arrTransactionData[sender.tag] as? NSDictionary
//        navigationItem.leftBarButtonItem = UIBarButtonItem(image: nil, style: .plain, target: self, action: #selector(handleOpenCustomAlert))
//        let CustomMenu = tipCustomView.alert(title: "String") { [weak self] in
//            //self!.callAPItoGetOrderInfo()
//
//       // self!.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "menu-black"), style: .plain, target: self, action: #selector(self!.handleOpenCustomAlert))
//        }
        
        let storyboard = UIStoryboard(name: "iPad", bundle: nil)
        let CustomMenu = storyboard.instantiateViewController(withIdentifier: "TipViewController") as! TipViewController
        // self.navigationController?.pushViewController(controller, animated: false)
        //self.navigationController?.present(controller, animated: true, completion: nil)
     //   self.transactionID = "#\(transactionInfoArray[indexPath.row].txnId)"
        CustomMenu.delegateTipView = self
        CustomMenu.transactionID = dataTransfer?.value(forKey: "txn_id") as? String ?? ""
        CustomMenu.cardNumber = dataTransfer?.value(forKey: "cardNumber") as? String ?? ""
        CustomMenu.orderId = self.orderID
        CustomMenu.arrTransactionData = dataTransfer as! [String : Any] as NSDictionary //as! NSMutableArray
        self.navigationController?.present(CustomMenu, animated: true, completion: nil)
        //present(CustomMenu, animated: true)
    }
    
    
    private func showHomeScreeen() {
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let storyboard = UIStoryboard(name: "iPad", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "iPad_SWRevealViewController") as! SWRevealViewController
            appDelegate.window?.rootViewController = controller
            appDelegate.window?.makeKeyAndVisible()
        }else {
            self.setRootViewControllerForIphone()
        }
    }
    
    @objc func handleFullRefundAction(sender: UIButton) {
        self.view.endEditing(true)
        orderInfoModelObj.transactionArray[sender.tag].isFullRefundSelected = true
        orderInfoModelObj.transactionArray[sender.tag].isPartialRefundSelected = false
        orderInfoModelObj.transactionArray[sender.tag].isVoidSelected = false
        orderInfoModelObj.transactionArray[sender.tag].partialAmount = ""
        tableView.reloadData()
        ClearbButtonColorChange()
    }
    
    @objc func handleVoidAction(sender: UIButton) {
        self.view.endEditing(true)
        orderInfoModelObj.transactionArray[sender.tag].isFullRefundSelected = false
        orderInfoModelObj.transactionArray[sender.tag].isPartialRefundSelected = false
        orderInfoModelObj.transactionArray[sender.tag].isVoidSelected = true
        orderInfoModelObj.transactionArray[sender.tag].partialAmount = ""
        tableView.reloadData()
        ClearbButtonColorChange()
    }
    
    func updateRefundAmount() {
        //refundButton.isUserInteractionEnabled = true
        //refundButton.alpha = 1.0
        
        var totalRefundAmount = Double()
        var isForExchange = false
        
        for product in orderInfoModelObj.productsArray {
            if product.isExchangeSelected {
                isForExchange = true
            }
            if product.isRefundSelected || product.isExchangeSelected {
                totalRefundAmount += product.refundAmount
            }
            totalRefundAmount = Double(totalRefundAmount.roundToTwoDecimal) ?? 0
        }
        
        for i in 0..<orderInfoModelObj.transactionArray.count {
            orderInfoModelObj.transactionArray[i].partialAmount = ""
            orderInfoModelObj.transactionArray[i].isPartialRefundSelected = false
            orderInfoModelObj.transactionArray[i].isFullRefundSelected = false
            orderInfoModelObj.transactionArray[i].isVoidSelected = false
        }
        
        var isTransactionFound = false
        
        if totalRefundAmount > 0 {
            for i in 0..<orderInfoModelObj.transactionArray.count {
                if totalRefundAmount < orderInfoModelObj.transactionArray[i].availableRefundAmount {
                    orderInfoModelObj.transactionArray[i].partialAmount = totalRefundAmount.roundToTwoDecimal
                    orderInfoModelObj.transactionArray[i].isPartialRefundSelected = true
                    isTransactionFound = true
                    break
                }
                
                if totalRefundAmount == orderInfoModelObj.transactionArray[i].availableRefundAmount {
                    orderInfoModelObj.transactionArray[i].partialAmount = ""
                    orderInfoModelObj.transactionArray[i].isPartialRefundSelected = false
                    orderInfoModelObj.transactionArray[i].isVoidSelected = false
                    orderInfoModelObj.transactionArray[i].isFullRefundSelected = true
                    isTransactionFound = true
                    break
                }
            }
        }
        
        //        if !isForExchange && !isTransactionFound && totalRefundAmount > 0 {
        //            refundButton.isUserInteractionEnabled = false
        //            refundButton.alpha = 0.5
        //            self.showAlert(message: "No transaction found of refunded amount \(totalRefundAmount.currencyFormat)")
        //        }
        
        
        tableView.reloadData()
        
        //Reload Table
        self.tableView.reloadData {
            self.tableView.contentOffset = .zero
            if !self.isOrderSummery {
                self.tableViewheight.constant = self.view.bounds.size.height - (UI_USER_INTERFACE_IDIOM() == .pad ? 120 : 210)
            }else {
                self.tableView.layoutIfNeeded()
                self.tableViewheight.constant = self.tableView.contentSize.height
            }
        }
    }
}

extension OrderInfoViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.selectAll(nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
        return replacementText.isValidDecimal(maximumFractionDigits: 2)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.orderInfoModelObj.transactionArray[textField.tag].partialAmount = textField.text ?? ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//MARK: API Methods
extension OrderInfoViewController {
    func callAPItoGetOrderInfo() {
        
        DataManager.isCaptureButton = false
        
        if orderID == "" {
            return
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            Indicator.isEnabledIndicator = false
            Indicator.sharedInstance.showIndicator()
        }
        
        self.noTransactionFoundImageView.isHidden = true
        //self.refundButton.isUserInteractionEnabled = true
        //self.refundButton.alpha = 1.0
        let addKey = "/?source=web"
        //Call API
        OrderVM.shared.getOrderInfo(orderId: "\(orderID)\(addKey)") { (success, message, error) in
            if success == 1 {
                if UI_USER_INTERFACE_IDIOM() == .phone {
                    self.scrollView.scrollToTop()
                }
                DataManager.isshippingRefundOnly = false
                DataManager.isTipRefundOnly = false
                self.orderInfoModelObj = OrderVM.shared.orderInfo
                
                DataManager.OrderDataModel?.removeAll()
                
                if self.orderInfoModelObj.paymentStatus == "AUTH" {
                    
                    var CardNumber = ""
                    var CardYear = ""
                    var CardMonth = ""
                    //appDelegate.authOrderIdForOrderHistory = ""
                    
                    if let val = self.orderInfoModelObj.cardDetail?.number {
                        CardNumber = val
                    }
                    
                    if let val = self.orderInfoModelObj.cardDetail?.year {
                        CardYear = val
                    }
                    
                    if let val = self.orderInfoModelObj.cardDetail?.month {
                        CardMonth = val
                    }
                    
                    
                    let disValue = self.orderInfoModelObj.discount
                    var discountValue = ""
                    if disValue > 0 {
                        discountValue = "\(disValue + self.orderInfoModelObj.couponPrice)"
                        print("Discount data value \(discountValue)")
                    }
                    
                    let dataval = ["str_paymentMethod": self.orderInfoModelObj.paymentMethod,
                                   "str_cardNumber": CardNumber,
                                   "str_cardYear": CardYear,
                                   "str_cardMonth": CardMonth,
                                   "str_OrderId": self.orderInfoModelObj.orderID,
                                   "str_TipAmount": self.orderInfoModelObj.tip,
                                   "str_CouponId": self.orderInfoModelObj.couponId,
                                   "str_CouponCode": self.orderInfoModelObj.couponCode,
                                   "str_CouponPrice": self.orderInfoModelObj.couponPrice,
                                   "str_CouponTitle": self.orderInfoModelObj.couponCode,
                                   "str_Tax": self.orderInfoModelObj.tax,
                                   "str_CustomTaxId": self.orderInfoModelObj.customTax,
                                   "str_ShippingRate": self.orderInfoModelObj.shipping,
                                   "str_DiscountPrize": discountValue] as [String : Any]
                    DataManager.OrderDataModel = dataval
                }
                
                DispatchQueue.main.async(){
                    self.updateUI()
                    
                }
                
                self.handleOrderSummaryButtonAction()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    if UI_USER_INTERFACE_IDIOM() == .pad {
                        Indicator.isEnabledIndicator = true
                        Indicator.sharedInstance.hideIndicator()
                        //self.tableView.reloadData()
                    }
                })
                self.reloadTableData()
            }else {
                
                if UI_USER_INTERFACE_IDIOM() == .pad {
                    Indicator.isEnabledIndicator = true
                    Indicator.sharedInstance.hideIndicator()
                }
                
                if message != nil {
//                    self.showAlert(message: message!)
                    appDelegate.showToast(message: message!)
                }else {
                    self.showErrorMessage(error: error)
                }
                
                self.reloadTableData()
            }
        }
    }
    
    func callAPItoGetTransactionPrintReceiptContent(transactionID: String)
    {
        if orderID == "" {
            Indicator.isEnabledIndicator = true
            Indicator.sharedInstance.hideIndicator()
            return
        }
        
        //Call API
        HomeVM.shared.getTransactionPrintReceiptContent(transactionID: transactionID, responseCallBack: { (success, message, error) in
            if success == 1 {
                self.receiptModel = HomeVM.shared.receiptModel
                self.handlePrinterAction()
            }else {
                if message != nil {
                    //                    self.showAlert(message: message!)
                    appDelegate.showToast(message: message!)
                }else {
                    self.showErrorMessage(error: error)
                }
            }
        })
    }
    
    func callAPItoGetReceiptContent()
    {
        if orderID == "" {
            Indicator.isEnabledIndicator = true
            Indicator.sharedInstance.hideIndicator()
            return
        }
        
        //Call API
        HomeVM.shared.getReceiptContent(orderID: orderID, responseCallBack: { (success, message, error) in
            if success == 1 {
                self.receiptModel = HomeVM.shared.receiptModel
                self.handlePrinterAction()
            }else {
                if message != nil {
//                    self.showAlert(message: message!)
                    appDelegate.showToast(message: message!)
                }else {
                    self.showErrorMessage(error: error)
                }
            }
        })
    }
    
    func callAPItoGetTransactionInfo() {
        if orderID == "" {
            Indicator.isEnabledIndicator = true
            Indicator.sharedInstance.hideIndicator()
            return
        }
        
        Indicator.isEnabledIndicator = false
        Indicator.sharedInstance.showIndicator()
        
        //Call API
        OrderVM.shared.getTransactionInfo(orderId: orderID) { (success, message, error) in
            Indicator.isEnabledIndicator = true
            Indicator.sharedInstance.hideIndicator()
            if success == 1 {
                self.scrollView.scrollToTop()
                self.tableView.scrollToTop()
                self.transactionInfoArray = OrderVM.shared.transactionInfoArray
                self.noTransactionFoundImageView.isHidden = self.transactionInfoArray.count != 0
                self.reloadTableData()
            }else {
                self.noTransactionFoundImageView.isHidden = false
                if message != nil {
                    //...
                }else {
                    self.showErrorMessage(error: error)
                }
            }
        }
    }
}


//MARK: PrinterManagerDelegate
extension OrderInfoViewController: PrinterManagerDelegate {
    
    func loadPrinter() {
        PrintersViewController.printerManager = PrinterManager()
        PrintersViewController.printerManager?.delegate = self
        PrintersViewController.printerArray = PrintersViewController.printerManager!.nearbyPrinters
        PrintersViewController.printerUUID = nil
        //self.tbl_Settings.reloadData()
    }
    public func nearbyPrinterDidChange(_ change: NearbyPrinterChange) {
        switch change {
        case let .add(p):
            PrintersViewController.printerArray.append(p)
        case let .update(p):
            guard let row = (PrintersViewController.printerArray.index() { $0.identifier == p.identifier } ) else {
                return
            }
            if p.state == .connected {
                DataManager.receipt = true
                PrintersViewController.printerUUID = p.identifier
            }
            else if p.state == .disconnected {
                PrintersViewController.printerUUID = nil
            }
            PrintersViewController.printerArray[row] = p
            print(p.state)
        case let .remove(identifier):
            guard let row = (PrintersViewController.printerArray.index() { $0.identifier == identifier } ) else {
                return
            }
            
            if PrintersViewController.printerUUID == PrintersViewController.printerArray[row].identifier {
                PrintersViewController.printerUUID = nil
            }
            PrintersViewController.printerArray.remove(at: row)
        }
        
        //self.tbl_Settings.reloadData()
    }
    
    func moveToSettings() {
        //        if !isShowAlert {
        //            return
        //        }
        self.showAlert(title: "Alert", message: "Please enable the bluetooth from Settings.", otherButtons: nil, cancelTitle: kOkay) { (action) in
            guard let url = URL(string: "App-Prefs:root=Bluetooth") else {return}
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}

