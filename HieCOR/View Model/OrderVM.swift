//
//  OrderVM.swift
//  HieCOR
//
//  Created by Deftsoft on 21/08/18.
//  Copyright © 2018 HyperMacMini. All rights reserved.
//

import Foundation

class OrderVM {
    
    //MARK: Variables
    var isMoreOrderFound = true
    var isMoreEndDrawerFound = true
    var totalRecord = String()
    var ordersList = [String: [OrdersHistoryList]]()
    var orderInfo = OrderInfoModel()
    var productReturnStatus = Bool()
    var transactionInfoArray = [TransactionsDetailModel]()
    var returnConditions = [ConditionsForReturnModel]()
	var drawerHistory = [DrawerHistoryModel]()
    var drawerHistoryOpen = [DrawerHistoryModel]()
    var drawerHistoryEnd = [DrawerHistoryModel]()
    var checkDrawerEnd = [DrawerHistoryModel]()
    var refundData = RefundOrderDataModel()
    var transactionVoidData = VoidTransactionDataModel()
    
    //MARK: Create Shared Instance
    public static let shared = OrderVM()
    private init() {}
    
    //MARK: Class Functions
    func getOrder(url: String, startDate: String? = nil, endDate: String? = nil, pageNumber: Int? = nil, responseCallBack: @escaping responseCallBack) {
        //Offline
        if !NetworkConnectivity.isConnectedToInternet() && DataManager.isOffline {
            CDManager.shared.fetchAllOrders(startDate: startDate, endDate: endDate, pageNumber: pageNumber, responseCallBack: { (success, message, error) in
                responseCallBack(success, message, error)
            })
            return
        }
        //Online
        APIManager.getOrder(url: url, successCallBack: { (responseDict) in
            if responseDict?[kSuccess] as? Int == 1 {
                self.parseOrderData(responseDict: responseDict!)
                responseCallBack(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                responseCallBack(0, responseDict![APIKeys.kError] as? String, nil)
            }
        }) { (errorReason, error) in
            if error?.code != -999 {
                responseCallBack(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
                debugPrint(errorReason ?? "")
            }
        }
    }
    
    func getSearchOrder(searchText: String, pageNumber: Int? = nil, responseCallBack: @escaping responseCallBack) {
        
        APIManager.getSearchOrder(searchText: searchText, indexOfPage: pageNumber!, successCallBack: { (responseDict) in
            if responseDict?[kSuccess] as? Int == 1 {
                self.ordersList.removeAll()
                self.parseOrderData(responseDict: responseDict!)
                responseCallBack(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                responseCallBack(0, responseDict![APIKeys.kError] as? String, nil)
            }
        }) { (errorReason, error) in
            if error?.code != -999 {
                responseCallBack(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
                debugPrint(errorReason ?? "")
            }
        }
    }

    func getOrderInfo(orderId: String, responseCallBack: @escaping responseCallBack) {
        
        APIManager.getOrderInfo(orderId: orderId, successCallBack: { (responseDict) in
            if responseDict?[kSuccess] as? Int == 1 {
                self.parseOrderInfoData(responseDict: responseDict!)
            
                responseCallBack(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                responseCallBack(0, responseDict![APIKeys.kError] as? String, nil)
            }
        }) { (errorReason, error) in
            if error?.code != -999 {
                responseCallBack(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
                debugPrint(errorReason ?? "")
            }
        }
    }

    func getTransactionInfo(orderId: String, isTransactionID: Bool? = false, responseCallBack: @escaping responseCallBack) {
        
        APIManager.getTransactionInfo(orderId: orderId,isTransactionID: isTransactionID!, successCallBack: { (responseDict) in
            if responseDict?[kSuccess] as? Int == 1 {
                self.parseTransactionInfoData(responseDict: responseDict!)
                responseCallBack(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                responseCallBack(0, responseDict![APIKeys.kError] as? String, nil)
            }
        }) { (errorReason, error) in
            if error?.code != -999 {
                responseCallBack(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
                debugPrint(errorReason ?? "")
            }
        }
    }

    func getReturnConditions(responseCallBack: @escaping responseCallBack) {
        
        APIManager.getReturnConditions(successCallBack: { (responseDict) in
            if responseDict?[kSuccess] as? Int == 1 {
                self.parseReturnConditionData(responseDict: responseDict!)
                responseCallBack(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                responseCallBack(0, responseDict![APIKeys.kError] as? String, nil)
            }
        }) { (errorReason, error) in
            if error?.code != -999 {
                responseCallBack(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
                debugPrint(errorReason ?? "")
            }
        }
    }
    
    func returnOrder(parameters: JSONDictionary, responseCallBack: @escaping responseCallBack) {
        
        APIManager.returnOrder(parameters: parameters, successCallBack: { (responseDict) in
            if responseDict?[kSuccess] as? Int == 1 {
                responseCallBack(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                responseCallBack(0, responseDict![APIKeys.kError] as? String, nil)
            }
        }) { (errorReason, error) in
            if error?.code != -999 {
                responseCallBack(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
                debugPrint(errorReason ?? "")
            }
        }
    }

    func refundOrder(parameters: JSONDictionary, responseCallBack: @escaping responseCallBack) {
        
        APIManager.refundOrder(parameters: parameters, successCallBack: { (responseDict) in
            if responseDict?[kSuccess] as? Int == 1 {
                self.parseRefundOrderData(responseDict: responseDict!)
                responseCallBack(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                responseCallBack(0, responseDict![APIKeys.kError] as? String, nil)
            }
        }) { (errorReason, error) in
            if error?.code != -999 {
                responseCallBack(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
                debugPrint(errorReason ?? "")
            }
        }
    }
    
    func voidRefundTransaction(parameters: JSONDictionary, responseCallBack: @escaping responseCallBack) {
        
        APIManager.voidRefundTransaction(parameters: parameters, successCallBack: { (responseDict) in
            if responseDict?[kSuccess] as? Int == 1 {
                self.parseVoidTrnsactionData(responseDict: responseDict!)
                responseCallBack(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                responseCallBack(0, responseDict![APIKeys.kError] as? String, nil)
            }
        }) { (errorReason, error) in
            if error?.code != -999 {
                responseCallBack(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
                debugPrint(errorReason ?? "")
            }
        }
    }
    
    func saveSignatureOrder(parameters: JSONDictionary, responseCallBack: @escaping responseCallBack) {
        
        APIManager.saveSignatureOrder(parameters: parameters, successCallBack: { (responseDict) in
            if responseDict?[kSuccess] as? Int == 1 {
                //self.parseVoidTrnsactionData(responseDict: responseDict!)
                responseCallBack(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                responseCallBack(0, responseDict![APIKeys.kError] as? String, nil)
            }
        }) { (errorReason, error) in
            if error?.code != -999 {
                responseCallBack(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
                debugPrint(errorReason ?? "")
            }
        }
    }

    func sendEmailOrText(url: String,parameters: JSONDictionary, responseCallBack: @escaping responseCallBack) {
        
        APIManager.sendEmailOrText(url: url,parameters: parameters, successCallBack: { (responseDict) in
            if responseDict?[kSuccess] as? Int == 1 {
                responseCallBack(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                responseCallBack(0, responseDict![APIKeys.kError] as? String, nil)
            }
        }) { (errorReason, error) in
            if error?.code != -999 {
                responseCallBack(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
                debugPrint(errorReason ?? "")
            }
        }
    }

    func checkDrawerEnd(source: String, responseCallBack: @escaping responseCallBack) {
        
        APIManager.checkDrawerEnd(source: source, successCallBack: { (responseDict) in
            if responseDict?[kSuccess] as? Int == 1 {
                self.checkDrawerEnd.removeAll()
                self.parseCheckDrawerEndData(responseDict: responseDict!)
                responseCallBack(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                responseCallBack(0, responseDict![APIKeys.kError] as? String, nil)
            }
        }) { (errorReason, error) in
            if error?.code != -999 {
                responseCallBack(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
                debugPrint(errorReason ?? "")
            }
        }
    }
    
    func getDrawerHistory(source: String,pageNumber: Int,responseCallBack: @escaping responseCallBack) {
        
        APIManager.getDrawerHistory(source: source,pageNumber: pageNumber , successCallBack: { (responseDict) in
            if responseDict?[kSuccess] as? Int == 1 {
                if pageNumber == -1 {   // Offline Data Parsing if page number == -1
                    //  self.parseCategoryDataOffline(responseDict: responseDict!)
                }else {                 //Online Data Parsing
                    
                    if pageNumber == 1 {
                        self.isMoreEndDrawerFound = true
                         self.drawerHistoryEnd.removeAll()
                         self.drawerHistoryOpen.removeAll()
//                        self.isAllDataLoaded[1] = true
//                        self.checkIsDataLoaded()
                    }
                    self.drawerHistoryOpen.removeAll()
                     self.parseDrawerHistoryData(responseDict: responseDict!)
                }
                
                
                responseCallBack(1, responseDict![APIKeys.kMessage] as? String, nil)
            }else{
                responseCallBack(0, responseDict![APIKeys.kError] as? String, nil)
            }
        }) { (errorReason, error) in
            if error?.code != -999 {
                responseCallBack(0, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
                debugPrint(errorReason ?? "")
            }
        }
    }
}
