//
//  UIColor.swift
//  HieCOR
//
//  Created by ios on 23/04/18.
//  Copyright © 2018 ios. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    //MARK: Enumeration
    enum HieCORColor {
        
        case blue
        case gray
        case buttonGray

        func colorWith(alpha: CGFloat) -> UIColor {
            var colorToReturn:UIColor?
            switch self {
            case .blue:
                colorToReturn = UIColor.init(red: 11.0/255.0, green: 139.0/255.0, blue: 201.0/255.0, alpha: 1.0)
            case .gray:
                colorToReturn = #colorLiteral(red: 0.631372549, green: 0.6431372549, blue: 0.6745098039, alpha: 1).withAlphaComponent(alpha)
            case .buttonGray:
                colorToReturn = #colorLiteral(red: 0.9528579116, green: 0.9529945254, blue: 0.9528279901, alpha: 1).withAlphaComponent(alpha)
            }
            return colorToReturn!
        }
    }
    
    func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        setFill()
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return image
    }
    
}

